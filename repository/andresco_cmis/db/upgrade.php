<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @param int $oldversion the version we are upgrading from
 * @return bool result
 */
function xmldb_repository_andresco_cmis_upgrade($oldversion = 0) {
    global $DB;
    $dbman = $DB->get_manager();
    $result = true;
    if ($result && $oldversion >= 20130101500) {

        $table = new xmldb_table('repository_instances');
        $field = new xmldb_field('disable');
        if (!$dbman->field_exists($table, $field)) {
            $field->set_attributes(XMLDB_TYPE_INTEGER, '1', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '0');
            $dbman->add_field($table, $field);
        }

        //START : ANDRESCO CMIS
        // Prepare a SQL file to run with the first time installation of plugin to Create "Username" field with the validations given in SRS
        // create the Andresco category
        $data_category = new stdClass();
        $data_category->name = 'Andresco';
        $data_category->sortorder = '2';
        $andresco_catg_id = $DB->get_record('user_info_category', array('name' => $data_category->name), 'id');
        if (!empty($andresco_catg_id->id)) {
            $get_field_data = $DB->get_record('user_info_field', array('categoryid' => $andresco_catg_id->id), 'id');
            $DB->delete_records('user_info_field', array('categoryid' => $andresco_catg_id->id));
            $DB->delete_records('user_info_category', array('id' => $andresco_catg_id->id));
        }
        $DB->insert_record('user_info_category', $data_category);
        // create the alfrescousername field entry
        $get_andresco_catg_id = $DB->get_record('user_info_category', array('name' => $data_category->name), 'id');

        if (!empty($get_andresco_catg_id)) {
            $data_alfresco_username = new stdClass();
            $data_alfresco_username->shortname = 'alfrescousername';
            $data_alfresco_username->name = 'Alfresco User Name';
            $data_alfresco_username->datatype = 'text';
            $data_alfresco_username->description = 'Mapping to Alfresco User Name for Andresco Repository Plugin Single Sign-on';
            $data_alfresco_username->descriptionformat = '1';
            $data_alfresco_username->categoryid = $get_andresco_catg_id->id;
            $data_alfresco_username->sortorder = '1';
            $data_alfresco_username->required = '0';
            $data_alfresco_username->locked = '1';
            $data_alfresco_username->visible = '0';
            $data_alfresco_username->forceunique = '0';
            $data_alfresco_username->signup = '0';
            $data_alfresco_username->defaultdata = '';
            $data_alfresco_username->defaultdataformat = '0';
            $data_alfresco_username->param1 = '30';
            $data_alfresco_username->param2 = '256';
            $data_alfresco_username->param3 = '0';
            $data_alfresco_username->param4 = '';
            $data_alfresco_username->param5 = '';
            $rec = $DB->insert_record('user_info_field', $data_alfresco_username);
        }

        if (!empty($get_field_data)) {
            $user_info_data = $DB->get_records('user_info_data', array('fieldid' => $get_field_data->id));
            foreach ($user_info_data as $key => $value) {
                $DB->update_record('user_info_data', array('id' => $value->id, 'fieldid' => $rec));
            }
        }

        // Create Scafold Table
        $table_scafold = new xmldb_table('scafold_ui');
        $table_scafold->add_field('id', XMLDB_TYPE_INTEGER, '20', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null, null);
        $table_scafold->add_field('property_name', XMLDB_TYPE_CHAR, '255', null, null, null, null, null, null);
        $table_scafold->add_field('display_label', XMLDB_TYPE_CHAR, '255', null, null, null, null, null, null);
        $table_scafold->add_field('is_constraint', XMLDB_TYPE_INTEGER, '4', null, null, null, null, null, null);
        $table_scafold->add_field('default_value_expression', XMLDB_TYPE_TEXT, 'medium', null, null, null, null, null, null);
        $table_scafold->add_field('render_type', XMLDB_TYPE_CHAR, '255', null, null, null, null, null, null);
        $table_scafold->add_field('is_mandatory', XMLDB_TYPE_INTEGER, '4', null, null, null, null, null, null);
        $table_scafold->add_field('weightage', XMLDB_TYPE_INTEGER, '11', null, null, null, null, null, null);
        $table_scafold->add_field('repository_instance_id', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
        $table_scafold->add_field('created_by', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
        $table_scafold->add_field('updated_by', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
        $table_scafold->add_field('created', XMLDB_TYPE_INTEGER, '11', null, null, null, null, null, null);
        $table_scafold->add_field('updated', XMLDB_TYPE_INTEGER, '11', null, null, null, null, null, null);
        $table_scafold->add_key('primary', XMLDB_KEY_PRIMARY, array('id'), null, null);
        if (!$dbman->table_exists($table_scafold)) {
            $dbman->create_table($table_scafold);
        }

        // Create Display Item config table
        $table_display_config = new xmldb_table('display_item_config');
        $table_display_config->add_field('id', XMLDB_TYPE_INTEGER, '20', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null, null);
        $table_display_config->add_field('property_name', XMLDB_TYPE_CHAR, '255', null, null, null, null, null, null);
        $table_display_config->add_field('display_label', XMLDB_TYPE_CHAR, '255', null, null, null, null, null, null);
        $table_display_config->add_field('render_type', XMLDB_TYPE_CHAR, '255', null, null, null, null, null, null);
        $table_display_config->add_field('weightage', XMLDB_TYPE_INTEGER, '11', null, null, null, null, null, null);
        $table_display_config->add_field('repository_instance_id', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
        $table_display_config->add_field('created_by', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
        $table_display_config->add_field('updated_by', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
        $table_display_config->add_field('created', XMLDB_TYPE_INTEGER, '11', null, null, null, null, null, null);
        $table_display_config->add_field('updated', XMLDB_TYPE_INTEGER, '11', null, null, null, null, null, null);
        $table_display_config->add_key('primary', XMLDB_KEY_PRIMARY, array('id'), null, null);
        if (!$dbman->table_exists($table_display_config)) {
            $dbman->create_table($table_display_config);
        }
    }

    return $result;
}
