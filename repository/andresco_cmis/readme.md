Andresco CMIS
-------------

Androgogic's Moodle-Alfresco Integration and Repository Plugin.

(c) 2013+ Androgogic Pty Ltd  
http://www.androgogic.com  
support@androgogic.com

URL format
----------

Andresco uses an internal rewrite for handling URLs. This is so that it can add
dynamic paramters such as the name of the Moodle instance, course information,
user information etc to Alfresco.

The format of this URL is:

<<protocol>>://<<baseurl]/repository/andresco_cmis/index.php?repo=A&uuid=X&version=Y&filename=Z

- repo is the Moodle repository instance number
- UUID is the Alfresco unique content ID
- Version is the specific Alfresco version of the content (optional, assumes latest if omitted)
- Filename is the name of the file

Dynamic parameters are added to the URL between the version and filename as query parameters.

These are then passed to a script on the Alfresco server responsible for authenticating requests
called auth.php.
