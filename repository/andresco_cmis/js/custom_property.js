/**
 * Default value expression parser
 * @author Dushyant Sharma
 */

//Evaluate value of expression
function getvalue(obj) {
//$(".default_value").on("blur", function() {
    var default_value = $(obj).val();
    var parsed_str = '';
    var final_str = '';
    var str = '';
    var flag = true;
    var error_obj = '';
    var error_msg = '';
    var split_id = $(obj).attr('id').split("_");
    var js_array = default_value.split("REF");
    if (default_value != '' && js_array.length > 1) {
        var code = js_array[1].replace("[", "");
        var code = code.replace("]", "");
        $('.prop_name').each(function() {
            if ($(this).val() == code) {
                var id_array = $(this).attr('id').split("_");
                default_value = $("#default_value_expression_" + id_array[2]).val();
            }
            if (default_value == '') {
                error_obj = "Unsuccessful";
                error_msg = "Not a proper reference";
            }
        });
    }
    if (default_value != '') {
        $.loader({className: "blue-with-image-2", content: ''});
        $.ajax({
            type: "POST",
            url: "../util/parse_variables.php",
            data: {default_value: default_value},
            async: false
        }).done(function(msg) {
            $.loader('close');
            var data = $.parseJSON(msg);
            if (data.result == "Successful") {
                parsed_str = data.value;
            } else {
                error_obj = data.result;
                error_msg = data.value;
            }
        });
    } else {
        parsed_str = default_value;
    }
    if (error_obj == '') {
        var js_array = parsed_str.split("JS");
        for (var i = 0; i < js_array.length; i++) {
            var isMatch = js_array[i].substr(0, 1) == "[";
            if (isMatch) {
                var code = js_array[i].replace("[", "");
                var remain_string = code.split("]");
                code = remain_string[0];
                options = {};
                try {
                    result = esprima.parse(code, options);
                    var exVal = eval(code);
                    str = JSON.stringify(result, adjustRegexLiteral, 4);
                    options.tokens = true;
                    if (window.updateTree) {
                        window.updateTree(result);
                    }
                    var err = 'No error';
                } catch (e) {
                    if (window.updateTree) {
                        window.updateTree();
                    }
                    str = e.name + ': ' + e.message;
                    err = str;
                    //TODO return and save the value
                }
                if (err == "No error") {
                    //TODO: How to obtain the code result
                    exVal = exVal + remain_string[1];
                    var parsed_str = parsed_str.replace("JS" + js_array[i], exVal);
//                    var parsed_str = parsed_str.replace("JS|js", "");
                } else {
                    //Set error for Javascript expression
                    $("#error_span_" + split_id[3]).text(err);
                    $("#span_" + split_id[3]).text("");
                    return;
                }
            }
        }
    } else {
        //Set error for context variable
        $("#error_span_" + split_id[3]).text("Error: " + error_msg);
        $("#span_" + split_id[3]).text("");
        return;
    }
    $("#span_" + split_id[3]).text(parsed_str);
    $("#error_span_" + split_id[3]).text("");
    return;
}


// Special handling for regular expression literal since we need to
// convert it to a string literal, otherwise it will be decoded
// as object "{}" and the regular expression would be lost.
function adjustRegexLiteral(key, value) {
    if (key === 'value' && value instanceof RegExp) {
        value = value.toString();
    }
    return value;
}
