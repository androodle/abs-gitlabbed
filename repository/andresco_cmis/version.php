<?php

/*-----------------------------------------------------------------------------*
 * Andresco Repository Plugin: Version Information
 *-----------------------------------------------------------------------------*
 *
 * Andresco is copyright 2014+ by Androgogic Pty Ltd
 * http://www.androgogic.com.au/andresco
 *
 *-----------------------------------------------------------------------------*/

defined('MOODLE_INTERNAL') || die();

if ( !isset($plugin)) {
	$plugin = new stdClass();	
}

$plugin->version   = 2014070900;
$plugin->component = 'repository_andresco_cmis';
