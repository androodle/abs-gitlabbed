<?php

//Save Display Item Form Data

require('../../../config.php');
global $USER, $DB;
$j = 0;
if (isset($_POST['weightage'])) {
    $totalweightage = count($_POST['weightage']);
}
$rid = $_GET['rid'];
$DB->delete_records('display_item_config', array('repository_instance_id' => $rid));
if (isset($_POST['weightage'])) {
    foreach ($_POST['weightage'] as $key => $values) {
        $i = $values;
        $data = new stdClass();
        $data->property_name = $_POST['property_name_' . $i];
        $data->display_label = $_POST['display_label_' . $i];
        $data->render_type = $_POST['render_type_' . $i];
        $data->weightage = $j;
        $data->repository_instance_id = $rid;
        $data->created_by = $USER->id;
        $data->updated_by = $USER->id;
        $data->created = time();
        $data->updated = time();
        $data->id = $DB->insert_record('display_item_config', $data);
        $j++;
    }
}
//$returnurl = new moodle_url('/repository/andresco_cmis/util/add_scafold.php?rid='.$rid);
$sess_key = $USER->sesskey;
$returnurl = new moodle_url('/admin/repository.php?sesskey=' . $sess_key . '&action=edit&repos=andresco_cmis');
redirect($returnurl);

