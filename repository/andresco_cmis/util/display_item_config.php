<?php
/*
 * @File to add display configuration for custom properties
 * @author Dushyant Sharma
 */

require('../../../config.php');
global $DB, $USER, $CFG;
require_once($CFG->dirroot . '/repository/lib.php');
require_login();
$htm = '';
$even = '';
//$PAGE->requires->js('/repository/andresco_cmis/js/jquery.js');
//$PAGE->requires->js('/repository/andresco_cmis/js/jquery-ui.js');
$PAGE->requires->js('/totara/core/js/lib/jquery-2.1.0.min.js');
$PAGE->requires->js('/totara/core/js/lib/jquery-ui-1.10.4.custom.min.js');
$PAGE->requires->js('/repository/andresco_cmis/js/common.js');
$PAGE->requires->js('/repository/andresco_cmis/js/add_display_item.js');
$PAGE->requires->js('/repository/andresco_cmis/js/jquery.loader-0.3.js');
$PAGE->requires->css('/repository/andresco_cmis/css/loader.css');

$systemcontext = get_context_instance(CONTEXT_SYSTEM);
$PAGE->set_context($systemcontext);
$PAGE->set_url('/blocks/ui_scafold/display_item_config.php');
$PAGE->navbar->add('Admin', '/admin');
$PAGE->navbar->add(get_string('display_item_config', 'repository_andresco_cmis'));
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_button("");
$PAGE->set_title(get_string('display_item_config', 'repository_andresco_cmis'));
echo $OUTPUT->header();
//$objform->display();    
// GET the Scafold Data
$rid = required_param('rid', PARAM_INT);
$total_display_data = 0;
//ANDRESCO:: Fetch Repository instance Data
$where3 = " WHERE ( dd.repository_instance_id = '" . $rid . "' ) ";
$display_array = $DB->get_records_sql("SELECT dd.* FROM {display_item_config} dd $where3 ORDER BY dd.weightage");
$total_display_data = count($display_array);

//Move through a CSV file, and output an associative array for each line 
ini_set("auto_detect_line_endings", true);
$current_row = 1;
$header_array = array();
$data_array = array();
$handle = fopen($CFG->dirroot . "/repository/andresco_cmis/default_properties.csv", "r");
ini_set("auto_detect_line_endings", false);
while (($data = fgetcsv($handle, 10000, ",") ) !== FALSE) {
    $number_of_fields = count($data);
    if ($current_row == 1) {
//Header line 
        for ($c = 0; $c < $number_of_fields; $c++) {
            $header_array[$c] = $data[$c];
        }
    } else {
//Data line 
        for ($c = 0; $c < $number_of_fields; $c++) {
            $data_array[$current_row][$header_array[$c]] = $data[$c];
        }
    }
    $current_row++;
}
fclose($handle);
$htm .= "<div align='center'><h2>" . get_string('display_item_config', 'repository_andresco_cmis') . "</h2><br/><span id='global_error' style='color: red;display:none;'>contact Androgogic</span></div>";

// ANDRESCO:: Display Custom Form
$htm .= "<form name='Row_form' id='Row_form' method='post' action='save_display_items.php?rid=" . $rid . "'  >";
$htm .= "<div align='right'><input type='button' name='addrow' id='addrow' value='Add Row +' onclick='addrRow(" . $rid . ");' ></div></br>";


$htm .= "<div id='show_error' style='display:none;color:red;background-color:#DDDDDD;' align='left'>Error Found!!</br>
         <div>Below listed Property-Names have conflicts. Please correct them.</div>   
         <div id='err2' ></div> 
         </div>";



$htm .= '<table id="add"  cellspacing="0" cellpadding="2" width="100%" > 
            <th style="background-color: grey" >' . get_string('ui_scafold_property_name', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_display_label', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_rendering_type', 'repository_andresco_cmis') . '</th>
            <th style="background-color: grey">' . get_string('ui_scafold_delete', 'repository_andresco_cmis') . '</th>';

$htm .= '<tbody >';
if ($total_display_data == '0') {
    if (count($data_array) != 0) {
        $i = 0;
        foreach ($data_array as $data) {
            $htm .= '
            <tr style="background-color:#E0E0E0;vertical-align:top;height:50px" id="new_' . $i . '">
                <td><input type="hidden" name="weightage[]" id="weightage_' . $i . '" value="' . $i . '" >
                <input type="text" class="prop_name" name="property_name_' . $i . '" id="property_name_' . $i . '" value = "' . $data['Property Name'] . '"></td>
                <td><input  class="label_name" type="text" name="display_label_' . $i . '" id="display_label_' . $i . '" value = "' . $data['Display Label'] . '"></td>
                <td>
                <select class="render_type" name="render_type_' . $i . '" id="render_type_' . $i . '">';
            if ($data['Render Type'] == "Multiline Text Box") {
                $htm .=
                        '<option value = "Single Line Textfield">Single Line Textfield</option>       
                         <option value = "Multiline Text Box" selected = "selected">Multiline Text Box</option>';
            } else {
                $htm .=
                        '<option value = "Single Line Textfield" selected = "selected">Single Line Textfield</option>       
                         <option value = "Multiline Text Box">Multiline Text Box</option>';
            }
            $htm .=
                    '</select>
                     </td>
                     <td><img style = "cursor:pointer" width = "14" height = "14" src = "../pix/delete.png" onclick = "fncDelRow(0)"></td>
                     </tr>';
            $i++;
        }
    } else {
        $htm .= '
                    <tr style = "background-color:#E0E0E0;vertical-align:top;height:50px" id = "new_0">
                    <td><input type = "hidden" name = "weightage[]" id = "weightage_0" value = "0" >
                    <input type = "text" class = "prop_name" name = "property_name_0" id = "property_name_0"></td>
                    <td><input class = "label_name" type = "text" name = "display_label_0" id = "display_label_0"></td>
                    <td>
                    <select class = "render_type" name = "render_type_0" id = "render_type_0">
                    <option value = "Single Line Textfield">Single Line Textfield</option>
                    <option value = "Multiline Text Box">Multiline Text Box</option>
                    </select>
                    </td>
                    <td><img style = "cursor:pointer" width = "14" height = "14" src = "../pix/delete.png" onclick = "fncDelRow(0)"></td>
                    </tr>';
    }
} else {
// ANDRESCO:: Loop the Repository instance Data obtained
    foreach ($display_array as $values) {
        $dropDownSelected1 = $dropDownSelected2 = '';
        if ($values->render_type == 'Single Line Textfield') {
            $dropDownSelected1 = 'selected = selected';
        } else {
            $dropDownSelected2 = 'selected = selected';
        }
        $htm .= '
<tr ' . $even . ' id = "new_' . $values->weightage . '" style = "background-color:#E0E0E0;vertical-align:top;height:50px">
    <td><input type = "hidden" name = "weightage[]" id = "weightage_' . $values->weightage . '" value = "' . $values->weightage . '" >
        <input "type="text" class="prop_name" name="property_name_' . $values->weightage . '" id="property_name_' . $values->weightage . '" value="' . $values->property_name . '"></td>
    <td><input  class="label_name" type="text" name="display_label_' . $values->weightage . '" id="display_label_' . $values->weightage . '" value="' . $values->display_label . '"></td>
    <td>
        <select class="render_type" name="render_type_' . $values->weightage . '" id="render_type_' . $values->weightage . '">
            <option  ' . $dropDownSelected1 . ' value="Single Line Textfield">Single Line Textfield</option>
            <option  ' . $dropDownSelected2 . ' value="Multiline Text Box">Multiline Text Box</option>
        </select>
    </td>
    <td><img style="cursor:pointer" onclick="fncDelRow(' . $values->weightage . ')" width="14" height="14" src="../pix/delete.png" ></td>
</tr>';
    }
}
$sess_key = $USER->sesskey;
$returnurl = new moodle_url('/admin/repository.php?sesskey=' . $sess_key . '&action=edit&repos=andresco_cmis');
$htm .= '</tbody>';
$htm .= '</table>';
$htm .= "<div align = 'right'><input onclick = 'return validateProp();
                                     ' type = 'button' name = 'validaterow' id = 'validaterow' value = 'Validate' > <input type = 'submit' name = 'saverow' id = 'saverow' value = 'Save' > <input type = 'button' name = 'cancel' id = 'cancel' onclick = 'cancelFunction();
                                     ' value = 'Cancel'></div>";
$htm .= "</form>";

echo $htm;
echo $OUTPUT->footer();
?>
<script type="text/javascript">

    $(document).ready(function() {
        $("#add tbody").sortable({helper: fixHelper});
    });

    function cancelFunction() {
        var url = "<?php echo $returnurl; ?>";
        var div = document.createElement('div');
        div.innerHTML = url;
        url = div.firstChild.nodeValue;
        document.location.href = url;
    }

    function validateProp() {
        $("#global_error").hide();
        var flag = true;
        $('#add > tbody  > tr .prop_name').each(function() {
            var prop_name_id = '#' + $(this).attr('id');
            var get_prop_name_data = $(prop_name_id).val();
            if ($.trim(get_prop_name_data) == '') {
                $(prop_name_id).focus();
                alert("Property name can not be left blank");
                flag = false;
                return flag;
            }
        });

        //check if property name is blank then dont apply further validation
        if (flag == false) {
            return flag;
        }

        //Check for duplicate property values
        var values = [];
        $('#add > tbody  > tr .prop_name').each(function() {
            var prop_name_id = '#' + $(this).attr('id');
            var prop_name_data = $(prop_name_id).val();
            if ($.inArray(prop_name_data, values) > '-1') {
                $(prop_name_id).focus();
                alert("'" + prop_name_data + "' value must be unique.");
                flag = false; // <-- stops the loop
                return flag;
            } else {
                values.push(prop_name_data);
            }
        });
        if (flag == false) {
            return flag;
        }

        //Check for display name

        $('#add > tbody  > tr .label_name').each(function() {
            var label_name_id = '#' + $(this).attr('id');
            var get_label_name_data = $(label_name_id).val();
            if ($.trim(get_label_name_data) == '') {
                $(label_name_id).focus();
                alert("Display name can not be left blank");
                flag = false;
                return flag;
            }
        });
        if (flag == false) {
            return flag;
        }

        if (flag) {
            $.loader({className: "blue-with-image-2", content: ''});
            request({
                url: "<?php echo new moodle_url('/repository/andresco_cmis/util/validateprop.php'); ?>",
                form: jQuery("#Row_form")[0],
                data: {rid: "<?php echo $rid; ?>"
                },
                method: "POST",
                success: function(response) {
                    $.loader('close');
                    var data = JSON.parse(response);
                    var total_data = data.length;
                    if (total_data > 0) {
                        if (data[0] == "ERROR") {
                            $('#global_error').show();
                        } else {
                            $('#err2').html('');
                            var html = '<ul>';
                            for (var i = 0;
                                    i < total_data;
                                    i++) {
                                html += '<li style="color:red;">' + data[i] + '</li>';
                            }
                            html += '</ul>';
                            $('#show_error').show();
                            $('#err2').append(html);
                        }
                    } else {
                        $('#show_error').hide();
                        $('#err2').html('');
//                        if (val == "2") {
//                            document.Row_form.submit();
//                        }
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    $.loader('close');
                }

            }
            );
        }
    }
</script>



