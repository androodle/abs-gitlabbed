<?php

/** 
 * Androgogic Catalogue Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new location
 *
 **/

global $OUTPUT;
require_capability('block/androgogic_catalogue:edit', $context);
require_once('location_edit_form.php');
$mform = new location_edit_form();
if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot . '/blocks/androgogic_catalogue/index.php?tab=location_search');
}

echo $OUTPUT->header();
print_tabs($tabs, $currenttab);

if ($data = $mform->get_data()) {
    $data->created_by = $USER->id;
    $data->date_created = date('Y-m-d H:i:s');
    $newid = $DB->insert_record('androgogic_catalogue_locations',$data);
    echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_catalogue'), 'notifysuccess');
    echo $OUTPUT->action_link($PAGE->url, 'Create another item');
} else {
    echo $OUTPUT->heading(get_string('location_new', 'block_androgogic_catalogue'));
    $mform->display();
}
