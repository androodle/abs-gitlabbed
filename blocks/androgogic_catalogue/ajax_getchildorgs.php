<?php

require_once('../../config.php');
require_once('lib.php');

$id = required_param('id', PARAM_INT);

$ret = array();
if ($id) {
    $org = $DB->get_record('org', array('id' => $id), '*', MUST_EXIST);
    $sql = "SELECT id FROM {org} WHERE path LIKE '{$org->path}/%'";
    $rows = $DB->get_records_sql($sql);
    if ($rows) {
        foreach ($rows as $row) {
            $ret[] = $row->id;
        }
    }
}
echo json_encode($ret);