<?php

defined('MOODLE_INTERNAL') || die(); 

if ($ADMIN->fulltree) {
    
    $settings->add(new admin_setting_configcheckbox(
        'catalogue_allow_guest_access', 
        get_string('allow_guest_access', 'block_androgogic_catalogue'),
        get_string('allow_guest_access_explanation', 'block_androgogic_catalogue'), 
        0
    ));
    
    $settings->add(new admin_setting_configcheckbox(
        'catalogue_hide_search_form', 
        get_string('hide_search_form', 'block_androgogic_catalogue'),
        get_string('hide_search_form_explanation', 'block_androgogic_catalogue'), 
        0
    ));
    
}
