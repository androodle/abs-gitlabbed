<?php

/**
 * Androgogic Catalogue Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the catalogue_entries
 *
 * */
global $OUTPUT;
require_capability('block/androgogic_catalogue:edit', $context);
require_once('catalogue_entry_edit_form.php');

$id = optional_param('id', null, PARAM_INT);
$returnurl = new moodle_url('/blocks/androgogic_catalogue/index.php', array('tab' => 'catalogue_entry_search'));
$entry = new stdClass();

if (!empty($id)) {
    $entry = catalogue_load_entry_form_data($id);
    if (!$entry) {
        print_error('entrynotfound', 'block_androgogic_catalogue');
    }
}

$editoroptions = array(
    'maxfiles' => EDITOR_UNLIMITED_FILES, 
    'maxbytes' => $CFG->maxbytes, 
    'forcehttps' => false,
    'subdirs' => true,
    'context' => $context,
);
$mform = new catalogue_entry_edit_form(null, array('editoroptions' => $editoroptions));
$entry = file_prepare_standard_editor(
    $entry, 
    'description', 
    $editoroptions, 
    $context, 
    'block_androgogic_catalogue', 
    'catalogue_entry_description', 
    $id
);

$mform->set_data($entry);

if ($mform->is_cancelled()) {
    redirect($returnurl);
}

echo $OUTPUT->header();
print_tabs($tabs, $currenttab);

if ($data = $mform->get_data()) {
    
    $id = catalogue_entry_save($data);
    
    $data = file_postupdate_standard_editor(
        $data, 
        'description', 
        $editoroptions, 
        $context, 
        'block_androgogic_catalogue', 
        'catalogue_entry_description', 
        $id
    );
    $DB->set_field('androgogic_catalogue_entries', 'description', $data->description, array('id' => $id));
    
    echo $OUTPUT->notification(get_string('datasubmitted', 'block_androgogic_catalogue'), 'notifysuccess');
    echo $OUTPUT->footer();
    die;
}

echo $OUTPUT->heading(get_string('catalogue_entry_edit', 'block_androgogic_catalogue'));

$mform->display();

// Require jquery and plugin(s)
local_js();
$PAGE->requires->js('/blocks/androgogic_catalogue/jquery.multi-select.js');
$PAGE->requires->js('/blocks/androgogic_catalogue/jquery.quicksearch.js');
$PAGE->requires->js('/blocks/androgogic_catalogue/multi-select.js');

