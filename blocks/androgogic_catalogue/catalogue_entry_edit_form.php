<?php

/** 
 * Androgogic Catalogue Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class catalogue_entry_edit_form extends moodleform {
    
    protected $catalogue_entry;

    function definition() {
        
        global $DB, $id;
        
        $mform =& $this->_form;
        
        $mform->addElement('html','<div>');

        $mform->addElement('text', 'name', get_string('name','block_androgogic_catalogue'), array('size'=>100));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', get_string('required'), 'required', null, 'server');
        $mform->addRule('name', 'Maximum 100 characters', 'maxlength', 100, 'client');

        $mform->addElement('date_selector', 'end_date', get_string('end_date','block_androgogic_catalogue'));

        $mform->addElement('editor', 'description_editor', get_string('description','block_androgogic_catalogue'), null, $this->_customdata['editoroptions']);
        $mform->setType('description_editor', PARAM_RAW);
        
        $mform->addElement('selectyesno', 'public', get_string('public','block_androgogic_catalogue'));

        $options = $DB->get_records_menu('androgogic_catalogue_locations',array(),'name','id,name');
        $select = $mform->addElement('select', 'location_id', get_string('location','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $sql = "select id, fullname from mdl_course where visible = 1 and id != 1 ORDER BY fullname";
        $results = $DB->get_records_sql_menu($sql);
        $select = $mform->addElement('select', 'course_id', get_string('course', 'block_androgogic_catalogue'), $results,array('class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('prog',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'program_id', get_string('program','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('org',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'organisation_id', get_string('organisation','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('pos',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'position_id', get_string('position','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('comp',array(),'fullname','id,fullname');
        $select = $mform->addElement('select', 'competency_id', get_string('competency','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $options = $DB->get_records_menu('cohort',array(),'name','id,name');
        $select = $mform->addElement('select', 'cohort_id', get_string('cohort','block_androgogic_catalogue'), $options,array('size'=>10, 'class'=>'multiselect'));
        $select->setMultiple(true);

        $mform->addElement('hidden', 'tab', 'catalogue_entry_edit');
        $mform->setType('tab', PARAM_TEXT);
        
        if (!empty($id)) {
            $mform->addElement('hidden','id', $id);
            $mform->setType('id', PARAM_INT);
        }
        
        $this->add_action_buttons(true);
        
        $mform->addElement('html','</div>');
    }

    function add_action_buttons ($cancel = true, $submitlabel=null) {
        global $id;
        if (is_null($submitlabel)){
            $submitlabel = get_string('savechanges');
        }
        $mform =& $this->_form;
        if ($cancel) {
            //when two elements we need a group
            $buttonarray=array();
            $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
            $buttonarray[] = &$mform->createElement('cancel');
            if (!isset($id)) {
                $buttonarray[] = &$mform->createElement('reset', 'resetbutton', get_string('reset'),  ' onclick="javascript:return multireset();"');
            }
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
            $mform->closeHeaderBefore('buttonar');
        } else {
            //no group needed
            $mform->addElement('submit', 'submitbutton', $submitlabel);
            $mform->closeHeaderBefore('submitbutton');
        }
    }

    function get_editor_options() {
        return $this->_customdata['editoroptions'];
    }

}
