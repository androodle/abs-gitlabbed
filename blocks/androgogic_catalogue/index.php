<?php
/** 
 * Androgogic Catalogue Block: Index
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

require_once('../../config.php');
require_once('lib.php');
require_once("$CFG->dirroot/totara/core/js/lib/setup.php");

$currenttab = optional_param('tab', 'catalogue_entry_search', PARAM_FILE);
$id = optional_param('id', null, PARAM_INT);

$context = context_system::instance();
$PAGE->set_context($context);
$urlparams = array('tab' => $currenttab);
if (isset($id)) {
    $urlparams['id'] = intval($id);
}

$PAGE->set_url("$CFG->wwwroot/blocks/androgogic_catalogue/index.php", $urlparams);
$PAGE->set_title(get_string($currenttab, 'block_androgogic_catalogue'));
$PAGE->requires->css('/blocks/androgogic_catalogue/multi-select.css');
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$navigation = $PAGE->navbar->add(get_string('catalogue_entry_search','block_androgogic_catalogue'), "$CFG->wwwroot/blocks/androgogic_catalogue/index.php");

if ($currenttab != 'catalogue_entry_search'){
    require_login();
}

//show tabs
include('tabs.php');

if (file_exists($CFG->dirroot.'/blocks/androgogic_catalogue/'.$currenttab.'.php')) {
    include $currenttab.'.php';
}

echo $OUTPUT->footer();
