<?php
/** 
 * Androgogic Catalogue Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/
function user_can_see_catalogue_entry($catalogue_entry, $user_data){
    
    global $DB;
    
    // any org entries for this cat_entry?
    $q = "SELECT o.* 
        FROM {androgogic_catalogue_entry_organisations} co 
        INNER JOIN {org} o ON co.organisation_id = o.id 
        WHERE co.catalogue_entry_id = ?";
    $cat_org_result = $DB->get_records_sql($q, array($catalogue_entry->id));
    
    // any pos entries for this cat_entry?
    $q = "SELECT o.* 
        FROM {androgogic_catalogue_entry_positions} co 
        INNER JOIN {pos} o ON co.position_id = o.id 
        WHERE co.catalogue_entry_id = ?";
    $cat_pos_result = $DB->get_records_sql($q, array($catalogue_entry->id));
    
    // any cohort entries for this cat_entry?
    $q = "SELECT c.* 
        FROM {androgogic_catalogue_entry_cohorts} co 
        INNER JOIN {cohort} c ON co.cohort_id = c.id 
        WHERE co.catalogue_entry_id = ?";
    $cat_cohort_result = $DB->get_records_sql($q, array($catalogue_entry->id));
    
    if (!$cat_org_result && !$cat_pos_result && !$cat_cohort_result){
        //no filters on this catalogue entry, so nothing else to do here
        return true;
    } 
    
    $passed_org_test = false;
    $passed_pos_test = false;
    $passed_cohort_test = false;
    
    if ($cat_org_result) {
        if (!isset($user_data->user_org_result)) {
            //fail: no user org, but we do have cat org, so let's short circuit this
            return false;
        }
        foreach ($user_data->user_org_result as $user_org) {
            // are any of the cat org paths upstream from the user org path?
            foreach ($cat_org_result as $cat_org) {
                //close the paths, in case of false positives
                if (stristr($user_org->path.'/',$cat_org->path.'/')) {
                    //in other words, does the user belong to an organisation that either is the same as the cat org, or is among the descendants of it?
                    $passed_org_test = true;
                }
            }
        }  
    } else {
        $passed_org_test = true;
    }
    
    if ($cat_pos_result) {
        if (!isset($user_data->user_pos_result)){
            //fail: no user pos, but we do have cat pos, so let's short circuit this
            return false;
        }
        foreach ($user_data->user_pos_result as $user_pos) {
            //does the user pos path match any of the cat_pos paths?
            foreach ($cat_pos_result as $cat_pos) {
                if ($user_pos->path == $cat_pos->path){
                    $passed_pos_test = true;
                } elseif(stristr($cat_pos->path,$user_pos->path.'/')) {
                    //in other words, does the user hold a higher position within the hierarchy than the specified catalogue position?
                    $passed_pos_test = true;
                }
            }
        }            
    } else {
        $passed_pos_test = true;
    }
    
    if ($cat_cohort_result) {
        if (!isset($user_data->user_cohort_result)) {
            // fail!
            return false;
        }
        foreach ($user_data->user_cohort_result as $user_cohort) {
            foreach ($cat_cohort_result as $cat_cohort) {
                if ($cat_cohort->id == $user_cohort->cohortid){
                    $passed_cohort_test = true;
                }
            }
        }            
    } else {
        $passed_cohort_test = true;
    }
    
    // if we got down to here then we should have the results of the 2 tests. 
    // if both are true then the user can see, if not, not
    return ($passed_org_test && $passed_pos_test && $passed_cohort_test);
}

function get_user_pos_org_data(){
    
    global $USER,$DB;
    
    //get org data for the user
    $q = "SELECT o.* 
        FROM {pos_assignment} pa
        INNER JOIN {org} o ON pa.organisationid = o.id
        WHERE pa.userid = ? 
            AND pa.type IN (1,2)";
    $user_org_result = $DB->get_records_sql($q, array($USER->id));
    
    //get pos data for the user
    $q = "SELECT p.* 
        FROM {pos_assignment} pa
        INNER JOIN {pos} p ON pa.positionid = p.id
        WHERE pa.userid = ? 
            AND pa.type IN (1,2)";
    $user_pos_result = $DB->get_records_sql($q, array($USER->id));
    
    //get cohort data for the user
    $q = "SELECT cm.* 
        FROM {cohort_members} cm
        INNER JOIN {cohort} c ON cm.cohortid = c.id
        WHERE cm.userid = ?";
    $user_cohort_result = $DB->get_records_sql($q, array($USER->id));
    
    $return = new stdClass();
    $return->user_org_result = $user_org_result;
    $return->user_pos_result = $user_pos_result;
    $return->user_cohort_result = $user_cohort_result;
    
    return $return;
}

function catalogue_load_entry_form_data($id) {
    
    global $DB;
    
    $entry = $DB->get_record('androgogic_catalogue_entries', array('id' => $id));
    
    if (!$entry) {
        return false;
    }
    
    // Preparing for HTML editor.
    $entry->descriptionformat = 1;
    
    // Preparing for calendar input.
    $entry->end_date = strtotime($entry->end_date);
    
    $entry->location_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_locations', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->location_id[] = $row->location_id;
    }
    
    $entry->course_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_courses', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->course_id[] = $row->course_id;
    }
    
    $entry->program_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_programs', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->program_id[] = $row->program_id;
    }
    
    $entry->organisation_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_organisations', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->organisation_id[] = $row->organisation_id;
    }
    
    $entry->position_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_positions', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->position_id[] = $row->position_id;
    }
    
    $entry->competency_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_competencies', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->competency_id[] = $row->competency_id;
    }
    
    $entry->cohort_id = array();
    $result = $DB->get_records('androgogic_catalogue_entry_cohorts', array('catalogue_entry_id' => $id));
    foreach ($result as $row) {
        $entry->cohort_id[] = $row->cohort_id;
    }
    
    return $entry;
}

function catalogue_entry_save($data) {
    
    global $USER, $DB;
    
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    $data->end_date = date('Y-m-d', $data->end_date);
    
    if (!empty($data->id)) {
        $DB->update_record('androgogic_catalogue_entries', $data);
    } else {
        $data->date_created = date('Y-m-d H:i:s');
        $data->created_by = $USER->id;
        $data->id = $DB->insert_record('androgogic_catalogue_entries',$data);
    }
    
    $DB->delete_records('androgogic_catalogue_entry_locations', array('catalogue_entry_id' => $data->id));
    if (isset($data->location_id)) {
        foreach ($data->location_id as $location_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->location_id = $location_id;
            $DB->insert_record('androgogic_catalogue_entry_locations', $insert);
        }
    }
    
    $DB->delete_records('androgogic_catalogue_entry_courses', array('catalogue_entry_id' => $data->id));
    if (isset($data->course_id)) {
        foreach ($data->course_id as $course_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->course_id = $course_id;
            $DB->insert_record('androgogic_catalogue_entry_courses', $insert);
        }
    }
    
    $DB->delete_records('androgogic_catalogue_entry_programs', array('catalogue_entry_id' => $data->id));
    if (isset($data->program_id)) {
        foreach ($data->program_id as $program_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->program_id = $program_id;
            $DB->insert_record('androgogic_catalogue_entry_programs', $insert);
        }
    }
    
    $DB->delete_records('androgogic_catalogue_entry_organisations', array('catalogue_entry_id' => $data->id));
    if (isset($data->organisation_id)) {
        foreach ($data->organisation_id as $organisation_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->organisation_id = $organisation_id;
            $DB->insert_record('androgogic_catalogue_entry_organisations', $insert);
        }
    }
    
    $DB->delete_records('androgogic_catalogue_entry_positions', array('catalogue_entry_id' => $data->id));
    if (isset($data->position_id)) {
        foreach ($data->position_id as $position_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->position_id = $position_id;
            $DB->insert_record('androgogic_catalogue_entry_positions', $insert);
        }
    }
    
    $DB->delete_records('androgogic_catalogue_entry_competencies', array('catalogue_entry_id' => $data->id));
    if (isset($data->competency_id)) {
        foreach ($data->competency_id as $competency_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->competency_id = $competency_id;
            $DB->insert_record('androgogic_catalogue_entry_competencies', $insert);
        }
    }

    $DB->delete_records('androgogic_catalogue_entry_cohorts', array('catalogue_entry_id' => $data->id));
    if (isset($data->cohort_id)) {
        foreach ($data->cohort_id as $cohort_id) {
            $insert = new stdClass();
            $insert->catalogue_entry_id = $data->id;
            $insert->cohort_id = $cohort_id;
            $DB->insert_record('androgogic_catalogue_entry_cohorts', $insert);
        }
    }
    
    return $data->id;
}

function block_androgogic_catalogue_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload) {
    
    global $DB;
    
    $sql = "select * 
        from {files} 
        where itemid = ? 
            and filename = ?
            and contextid = ? 
            and filearea = ?";
    $params = array(
        $args[0],
        $args[1],
        $context->id,
        $filearea,
    );
    
    $file = $DB->get_record_sql($sql, $params);
    
    if (!$file) {
        send_file_not_found();
    } else {
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }

    $forcedownload = true;
    session_get_instance()->write_close();
    send_stored_file($stored_file, 60 * 60, 0, $forcedownload);
}
