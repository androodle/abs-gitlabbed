<?php

/**
 * Androgogic Catalogue Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     15/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search catalogue_entries
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */

//params
$sort = optional_param('sort', 'name', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$tab = optional_param('tab', 'catalogue_entry_search', PARAM_TEXT);

if (!$CFG->catalogue_allow_guest_access) {
    require_login();
}

require_once('catalogue_entry_search_form.php');

echo $OUTPUT->header();
print_tabs($tabs, $currenttab);

$customdata = array(
    'tab' => $tab,
    'sort' => $sort,
    'dir' => $dir,
    'perpage' => $perpage,
);
$mform = new catalogue_entry_search_form(null, $customdata);
if ($data = $mform->get_data()) {
    $search = $data->search;
    $androgogic_catalogue_locations_id = $data->androgogic_catalogue_locations_id;
    $comp_id = $data->comp_id;
    $coursetype = $data->coursetype;
} else {
    $search = optional_param('search', '', PARAM_TEXT);
    $androgogic_catalogue_locations_id = 0;
    $comp_id = 0;
    $coursetype = -1;
}

if (!empty($_POST['startdate']['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']) - 1;
}
if (!empty($_POST['enddate']['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab'));
//figure out the and clause from what has been submitted
$and = '';

$searchsql = addslashes($search);
$searchhtml = htmlspecialchars($search);
if ($search != '') {
    $and .= " and concat(a.name, a.description) like '%{$searchsql}%'";
}
//are we filtering on androgogic_catalogue_locations?
if ($androgogic_catalogue_locations_id > 0) {
    $and .= " and cl.id = $androgogic_catalogue_locations_id ";
}
//are we filtering on comp?
if ($comp_id > 0) {
    $and .= " and comp.id = $comp_id ";
}
//are we filtering on coursetype?
if ($coursetype > -1) {
    $and .= " and c.coursetype = $coursetype ";
}
if (isset($startdate)) {
    $and .= " and (c.startdate > $startdate or p.availablefrom > $startdate)";
}
if (isset($enddate)) {
    $and .= " and (c.startdate < $enddate or p.availablefrom < $enddate)";
}

$is_logged_in = true;
// if they are not logged in, they don't get to see non-public entries, 
// and they can't have the pos or org logic applied, so don't show them these
if (!isset($USER) || $USER->id == 0) {
    $and .= " AND a.public = 1 
        AND o.id IS NULL 
        AND pos.id IS NULL 
        AND coh.id IS NULL ";
    $is_logged_in = false;
}
else {
    $user_pos_org_data = get_user_pos_org_data();
}
$joined_tables = "FROM {androgogic_catalogue_entries} a 
    LEFT JOIN {androgogic_catalogue_entry_locations} cel ON a.id = cel.catalogue_entry_id
    LEFT JOIN {androgogic_catalogue_locations} cl ON cel.location_id = cl.id
    LEFT JOIN {androgogic_catalogue_entry_courses} cec ON a.id = cec.catalogue_entry_id
    LEFT JOIN {course} c ON cec.course_id = c.id
    LEFT JOIN {androgogic_catalogue_entry_programs} cep ON a.id = cep.catalogue_entry_id
    LEFT JOIN {prog} p ON cep.program_id = p.id
    LEFT JOIN {androgogic_catalogue_entry_organisations} ceo ON a.id = ceo.catalogue_entry_id
    LEFT JOIN {org} o ON ceo.organisation_id = o.id
    LEFT JOIN {androgogic_catalogue_entry_positions} cepos ON a.id = cepos.catalogue_entry_id
    LEFT JOIN {pos} pos ON cepos.position_id = pos.id
    LEFT JOIN {androgogic_catalogue_entry_competencies} cecomp ON a.id = cecomp.catalogue_entry_id
    LEFT JOIN {comp} comp ON cecomp.competency_id = comp.id
    LEFT JOIN {androgogic_catalogue_entry_cohorts} cecoh ON a.id = cecoh.catalogue_entry_id
    LEFT JOIN {cohort} coh ON cecoh.cohort_id = coh.id";

$q = "SELECT DISTINCT a.*  
    $joined_tables
    WHERE 1 = 1
    $and
    ORDER BY $sort $dir";

//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);

//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
    $joined_tables
    WHERE 1 = 1
    $and";

$result_count = $DB->get_field_sql($q);

if (!$CFG->catalogue_hide_search_form) {
    $mform->display();
}

//only show the result count if they have edit - if not the results may be filtered by org/pos
if (has_capability('block/androgogic_catalogue:edit', $context)) {
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('catalogue_entry_plural', 'block_androgogic_catalogue') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    echo "<a href='index.php?tab=catalogue_entry_edit'>" . get_string('catalogue_entry_new', 'block_androgogic_catalogue') . "</a>";
    echo '</td></tr></table>';
}
flush();

if (!$results) {
    $match = array();
    echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_catalogue', $searchhtml));
    echo $OUTPUT->footer();
    die;
}

foreach ($results as $result) {

    $edit_link = "";
    $delete_link = "";
    if (has_capability('block/androgogic_catalogue:edit', $context) || has_capability('block/androgogic_catalogue:delete', $context)){
        if (has_capability('block/androgogic_catalogue:edit', $context)) {
            $url = new moodle_url('/blocks/androgogic_catalogue/index.php', array('tab' => 'catalogue_entry_edit', 'id' => $result->id));
            $edit_link = '<a href="'.$url.'">'.get_string('edit').'</a> ';
        }
        if (has_capability('block/androgogic_catalogue:delete', $context)) {
            $url = new moodle_url('/blocks/androgogic_catalogue/index.php', array('tab' => 'catalogue_entry_delete', 'id' => $result->id));
            $delete_link = '<a href="'.$url.'" onclick="javascript:return confirm(\'Are you sure you want to delete this item?\')">'.get_string('delete').'</a> ';
        }
    }
    elseif ($is_logged_in){
        //we may need to exclude the user from seeing the entry, based on org or pos
        if (!user_can_see_catalogue_entry($result, $user_pos_org_data)){
            continue;
        }
    }
    echo '<h3>' . $result->name . '</h3>';
    if($edit_link != "" || $delete_link != ""){
        echo $edit_link . $delete_link . '<br>';
    }

    $result->description = file_rewrite_pluginfile_urls(
        $result->description, 
        'pluginfile.php', 
        $context->id, 
        'block_androgogic_catalogue',  
        'catalogue_entry_description', 
        $result->id
    );
    echo format_text($result->description, FORMAT_HTML) . '<br/>';

    //get any courses that are linked to the cat entry
    $sql = "SELECT c.* 
        FROM {androgogic_catalogue_entries} a
        LEFT JOIN {androgogic_catalogue_entry_courses} cc on a.id = cc.catalogue_entry_id
        LEFT JOIN {course} c on cc.course_id = c.id
        WHERE a.id = ?";
    $courses = $DB->get_records_sql($sql, array($result->id));
    if (!empty($courses)) {            
        foreach ($courses as $course) {
            // Redirect course with learning plans to an intermediate page that allows the user to set up or select a plan to use.
            $enrol_methods = $DB->get_fieldset_select('enrol', 'enrol', 'courseid = ?', array('courseid' => $course->id));
            $has_self_enrolment = in_array('self', $enrol_methods);
            $has_lp_enrolment = in_array('totara_learningplan', $enrol_methods);

            if ($has_lp_enrolment & !$has_self_enrolment) {
                $url = new moodle_url('/blocks/androgogic_catalogue/add_to_learningplan.php', array('courseid' => $course->id));
                echo '<a href="' . $url . '">' . $course->fullname . '</a><br>';
            } else {
                $url = new moodle_url('/course/view.php', array('id' => $course->id));
                echo '<a href="' . $url . '">' . $course->fullname . '</a><br>';
            }
        }
    }

    //get any progs that are linked to the cat entry
    $sql = "SELECT p.* 
        FROM {androgogic_catalogue_entries} a
        LEFT JOIN {androgogic_catalogue_entry_programs} cp on a.id = cp.catalogue_entry_id
        LEFT JOIN {prog} p on cp.program_id = p.id 
        WHERE a.id = ?";
    $programs = $DB->get_records_sql($sql, array($result->id));
    if (!empty($programs)) {
        foreach ($programs as $program) {
            $url = new moodle_url('/totara/program/view.php', array('id' => $program->id));
            echo '<a href="' . $url . '">' . $program->fullname . '</a><br>';
        }
    }
}

$pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
