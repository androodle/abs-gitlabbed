<?php

/**
 * Androgogic Catalogue Learning Plan Handling
 * 
 * Add a course to a users learning plan.
 *  
 * @copyright	2012+ Androgogic Pty Ltd
 * @author		Greg Newton
 * 
 */
global $CFG, $PAGE, $USER;

require('../../config.php');
require('../../totara/plan/lib.php');


$courseid = required_param('courseid', PARAM_INT);
$plan_id = optional_param('plan', NULL, PARAM_ALPHANUM);
$plan_name = optional_param('plan_name', NULL, PARAM_TEXT);

require_capability('totara/plan:accessplan', get_system_context());
$site = get_site();
$context = get_context_instance(CONTEXT_SYSTEM);
require_login($site);

// If the user is already enroled in the course, take them straight there.
$existing_enrolment = $DB->get_record_sql('
    select  ue.userid, e.courseid
    from    mdl_user_enrolments ue
            inner join mdl_enrol e on e.id = ue.enrolid
    where   ue.userid = ?
        and e.courseid = ?',
    array('userid' => $USER->id, 'courseid' => $courseid));
if ($existing_enrolment) {
    redirect("{$CFG->wwwroot}/course/view.php?id={$courseid}");
}

if (!empty($plan_id)) {
    // We are adding a course to a plan.

    if ($plan_id == 'new') {
        // We are adding a new Plan
        
        // Chunks of code lifted from /totara/plan/add.php and modified.
                
        $template = $DB->get_record('dp_template', array('isdefault' => 1));
                
        $transaction = $DB->start_delegated_transaction();
        
        $data = new stdClass();
        $data->userid = $USER->id;
        $data->templateid = $template->id;
        $data->name = $plan_name;
        $data->status = 0; // Makes no sense to me but copied from Totara
        $data->enddate = totara_date_parse_from_format(get_string('datepickerparseformat', 'totara_core'), $data->enddate);  // convert to timestamp
        // Set up the plan
        $newid = $DB->insert_record('dp_plan', $data);
        $data->id = $newid;
        $plan = new development_plan($newid);
        // Update plan status and plan history
        $plan->set_status(DP_PLAN_STATUS_UNAPPROVED, DP_PLAN_REASON_CREATE);
        if ($plan->get_component('competency')->get_setting('enabled')) {
            // Auto-assign competencies
            $competencycomponent = $plan->get_component('competency');
            if ($competencycomponent->get_setting('autoassignorg')) {
                // From organisation
                if (!$competencycomponent->assign_from_org()) {
                    totara_set_notification(get_string('plancreatefail', 'totara_plan', get_string('unabletoassigncompsfromorg', 'totara_plan')), $currenturl);
                }
            }
            if ($competencycomponent->get_setting('autoassignpos')) {
                // From position
                if (!$competencycomponent->assign_from_pos()) {
                    totara_set_notification(get_string('plancreatefail', 'totara_plan', get_string('unabletoassigncompsfrompos', 'totara_plan')), $currenturl);
                }
            }
            unset($competencycomponent);
        }
        $transaction->allow_commit();

        // Send out a notification?
        if ($plan->is_active()) {
            if ($role == 'manager') {
                $plan->send_alert(true,'learningplan-update.png','plan-add-learner-short','plan-add-learner-long');
            }
        }
#        $data = file_postupdate_standard_editor($data, 'description', $TEXTAREA_OPTIONS, $TEXTAREA_OPTIONS['context'], 'totara_plan', 'dp_plan', $data->id);
#        $DB->set_field('dp_plan', 'description', $data->description, array('id' => $data->id));
        $viewurl = "{$CFG->wwwroot}/totara/plan/view.php?id={$newid}";
        add_to_log(SITEID, 'plan', 'created', "view.php?id={$newid}", $plan->name);
        totara_set_notification(get_string('plancreatesuccess', 'totara_plan'), NULL, array('class' => 'notifysuccess'));
        
        $plan_id = $newid;
    }
    
    // Add course to the learning plan
    $plan = new development_plan($plan_id);
    
    $componentname = 'course';
    $component = $plan->get_component($componentname);
    $currentlist = $component->get_assigned_items();

    $full = array();
    $current_course_ids = array();
    foreach ($currentlist as $rec) {
        $full[] = $rec->courseid;
        $current_course_ids[] = $rec->courseid;
    }
    if (!in_array($courseid, $current_course_ids)) {
        $full[] = $courseid;
    }
    
    ///
    /// Permissions check
    ///

    if (!$component->can_update_items()) {
        notice(get_string('error:cannotupdateitems', 'totara_plan'));
    }

    ///
    /// Update component
    ///
    $component->update_assigned_items($full);

    // Redirect to the plan when done.
    $viewurl = "{$CFG->wwwroot}/totara/plan/view.php?id={$plan_id}&c=course";
    redirect($viewurl);
}

$url = new moodle_url('/blocks/androgogic_catalogue/add_to_learningplan.php', array('courseid' => $courseid));
$str_add_to_lp = "Add course to Learning Plan";

$PAGE->set_url($url);
$PAGE->set_context($context);
#$PAGE->set_pagelayout('report');
#$PAGE->set_pagelayout('standard');
$PAGE->set_title("$site->shortname: $str_add_to_lp");
$PAGE->set_heading($site->fullname);

#require_capability('moodle/role:assign', $context);
#require_capability('moodle/site:uploadusers', $context);
#require_capability('moodle/site:viewparticipants', $context);


$PAGE->navbar->add( $str_add_to_lp );

$existing_learning_plans = $DB->get_records('dp_plan', array('userid' => $USER->id));

echo $OUTPUT->header();
echo $OUTPUT->box_start('generalbox');

echo $OUTPUT->heading("Add course to a learning plan");

echo 'To enrol in this course, you will need to add it to a learning plan and have it approved by your manager.';

echo '<form action="" method="post">';
echo '<ul style="list-style-type: none;">';
$first_valid = true;
foreach ($existing_learning_plans as $plan) {
    if ($plan->status != 100) {
        $checked = '';
        if ($first_valid) {
            $checked = ' checked';
            $first_valid = false;
        }
        echo '<li>';
        echo '<input type="radio" id="plan_' . $plan->id . '" name="plan" value="' . $plan->id . '"' . $checked . '>';
        echo '<label for="plan_' . $plan->id . '">' . $plan->name . '</label>';
        echo '</li>';
    } else {
        echo '<li>';
        echo '<input type="radio" id="plan_' . $plan->id . '" name="plan" value="' . $plan->id . '" disabled>';
        echo '<label for="plan_' . $plan->id . '">' . $plan->name . ' (Completed)</label>';
        echo '</li>';
    }
}
echo '<li>';
echo '<input type="radio" id="plan_new" name="plan" value="new">';
echo '<label for="plan_new">New Learning Plan</label>';
echo '<br>';
echo '<label for="plan_name">Plan name</label> ';
echo '<input type="text" id="plan_name" name="plan_name" length="30">';
echo '</li>';
echo '</ul>';
echo '<input type="hidden" name="courseid" value="' . $courseid . '">';
echo '<input type="submit" name="action" value="Add">';
echo '</form>';

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
