<?php

/** 
 * Androgogic Catalogue Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class location_edit_form extends moodleform {

    protected $location;

    function definition() {
        global $USER, $courseid, $DB, $PAGE, $id;
        $mform =& $this->_form;
        $context = context_system::instance();
        if ($id) {
            $q = "select a.*
            from mdl_androgogic_catalogue_locations a
            where a.id = $id ";
            $location = $DB->get_record_sql($q);
        } else {
            $location = $this->_customdata['$location']; // this contains the data of this form
        }
        $tab = 'location_new'; // from whence we were called
        if (!empty($location->id)) {
            $tab = 'location_edit';
        }
        $mform->addElement('html','<div>');

        //name
        $mform->addElement('text', 'name', get_string('name','block_androgogic_catalogue'), array('size'=>100));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', 'Maximum 50 characters', 'maxlength', 100, 'client');

        //hiddens
        $mform->addElement('hidden','tab',$tab);
        $mform->setType('tab', PARAM_TEXT);

        if (isset($_REQUEST['id'])){
            $mform->addElement('hidden','id',$id);
            $mform->setType('id', PARAM_INT);
        }

        $this->add_action_buttons(true);
        $mform->addElement('html','</div>');
    }
}
