<?php

function xmldb_block_androgogic_catalogue_upgrade($oldversion=0) {
    
    global $CFG, $DB;
    
    $result = true;
    $dbman = $DB->get_manager();
    
    if ($oldversion < 2015062300) {
        
        $table = new xmldb_table('androgogic_catalogue_entry_cohorts');
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('catalogue_entry_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('cohort_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_index('unq_catalogue_entry_cohort', XMLDB_INDEX_UNIQUE, array('catalogue_entry_id', 'cohort_id'));
        
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        
    }
    
    return $result;
}

