<?php
/** 
 * Androgogic Catalogue Block: Language Pack
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

$string['allow_guest_access'] = 'Allow guest access';
$string['allow_guest_access_explanation'] = 'Allow users who are not logged in to view the catalogue entries.';
$string['androgogic_catalogue:view'] = 'View Catalogue entries';
$string['hide_search_form'] = 'Hide search form';
$string['hide_search_form_explanation'] = 'Hides the search form from the catalogue entries page.<br><b>Note</b> that the search parameters passed via URL will still work.';
$string['pluginname'] = 'Androgogic Catalogue';
$string['plugintitle'] = 'Androgogic Catalogue';

//spares
$string['noresults'] = 'There were no results from your search';
$string['startdate'] = 'Course or program starts between: ';
$string['enddate'] = 'and';

//catalogue_entry items
$string['catalogue'] = 'Catalogue';
$string['catalogue_entry'] = 'Catalogue Entry';
$string['name'] = 'Name';
$string['end_date'] = 'End Date';
$string['description'] = 'Description';
$string['public'] = 'Public';
$string['location'] = 'Location';
$string['course'] = 'Course';
$string['cohort'] = 'Audience';
$string['program'] = 'Program';
$string['organisation'] = 'Organisation';
$string['position'] = 'Position';
$string['capability_element'] = 'Capability Element';
$string['catalogue_entry_search'] = 'Catalogue Entries';
$string['catalogue_entry_plural'] = 'Catalogue Entries';
$string['catalogue_entry_new'] = 'New Catalogue Entry';
$string['catalogue_entry_edit'] = 'Edit Catalogue Entry';
$string['catalogue_entry_delete'] = 'Delete Catalogue Entry';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['androgogic_catalogue:view'] = 'View and search objects within the Androgogic Catalogue block';
$string['androgogic_catalogue:edit'] = 'Edit objects within the Androgogic Catalogue block';
$string['androgogic_catalogue:delete'] = 'Delete objects within the Androgogic Catalogue block';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';

//location items
$string['location_search'] = 'Locations';
$string['location_plural'] = 'Locations';
$string['location_new'] = 'New Location';
$string['location_edit'] = 'Edit Location';
$string['location_delete'] = 'Delete Location';
$string['competency'] = 'Competency';
$string['catalogue_entry_search_instructions'] = 'Search by Name or Description';
$string['location_search_instructions'] = 'Search by Name';
$string['entrynotfound'] = 'Catalogue entry not found.';
