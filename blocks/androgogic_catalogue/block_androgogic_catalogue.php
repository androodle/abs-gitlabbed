<?php
/** 
 * Androgogic Catalogue Block: Class
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     13/05/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

class block_androgogic_catalogue extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_androgogic_catalogue');
		$this->cron = 1;
	}

	function get_content(){
		global $CFG;		
        
        if ($this->content !== null) {
			return $this->content;
		}
        
		$this->content = new stdClass;
        $url = new moodle_url('/blocks/androgogic_catalogue/index.php');
		$this->content->text = '<a href="'.$url.'">'.get_string('catalogue_entry_search','block_androgogic_catalogue').'</a>';

		return $this->content;
	}

    function has_config() {
        return true;
    }

}
