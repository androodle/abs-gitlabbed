<?php
/** 
 * Androgogic Training History Block: Index
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

require_once('../../config.php');
require_once('lib.php');
global $CFG,$USER,$DB,$PAGE, $OUTPUT;
//expected params
$currenttab = optional_param('tab', 'training_history_search', PARAM_TEXT);
$context = get_context_instance(CONTEXT_SYSTEM);
$PAGE->set_context($context);
$PAGE->set_url("$CFG->wwwroot/blocks/androgogic_training_history/index.php", array('tab'=>$currenttab));
$PAGE->set_title(get_string($currenttab, 'block_androgogic_training_history'));
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_training_history'));
$navlinks[] = array('name' => get_string('training_history_search','block_androgogic_training_history'), 'link' => "$CFG->wwwroot/blocks/androgogic_training_history/index.php", 'type' => 'misc');
$navigation = build_navigation($navlinks);
require_login();
echo $OUTPUT->header();
//show tabs
include('tabs.php');
// include current page
include $currenttab.'.php';
 echo $OUTPUT->footer();

// End of blocks/androgogic_training_history/index.php