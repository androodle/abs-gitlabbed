<?php

/** 
 * Androgogic Training History Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new training_history
 *
 **/

global $OUTPUT;

require_once('training_history_edit_form.php');
$mform = new training_history_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
if(!isset($data->approved)){
    $data->approved = 0;
}
else if(block_androgogic_training_history_is_manager() && $data->user_id == $USER->id){
    //can't approve your own th!
    $data->approved = 0;
    echo $OUTPUT->notification(get_string('cantapproveowntraininghistory','block_androgogic_training_history'), 'notifyfailure');
}
//fix file entries
$sql = "UPDATE mdl_files
SET filearea = 'content',component='block_androgogic_training_history'
WHERE itemid = '$data->file_id'";
$DB->execute($sql);
$newid = $DB->insert_record('androgogic_training_history',$data);
if($CFG->androgogic_training_history_approval_workflow){
    $data->id = $newid;
    block_androgogic_training_history_advise_manager($data);
}
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('training_history_new', 'block_androgogic_training_history'));
$mform->display();
}

?>
