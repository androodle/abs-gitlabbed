<?php

/** 
 * Androgogic User period Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the user_periods
 *
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('user_period_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_user.firstname || ' ' || mdl_user.lastname as user 
from mdl_andro_user_period a 
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
where a.id = $id ";
$user_period = $DB->get_record_sql($q);
$mform = new user_period_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$DB->update_record('andro_user_period',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('user_period_edit', 'block_androgogic_training_history'));
$mform->display();
}

?>
