<?php

/** 
 * Androgogic Support Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class server_status_edit_form extends moodleform {
protected $server_status;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_androgogic_server_status a 
where a.id = {$_REQUEST['id']} ";
$server_status = $DB->get_record_sql($q);
}
else{
$server_status = $this->_customdata['$server_status']; // this contains the data of this form
}
$tab = 'server_status_new'; // from whence we were called
if (!empty($server_status->id)) {
$tab = 'server_status_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_support'), array('size'=>50));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');

//summary
$editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
$mform->addElement('editor', 'summary', get_string('summary','block_androgogic_support'), null, $editoroptions);
$mform->setType('summary', PARAM_CLEANHTML);
$mform->addRule('summary', get_string('required'), 'required', null, 'server');

//active
$mform->addElement('selectyesno', 'active', get_string('active','block_androgogic_support'));
$mform->addRule('active', get_string('required'), 'required', null, 'server');

//listing_order
$mform->addElement('text', 'listing_order', get_string('listing_order','block_androgogic_support'), array('size'=>5));
$mform->addRule('listing_order', get_string('required'), 'required', null, 'server');
$mform->setType('listing_order', PARAM_INT);

//set values if we are in edit mode
if (!empty($server_status->id) && isset($_GET['id'])) {
$mform->setConstant('name', $server_status->name);
$summary['text'] = $server_status->summary;
$summary['format'] = 1;
$mform->setDefault('summary', $summary);
$mform->setConstant('active', $server_status->active);
$mform->setConstant('listing_order', $server_status->listing_order);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons();
$mform->addElement('html','</div>');
}
}
