<?php

/**
 *-----------------------------------------------------------------------------
 * Custom ABS Logout page
 *-----------------------------------------------------------------------------
 * Developed by Androgogic Pty Ltd
 * for the Australian Burea of Statistics 
 *-----------------------------------------------------------------------------
 **/

include_once("../../config.php");

global $CFG, $PAGE, $OUTPUT;

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

// get wantsurl from session and pass to the samlUrl
$samlUrl = "index.php";
if(isset($SESSION->wantsurl)) {
    $samlUrl = $samlUrl . "?wantsurl=" . urlencode($SESSION->wantsurl);
}

$context = CONTEXT_SYSTEM::instance();
$PAGE->set_url("$CFG->httpswwwroot/auth/saml/login.php");
$PAGE->set_context($context);
$PAGE->set_pagelayout('login');

/// Define variables used in page
$site = get_site();

$loginsite = get_string("loginsite");
$PAGE->navbar->add($loginsite);
$PAGE->set_title("$site->fullname: $loginsite");
$PAGE->set_heading("$site->fullname");

echo $OUTPUT->header();

echo '<h1 style="text-align: center; padding-top: 2em">'.get_string("logoutmessage", "auth_saml").'</h1>';
echo '<h3 style="text-align: center; padding-top: 0.5em">'.get_string("logoutmessagelink", "auth_saml", $CFG->wwwroot).'</h3>';

echo $OUTPUT->footer();
