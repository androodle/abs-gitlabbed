<?php

$plugin->version = 2014112401;
$plugin->requires = 2013040500;
$plugin->release = 2013110701;
$plugin->maturity = 100;

// BEGIN: CLIENT-SPECIFIC CHANGE: RETAIN
// This plugin has been customised by Androgogic Pty Ltd 
// for the Australian Bureau of Statistics. All customisations
// are tagged like this with BEGIN ... END tags.
// END: CLIENT-SPECIFIC CHANGE: RETAIN

