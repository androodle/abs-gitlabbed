<?php

/**
* Facetoface Block: Search form
*
* @author Daniel Morphett
* @version 11/09/2014
* @copyright 2014+ Androgogic Pty Ltd
*
* Provides search form for the object.
* This is used by search page
*
**/
if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.'); /// It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class facetoface_webservice_log_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$s = required_param('s', PARAM_INT);
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('facetoface_webservice_log_search_instructions', 'mod_facetoface'));
//$dboptions = $DB->get_records_menu('user',array(),'lastname','id,concat(firstname,\' \',lastname) as fullname');
//unset($options);
//$options[0] = 'Any';
//foreach($dboptions as $key=>$value){
//$options[$key] = $value;
//}
//$select = $mform->addElement('select', 'user_id', get_string('user','mod_facetoface'), $options);
$dboptions = $DB->get_records_menu('facetoface',array(),'name','id,name');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'facetoface_sessions_id', get_string('facetoface_sessions','mod_facetoface'), $options);
//hiddens
$mform->addElement('hidden','s',$s);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}