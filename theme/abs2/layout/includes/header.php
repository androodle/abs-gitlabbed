<header role="banner" class="navbar navbar-fixed-top moodle-has-zindex">
    <div id="page-header" class="clearfix">
        <div id="page-header-wrapper" class="container-fluid">
            <ul class="nav pull-right">
                <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
                <li class="navbar-text">
                    <?php echo $OUTPUT->login_info() ?> 
                </li>
                <li class="user-picture"><?php if (isloggedin()) {echo $OUTPUT->user_picture($USER, array('size'=>20));}?></li>
            </ul>
            <div class="logo">
                <a href="<?php echo $CFG->wwwroot; ?>"> <img src="<?php echo $logourl; ?>" alt="<?php echo 'Link to ', $SITE->fullname, ' homepage';?>" title="<?php echo 'Link to ', $SITE->fullname, ' homepage';?>"/></a>
            </div>
            <?php echo $html->bannertext; ?>
            <div class="custom_search_box">
                <?php if (isloggedin()) {
                    $catalogue_search_form = "<form action='" . $CFG->wwwroot . "/blocks/androgogic_catalogue/index.php' method='post'>
                        <label for='catalogue_search_text_box'>Search Courses:</label>
                        <input type='text' placeholder='Search courses' id='catalogue_search_text_box' name='search'>
                    </form>";
                    echo $catalogue_search_form;}
                ?>
            </div>
        </div>
        <nav role="navigation" class="navbar-inner">
            <div class="container-fluid">
                <a class="brand" href="<?php echo $CFG->wwwroot;?>"><?php echo $SITE->shortname; ?></a>
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse collapse">
                    <?php if ($showmenu) { ?>
                        <?php if ($hascustommenu) { ?>
                        <div id="custommenu"><?php echo $custommenu; ?></div>
                        <?php } else { ?>
                        <div id="totaramenu"><?php echo $totaramenu; ?></div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </nav>
        <div id="page-navbar-wrapper" class="clearfix">    
            <div id="page-navbar" class="container-fluid">
                <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
            </div>
            <!--<?php echo $OUTPUT->page_heading(); ?>
            <div id="course-header">
                <?php echo $OUTPUT->course_header(); ?>
            </div>-->
        </div>
    </div>
    
</header>