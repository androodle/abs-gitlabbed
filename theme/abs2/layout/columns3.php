<?php

// Get the HTML for the settings bits.
$html = theme_androtheme_get_html_for_settings($OUTPUT, $PAGE);
if (!empty($PAGE->theme->settings->logo)) {
    $logourl = $PAGE->theme->setting_file_url('logo', 'logo');
    $logoalt = get_string('logo', 'theme_androtheme', $SITE->fullname);
} else {
    $logourl = $OUTPUT->pix_url('logo', 'theme');
    $logoalt = get_string('logo', 'theme_androtheme');
}

if (!empty($PAGE->theme->settings->alttext)) {
    $logoalt = format_string($PAGE->theme->settings->alttext);
}

if (!empty($PAGE->theme->settings->favicon)) {
    $faviconurl = $PAGE->theme->setting_file_url('favicon', 'favicon');
} else {
    $faviconurl = $OUTPUT->favicon();
}

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = !empty($custommenu);

$haslogininfo = empty($PAGE->layout_options['nologininfo']);
$showmenu = empty($PAGE->layout_options['nocustommenu']);
$haslangmenu = (!isset($PAGE->layout_options['langmenu']) || $PAGE->layout_options['langmenu'] );
$left = (!right_to_left());

if ($showmenu && !$hascustommenu) {
    // load totara menu
    $menudata = totara_build_menu();
    $totara_core_renderer = $PAGE->get_renderer('totara_core');
    $totaramenu = $totara_core_renderer->print_totara_menu($menudata);
}

echo $OUTPUT->doctype() ?>

<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo $faviconurl; ?>" />
    
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<!--Andro sniffer-->
<div>
    <?php
    if ($PAGE->pagelayout == 'login' && file_exists("$CFG->dirroot/local/versionsniffer/locallib.php")) {
        require_once("$CFG->dirroot/local/versionsniffer/locallib.php");
        echo local_versionsniffer_out();
    }
    ?>
</div>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<?php require_once(dirname(__FILE__).'/includes/header.php'); ?>

<div id="page" class="container-fluid">
    <div id="page-content" class="row-fluid">
        <div id="region-bs-main-and-pre" class="span9 <?php echo $left ? "pull-left" : "pull-right" ?>">
            <div class="row-fluid">
                <section id="region-main" class="span8 <?php echo $left ? "pull-right" : "pull-left" ?>">
                <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
                </section>
                <?php echo $OUTPUT->blocks('side-pre', 'span4' . ($left ? ' desktop-first-column' : '')); ?>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', 'span3' . (!$left ? ' desktop-first-column pull-left' : '')); ?>
    </div>
</div>    

<?php require_once(dirname(__FILE__).'/includes/footer.php'); ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>

</body>
</html>
