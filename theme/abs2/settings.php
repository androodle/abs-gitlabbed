<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_abs2/logo';
        $title = get_string('logo','theme_abs2');
        $description = get_string('logodesc', 'theme_abs2');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        // Favicon file setting.
        $name = 'theme_abs2/favicon';
        $title = new lang_string('favicon', 'theme_abs2');
        $description = new lang_string('favicondesc', 'theme_abs2');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Banner header text setting
        $name = 'theme_abs2/bannertext';
        $title = get_string('bannertext', 'theme_abs2');
        $description = get_string('bannertextdesc', 'theme_abs2');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Custom CSS file
        $name = 'theme_abs2/customcss';
        $title = get_string('customcss', 'theme_abs2');
        $description = get_string('customcssdesc', 'theme_abs2');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_abs2/footnote';
        $title = get_string('footnote', 'theme_abs2');
        $description = get_string('footnotedesc', 'theme_abs2');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
}