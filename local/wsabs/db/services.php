<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Service definitions
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package     wsabs
 *-----------------------------------------------------------------------------
 **/

// Web service functions to install
$functions = array(
        'local_wsabs_hello_world' => array(
                'classname'   => 'local_wsabs_external',
                'methodname'  => 'hello_world',
                'classpath'   => 'local/wsabs/externallib.php',
                'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
                'type'        => 'read',
        ),
        'local_wsabs_delete_user_account' => array(
                'classname'   => 'local_wsabs_external',
                'methodname'  => 'delete_user_account',
                'classpath'   => 'local/wsabs/externallib.php',
                'description' => 'Delete user id in staging tables for processing by Androsync',
                'type'        => 'write',
        ),
        'local_wsabs_create_organisation_structure' => array(
                'classname'   => 'local_wsabs_external',
                'methodname'  => 'create_organisation_structure',
                'classpath'   => 'local/wsabs/externallib.php',
                'description' => 'Create an organisation structure into staging tables for processing by Androsync',
                'type'        => 'write',
        ),
        'local_wsabs_update_user_account' => array(
                'classname'   => 'local_wsabs_external',
                'methodname'  => 'update_user_account',
                'classpath'   => 'local/wsabs/externallib.php',
                'description' => 'Update user account in staging tables for processing by Androsync',
                'type'        => 'write',
        ),
);

// Pre-build services e.g. as a package
$services = array(
        'ABS Web Services' => array(
                'functions' => array (
                        'local_wsabs_hello_world',
                        'local_wsabs_delete_user_account',
                        'local_wsabs_create_organisation_structure',
                        'local_wsabs_update_user_account'
                ),
                'restrictedusers' => 0,
                'enabled'=>1,
        )
);
