<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Local plugin functions
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package 	wsabs
 *-----------------------------------------------------------------------------
 **/

define('ACTION_DELETE', 'D');
define('ACTION_UPDATE', 'U');

// Callback codes
define('ABS_RC_SUCCESS', 100);
define('ABS_RC_GENERAL_FAILURE', 101);

define('ABS_DEV_CALLBACK_URL', 'http://praj-mac/target.php'); // empty end point for dev
define('ABS_TEST_CALLBACK_URL', 'https://cds.test.abs.gov.au/identity-gateway-service/ExternalCallbackService?wsdl');
define('ABS_PROD_CALLBACK_URL', 'https://cds.abs.gov.au/identity-gateway-service/ExternalCallbackService?wsdl');

require_once("../../config.php");
require_once("../../webservice/soap/locallib.php");
require_once("classes/abssoapserver.class.php");
require_once('classes/abssoapclient.class.php');

/**
 *-----------------------------------------------------------------------------
 * Get URI
 * @param  none
 * @return string - uri with appropriate protocol
 *-----------------------------------------------------------------------------
 */

function get_uri() {
	if (strpos($_SERVER['HTTP_HOST'], 'abs.gov.au')) {
		return 'https://'.$_SERVER['HTTP_HOST'];
	}
	else {
		return 'http://'.$_SERVER['HTTP_HOST'].'/abs'; // for local dev
	}
}

/**
 *-----------------------------------------------------------------------------
 * Get WSDL Location
 * @param  string - $wsdl_name
 * @param  bool - https
 * @return string - $wsdl_location
 *-----------------------------------------------------------------------------
 */

function get_wsdl_location($wsdl_name, $https = TRUE) {

	if (!strpos($wsdl_name, 'wsdl.xml')) {
		$wsdl_name = $wsdl_name . '.wsdl.xml';
	}

	$wsdl_location = "/local/wsabs/wsdl/$wsdl_name";
	return $wsdl_location;
}

/**
 *-----------------------------------------------------------------------------
 * Provide web service using SoapServer
 * @param  string - wsdl_name
 * @param  string - service_class
 * @return start SOAP server for the given service
 *-----------------------------------------------------------------------------
 **/

function provide_service($wsdl_name, $service_class) {

	//ini_set("soap.wsdl_cache_enabled", 0);

	$uri = get_uri();
	$wsdl_location = $uri . get_wsdl_location($wsdl_name);

	$server_options = array(
		'location' => $uri."/local/wsabs/$service_class.php",
		'uri' => $uri
	);

    $server = new ABSSoapServer($wsdl_location, $server_options); // Debugging on
	$server->setClass($service_class);
	$server->handle();
}

/**
 *-----------------------------------------------------------------------------
 * Provide callback client using SoapClient
 * @param  (string) - environment: PROD/TEST
 * @return object - ABS Soap Client pointing to relevant callback URL
 *-----------------------------------------------------------------------------
 **/

function setup_callback_client($environment = 'TEST') {

	$client_options = array(
		'encoding' => 'UTF-8',
		'verifypeer' => false,
		'verifyhost' => false,
		'ssl' => array('ciphers'=>'RC4-SHA'),
		'soap_version' => SOAP_1_1, // Version used by ABS, don't use SOAP_1_2
		'trace' => 1,
		'exceptions' => 1,
		'connection_timeout' => 180
	);

	switch(strtoupper($environment)) {
		case 'PROD':
			$client = new ABSSoapClient(ABS_PROD_CALLBACK_URL, $client_options);
			break;
		case 'TEST':
			$client = new ABSSoapClient(ABS_TEST_CALLBACK_URL, $client_options);
			break;
		default:
			$client = new ABSSoapClient(ABS_DEV_CALLBACK_URL, $client_options);
	}

	return $client;
}

/**
 *-----------------------------------------------------------------------------
 * Get AndroSync Source ID
 * @param  string - Source name (friendly name)
 * @param  string - Source type
 * @return int - Source ID if found, NULL if not found.
 *-----------------------------------------------------------------------------
 **/

function get_androgogic_sync_source_id($source_name = NULL, $source_type = 'WEBSERVICE') {
	global $DB;
	$source_id = $DB->get_field('androgogic_sync_source', 'id', array('shortname' => $source_name, 'source' => $source_type));
	return $source_id;
}

/**
 *-----------------------------------------------------------------------------
 * Send callback message to ABS
 *
 * @param  (string) Callback response type
 * @param  (object) Staging data (user/org)
 * @param  (string) ABS callback method to invoke
 * @return (object) Response object
 *-----------------------------------------------------------------------------
 **/

function send_callback($feedback, $callback_method) {

	// Set up response message
	$response = new stdClass();

	// Send back a success message but show a fault if there is one in it.
	$message = new success($feedback->messageID, $feedback->description);
	$response->response->success = $message;

	// BEGIN: Useful debugging of the callback object
	/*
	print_r("<!-- RESPONSE ------------------------------------------------------------------------>\n");
	print_r($response);
	print_r("<!-- RESPONSE ------------------------------------------------------------------------>\n");
	die();
	*/
	//END: Useful debugging of the callback object

	// Make the callback to ABS
	$client = setup_callback_client('TEST');
	$client->$callback_method($response);

}