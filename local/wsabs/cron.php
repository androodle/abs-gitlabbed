<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Cron to pick up processed rows in staging table and send a message back to
 * the ABS web service end points
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package     wsabs
 *-----------------------------------------------------------------------------
 **/

function local_wsabs_respond_to_abs() {

	$trace = new text_progress_trace();
	$trace->output('ABS Web Service: local_wsabs_respond_to_abs');

	$trace->output('... Checking for processed user records');
	// TODO: implement user cron

	$trace->output('... Checking for processed org records');
	// TODO: implement org cron
}