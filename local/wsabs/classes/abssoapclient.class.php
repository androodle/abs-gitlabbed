<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Custom SoapClient that utilises WSE-PHP project for WSSE security headers.
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package 	wsabs
 *-----------------------------------------------------------------------------
 **/

include_once('locallib.php');

// Code from WSE-PHP project: https://github.com/robrichards/wse-php.git
include_once('xmlseclibs.php');
include_once('classes/WSASoap.php');
include_once('classes/WSSESoap.php');

global $CFG;

//-----------------------------------------------------------------------------
// Standard SOAP client with debugging
//-----------------------------------------------------------------------------

class ABSSoapClient extends SoapClient {

    private $_debug = FALSE;

    /**
     *-----------------------------------------------------------------------------
     * Extend PHP SoapClient constructor with debugging options
     * @param  string - wsdl
     * @param  array - options
     * @param  bool - debug flag
     * @return none - writes the message to error log if debugging turned on
     *-----------------------------------------------------------------------------
     **/

    public function __construct($wsdl, $options, $debug = FALSE) {
        $this->_debug = $debug;
        $this->_debug_client("-----------------------------------------------------------------------------");
        $this->_debug_client("ABS Soap Client");
        $this->_debug_client("-----------------------------------------------------------------------------");
        parent::__construct($wsdl, $options);
    }

    public function __doRequest($request, $location, $action, $version, $one_way = NULL) {

        $dom = new DOMDocument();
        $dom->loadXML($request);

        $objWSA = new WSASoap($dom);
        $objWSA->addAction($action);
        $objWSA->addTo($location);
        $objWSA->addMessageID();
        $objWSA->addReplyTo();

        $request = $objWSA->saveXML();

        $this->_debug_client("-----------------------------------------------------------------------------");
        $this->_debug_client("BEGIN: Client Request");
        $this->_debug_client("-----------------------------------------------------------------------------");
        $this->_debug_client($request);
        $this->_debug_client("-----------------------------------------------------------------------------");
        $this->_debug_client("END: Client Request");
        $this->_debug_client("-----------------------------------------------------------------------------");

        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

    /**
     *-----------------------------------------------------------------------------
     * Write debug messages to error log if debugging is turned on.
     * @param  string - debug message
     * @return none - writes the message to error log if debugging turned on
     *-----------------------------------------------------------------------------
     **/

    private function _debug_client($msg) {
        if ($this->_debug) {
            error_log($msg);
        }
    }

}

