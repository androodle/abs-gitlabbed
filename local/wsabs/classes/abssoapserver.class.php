<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Custom SoapServer for ABS
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package 	wsabs
 *-----------------------------------------------------------------------------
 **/

class ABSSoapServer extends SoapServer {

	private $_request_ip;
    private $_debug = FALSE;

    /**
     *-----------------------------------------------------------------------------
     * Extend PHP SoapServer constructor with debugging options
     * @param  string - wsdl
     * @param  array - options
     * @param  bool - debug flag
     * @return none - writes the message to error log if debugging turned on
     *-----------------------------------------------------------------------------
     **/

	public function __construct($wsdl, $options, $debug = FALSE) {

        $this->_debug = $debug;
        $this->_debug_server("-----------------------------------------------------------------------------");
        $this->_debug_server("ABS Soap Server");
        $this->_debug_server("-----------------------------------------------------------------------------");
        $this->_debug_server("WSDL=$wsdl");
        $this->_debug_server(serialize($options));

        parent::__construct($wsdl, $options);
	}

    /**
     *-----------------------------------------------------------------------------
     * Extend PHP Soap Server Handle with additional logic and debugging options.
     * @param  string - request - default null
     * @return string - result of soap call
     *-----------------------------------------------------------------------------
     **/

	public function handle($request = NULL) {
		$this->_debug_server("... ABS Soap Server: handle method");

		$this->_request_ip = $_SERVER['REMOTE_ADDR'];
		$this->_debug_server("... ABS Soap Server: request IP=$this->_request_ip");

        // Get the request
        if (is_null($request)) {
            $request = file_get_contents('php://input');
        }

        // Get the request headers

        if (function_exists('apache_request_headers')) {
        	$this->_debug_server("... ABS Soap Server request headers\n");
            $this->_debug_server(serialize(apache_request_headers()));
        }

   		// Start output buffering
        ob_start();

        // Call SoapServer::handle() - store result
        $this->_debug_server("... ABS Soap Server request\n");
        $this->_debug_server($request);

        // Remove soap:mustUnderstand from request as PHP doesn't like it
        $request = str_replace('mustUnderstand', 'mustUnderstandIgnored', $request);
        $result = parent::handle($request);
        $response = ob_get_contents();

        // Flush buffer
        ob_flush();

        // Store the response headers

        if (function_exists('apache_response_headers')) {
            $this->_debug_server('ABS Soap Server response headers='. serialize(apache_response_headers()));
        }

        $this->_debug_server('ABS Soap Server response='.serialize($response));

        return $result;
	}

    /**
     *-----------------------------------------------------------------------------
     * Write debug messages to error log if debugging is turned on.
     * @param  string - debug message
     * @return none - writes the message to error log if debugging turned on
     *-----------------------------------------------------------------------------
     **/

    private function _debug_server($msg) {
        if ($this->_debug) {
            error_log($msg);
        }
    }

}

