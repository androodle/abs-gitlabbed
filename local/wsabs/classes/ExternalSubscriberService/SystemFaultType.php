<?php

include_once('BaseFaultType.php');

class SystemFaultType extends BaseFaultType
{

    /**
     * @var BaseFaultType $cause
     * @access public
     */
    public $cause = null;

    /**
     * @var string[] $trace
     * @access public
     */
    public $trace = null;

    /**
     * @param ErrorCode $errorCode
     * @param HttpStatusCode $httpCode
     * @param string $summary
     * @param string $description
     * @param BaseFaultType $cause
     * @param string[] $trace
     * @access public
     */
    public function __construct($errorCode, $httpCode, $summary, $description, $cause, $trace)
    {
      parent::__construct($errorCode, $httpCode, $summary, $description);
      $this->cause = $cause;
      $this->trace = $trace;
    }

}
