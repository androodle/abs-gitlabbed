<?php

class OrganisationStructureType
{

    /**
     * @var anyType $organisationID
     * @access public
     */
    public $organisationID = null;

    /**
     * @var anyType $name
     * @access public
     */
    public $name = null;

    /**
     * @var anyType $parentID
     * @access public
     */
    public $parentID = null;

    /**
     * @param anyType $organisationID
     * @param anyType $name
     * @param anyType $parentID
     * @access public
     */
    public function __construct($organisationID, $name, $parentID)
    {
      $this->organisationID = $organisationID;
      $this->name = $name;
      $this->parentID = $parentID;
    }

}
