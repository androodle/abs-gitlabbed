<?php

class CreateOrganisationalStructureResponseType
{

    /**
     * @var string $response
     * @access public
     */
    public $response = null;

    /**
     * @param string $response
     * @access public
     */
    public function __construct($response)
    {
      $this->response = $response;
    }

}
