<?php

class UserAccountType
{

    /**
     * @var string $userID
     * @access public
     */
    public $userID = null;

    /**
     * @var Properties $properties
     * @access public
     */
    public $properties = null;

    /**
     * @param string $userID
     * @param Properties $properties
     * @access public
     */
    public function __construct($userID, $properties)
    {
      $this->userID = $userID;
      $this->properties = $properties;
    }

}
