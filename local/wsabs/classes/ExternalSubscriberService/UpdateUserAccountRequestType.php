<?php

class UpdateUserAccountRequestType
{

    /**
     * @var UserAccountType $userAccount
     * @access public
     */
    public $userAccount = null;

    /**
     * @var string $messageID
     * @access public
     */
    public $messageID = null;

    /**
     * @param UserAccountType $userAccount
     * @param string $messageID
     * @access public
     */
    public function __construct($userAccount, $messageID)
    {
      $this->userAccount = $userAccount;
      $this->messageID = $messageID;
    }

}
