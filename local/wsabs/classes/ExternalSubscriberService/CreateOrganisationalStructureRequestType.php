<?php

class CreateOrganisationalStructureRequestType
{

    /**
     * @var string $messageID
     * @access public
     */
    public $messageID = null;

    /**
     * @var OrganisationStructureList $OrganisationStructureList
     * @access public
     */
    public $OrganisationStructureList = null;

    /**
     * @param string $messageID
     * @param OrganisationStructureList $OrganisationStructureList
     * @access public
     */
    public function __construct($messageID, $OrganisationStructureList)
    {
      $this->messageID = $messageID;
      $this->OrganisationStructureList = $OrganisationStructureList;
    }

}
