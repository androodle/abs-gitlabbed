<?php

class DateProperty
{

    /**
     * @var attribute_name $name
     * @access public
     */
    public $name = null;

    /**
     * @var date $value
     * @access public
     */
    public $value = null;

    /**
     * @param attribute_name $name
     * @param date $value
     * @access public
     */
    public function __construct($name, $value)
    {
      $this->name = $name;
      $this->value = $value;
    }

}
