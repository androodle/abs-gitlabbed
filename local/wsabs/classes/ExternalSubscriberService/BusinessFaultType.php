<?php

include_once('BaseFaultType.php');

class BusinessFaultType extends BaseFaultType
{

    /**
     * @param ErrorCode $errorCode
     * @param HttpStatusCode $httpCode
     * @param string $summary
     * @param string $description
     * @access public
     */
    public function __construct($errorCode, $httpCode, $summary, $description)
    {
      parent::__construct($errorCode, $httpCode, $summary, $description);
    }

}
