<?php

class DeleteUserAccountRequestType
{

    /**
     * @var string $userID
     * @access public
     */
    public $userID = null;

    /**
     * @var string $messageID
     * @access public
     */
    public $messageID = null;

    /**
     * @param string $userID
     * @param string $messageID
     * @access public
     */
    public function __construct($userID, $messageID)
    {
      $this->userID = $userID;
      $this->messageID = $messageID;
    }

}
