<?php

include_once('AbstractServiceUnavailableFaultType.php');

class ServiceUnavailableFaultType extends AbstractServiceUnavailableFaultType
{

    /**
     * @var int $retryAfter
     * @access public
     */
    public $retryAfter = null;

    /**
     * @param ErrorCode $errorCode
     * @param HttpStatusCode $httpCode
     * @param string $summary
     * @param string $description
     * @param BaseFaultType $cause
     * @param string[] $trace
     * @param int $retryAfter
     * @access public
     */
    public function __construct($errorCode, $httpCode, $summary, $description, $cause, $trace, $retryAfter)
    {
      parent::__construct($errorCode, $httpCode, $summary, $description, $cause, $trace);
      $this->retryAfter = $retryAfter;
    }

}
