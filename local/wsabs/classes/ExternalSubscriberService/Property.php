<?php

class Property
{

    /**
     * @var attribute_name $name
     * @access public
     */
    public $name = null;

    /**
     * @var string $value
     * @access public
     */
    public $value = null;

    /**
     * @param attribute_name $name
     * @param string $value
     * @access public
     */
    public function __construct($name, $value)
    {
      $this->name = $name;
      $this->value = $value;
    }

}
