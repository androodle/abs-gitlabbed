<?php

class OrganisationStructureList
{

    /**
     * @var OrganisationStructureType $Organisation
     * @access public
     */
    public $Organisation = null;

    /**
     * @param OrganisationStructureType $Organisation
     * @access public
     */
    public function __construct($Organisation)
    {
      $this->Organisation = $Organisation;
    }

}
