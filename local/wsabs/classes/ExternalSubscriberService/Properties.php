<?php

class Properties
{

    /**
     * @var Property[] $property
     * @access public
     */
    public $property = null;

    /**
     * @var DateProperty[] $dateproperty
     * @access public
     */
    public $dateproperty = null;

    /**
     * @param Property[] $property
     * @param DateProperty[] $dateproperty
     * @access public
     */
    public function __construct($property, $dateproperty)
    {
      $this->property = $property;
      $this->dateproperty = $dateproperty;
    }

}
