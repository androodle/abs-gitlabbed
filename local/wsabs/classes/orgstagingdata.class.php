<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * LMS Organisation staging data class for AndroSync user staging table.
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package 	wsabs
 *-----------------------------------------------------------------------------
 **/

class OrgStagingData {

	// Message meta-data
	private $_messageID;
	private $_action;
	private $_organisationID;
	private $_errors;
	private $_staging_data;

	/**
	 *-----------------------------------------------------------------------------
	 * Constructor
	 * @param  message ID
	 * @param  user data including properties
	 * @param  action
	 * @return TRUE if data is valid, FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	public function __construct($messageID, $org_staging_data, $action) {

		if (!$this->_validate_message_id($messageID) ||
			!$this->_validate_message_action($action) ||
			!$this->_validate_organisation_id($org_staging_data->organisationID)) {
			return FALSE;
		}

		$this->_staging_data = new stdClass;
		$this->_staging_data->idnumber = $this->_organisationID;
		$this->_staging_data->fullname = $org_staging_data->name;
		$this->_staging_data->parentidnumber = $org_staging_data->parentID;

		// Validate required data
    	if (!$this->_validate_required_data()) {
    		$this->_log_error('Validation Failed', $this->_errors);
    		return FALSE;
    	}
    	else {
    		// Insert data into staging table
	    	return $this->_insert();
    	}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log error message to error log
	 * @param  string title
	 * @param  string exception (optional)
	 * @return none, writes message to PHP error log
	 *-----------------------------------------------------------------------------
	 **/

	private function _log_error($title, $data = NULL) {
		error_log('-----------------------------------------------------------------------------');
		error_log("local/wsabs/classes/orgstagingdata.class.php");
		error_log("Error: $title");
		if ($data) {
			error_log('-----------------------------------------------------------------------------');
			error_log("\n".print_r($data,1)); // String format
		}
		error_log('-----------------------------------------------------------------------------');
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate the supplied message ID
	 * @param  string messageID from ABS
	 * @return TRUE if valid, false otherwise.
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_message_id($messageID = null) {

		// Validate message ID has been provided
	    if (!empty($messageID)) {
        	$this->_messageID =$messageID; // TODO: where to store message ID? URL? Or use custom field?
        	return TRUE;
        }
        else {
    	    $this->_errors['messageID'] = "ERROR: message ID not supplied with request";
    	    return FALSE;
    	}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate Organisation Id in staging data
	 * @param  organisationID
	 * @return TRUE if valid, FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_organisation_id($organisationID = null) {
		if (!empty($organisationID)) {
    	    $this->_organisationID = $organisationID;
    	    return TRUE;
    	}
    	else {
    		$this->_errors['organisationID'] = "ERROR: organisation ID not supplied with request";
    		return FALSE;
    	}
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate the supplied message action
	 * @param  string messageID from ABS
	 * @return TRUE if valid, FALSE otherwise.
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_message_action($action = null) {

		if (!empty($action) && ($action == ACTION_UPDATE || $action == ACTION_DELETE)) {
        	$this->_action = $action;
        	return TRUE;
        }
        else {
    	    $this->_errors['action'] = "ERROR: action not supplied or not equal to update or delete";
    	    return FALSE;
    	}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate data before inserting into staging table. Record errors in
	 * errors array for display.
	 * @param  none
	 * @return TRUE if valid, FALSE if invalid capture error
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_required_data() {

		$validation_result = TRUE;

		if (empty($this->_staging_data->idnumber)) {
			$this->_errors['organisationID'] = "ERROR: Organisation ID not provided.";
			$validation_result = FALSE;
		}

		if (empty($this->_staging_data->fullname)) {
			$this->_errors['organisationName'] = "ERROR: Organisation name not provided.";
			$validation_result = FALSE;
		}

		return $validation_result;
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Insert data into staging table
	 * @param  none
	 * @return TRUE if successful, FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	private function _insert() {

		global $DB;

		// Staging data source information
    	$this->_staging_data->timecreated = date('U');
    	$this->_staging_data->sourceid = get_androgogic_sync_source_id('ABS Organisations Web Service');
    	$this->_staging_data->runid = 0; // Run ID should be 0 for web Service source
    	$this->_staging_data->processed = 0; // Processed be 0 for web service source

    	if (!isset($this->_staging_data->deleted)) {
    		$this->_staging_data->deleted = 0; // Default to 0, not deleted if not specified
    	}

    	// BEGIN: USEFUL-DEBUGGING
    	// Kills processing and shows the staging data just before insert into DB.
    	/*
    	print_r("--------- Staging data ---------\n");
    	print_r($this->_staging_data);
    	die();
		*/
    	// END: USEFUL-DEBUGGING

    	// Insert staging data
    	try {
    		$staging_data_id = $DB->insert_record('androgogic_sync_org', $this->_staging_data, true);
    	}
    	catch (Exception $e) {
    		// TODO: callback required here?
    		$this->_log_error("exception inserting staging data into database", $this->_staging_data);
    		return FALSE;
    	}

    	return TRUE;

	}

}