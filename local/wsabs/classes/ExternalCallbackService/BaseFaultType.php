<?php

class BaseFaultType
{

    /**
     * @var ErrorCode $errorCode
     * @access public
     */
    public $errorCode = null;

    /**
     * @var HttpStatusCode $httpCode
     * @access public
     */
    public $httpCode = null;

    /**
     * @var string $summary
     * @access public
     */
    public $summary = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @param ErrorCode $errorCode
     * @param HttpStatusCode $httpCode
     * @param string $summary
     * @param string $description
     * @access public
     */
    public function __construct($errorCode, $httpCode, $summary, $description)
    {
      $this->errorCode = $errorCode;
      $this->httpCode = $httpCode;
      $this->summary = $summary;
      $this->description = $description;
    }

}
