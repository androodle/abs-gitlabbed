<?php

class genericResponseType
{

    /**
     * @var BaseFaultType $fault
     * @access public
     */
    public $fault = null;

    /**
     * @var success $success
     * @access public
     */
    public $success = null;

    /**
     * @param BaseFaultType $fault
     * @param success $success
     * @access public
     */
    public function __construct($fault, $success)
    {
      $this->fault = $fault;
      $this->success = $success;
    }

}
