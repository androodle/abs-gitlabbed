<?php

include_once('BaseFaultType.php');
include_once('SystemFaultType.php');
include_once('InternalErrorFaultType.php');
include_once('NotImplementedFaultType.php');
include_once('ServiceUnavailableFaultType.php');
include_once('AbstractServiceUnavailableFaultType.php');
include_once('BusinessFaultType.php');
include_once('BadRequestFaultType.php');
include_once('UnauthorisedFaultType.php');
include_once('UnknownUserFaultType.php');
include_once('UpdateUserAccountResponseType.php');
include_once('DeleteUserAccountResponseType.php');
include_once('UpdateOrgStructureResponseType.php');
include_once('genericResponseType.php');
include_once('success.php');

class ExternalCallbackService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'BaseFaultType' => '\BaseFaultType',
      'SystemFaultType' => '\SystemFaultType',
      'InternalErrorFaultType' => '\InternalErrorFaultType',
      'NotImplementedFaultType' => '\NotImplementedFaultType',
      'ServiceUnavailableFaultType' => '\ServiceUnavailableFaultType',
      'AbstractServiceUnavailableFaultType' => '\AbstractServiceUnavailableFaultType',
      'BusinessFaultType' => '\BusinessFaultType',
      'BadRequestFaultType' => '\BadRequestFaultType',
      'UnauthorisedFaultType' => '\UnauthorisedFaultType',
      'UnknownUserFaultType' => '\UnknownUserFaultType',
      'UpdateUserAccountResponseType' => '\UpdateUserAccountResponseType',
      'DeleteUserAccountResponseType' => '\DeleteUserAccountResponseType',
      'UpdateOrgStructureResponseType' => '\UpdateOrgStructureResponseType',
      'genericResponseType' => '\genericResponseType',
      'success' => '\success');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = '/var/www/html/abs/local/wsabs/wsdl/external_callback_service.wsdl.xml')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * @param UpdateUserAccountResponseType $updateUserAccountResponse
     * @access public
     * @return void
     */
    public function UpdateExternalUserAccount(UpdateUserAccountResponseType $updateUserAccountResponse)
    {
      return $this->__soapCall('UpdateExternalUserAccount', array($updateUserAccountResponse));
    }

    /**
     * @param DeleteUserAccountResponseType $deleteUserAccountResponse
     * @access public
     * @return void
     */
    public function DeleteExternalUserAccount(DeleteUserAccountResponseType $deleteUserAccountResponse)
    {
      return $this->__soapCall('DeleteExternalUserAccount', array($deleteUserAccountResponse));
    }

    /**
     * @param UpdateOrgStructureResponseType $updateOrgStructureResponse
     * @access public
     * @return void
     */
    public function UpdateOrgStructure(UpdateOrgStructureResponseType $updateOrgStructureResponse)
    {
      return $this->__soapCall('UpdateOrgStructure', array($updateOrgStructureResponse));
    }

}
