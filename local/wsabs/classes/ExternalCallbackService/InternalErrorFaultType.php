<?php

class InternalErrorFaultType
{

    /**
     * @var ErrorCode $errorCode
     * @access public
     */
    public $errorCode = null;

    /**
     * @var HttpStatusCode $httpCode
     * @access public
     */
    public $httpCode = null;

    /**
     * @var string $summary
     * @access public
     */
    public $summary = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @var BaseFaultType $cause
     * @access public
     */
    public $cause = null;

    /**
     * @var string[] $trace
     * @access public
     */
    public $trace = null;

    /**
     * @param ErrorCode $errorCode
     * @param HttpStatusCode $httpCode
     * @param string $summary
     * @param string $description
     * @param BaseFaultType $cause
     * @param string[] $trace
     * @access public
     */
    public function __construct($errorCode, $httpCode, $summary, $description, $cause, $trace)
    {
      $this->errorCode = $errorCode;
      $this->httpCode = $httpCode;
      $this->summary = $summary;
      $this->description = $description;
      $this->cause = $cause;
      $this->trace = $trace;
    }

}
