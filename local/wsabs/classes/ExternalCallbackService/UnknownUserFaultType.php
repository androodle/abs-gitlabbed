<?php

include_once('SystemFaultType.php');

class UnknownUserFaultType extends SystemFaultType
{

    /**
     * @param ErrorCode $errorCode
     * @param HttpStatusCode $httpCode
     * @param string $summary
     * @param string $description
     * @param BaseFaultType $cause
     * @param string[] $trace
     * @access public
     */
    public function __construct($errorCode, $httpCode, $summary, $description, $cause, $trace)
    {
      parent::__construct($errorCode, $httpCode, $summary, $description, $cause, $trace);
    }

}
