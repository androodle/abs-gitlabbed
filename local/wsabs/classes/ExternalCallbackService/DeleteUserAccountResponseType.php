<?php

class DeleteUserAccountResponseType
{

    /**
     * @var genericResponseType $response
     * @access public
     */
    public $response = null;

    /**
     * @param genericResponseType $response
     * @access public
     */
    public function __construct($response)
    {
      $this->response = $response;
    }

}
