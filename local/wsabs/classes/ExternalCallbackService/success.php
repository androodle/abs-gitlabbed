<?php

class success
{

    /**
     * @var messageID_type $messageID
     * @access public
     */
    public $messageID = null;

    /**
     * @var message_type $message
     * @access public
     */
    public $message = null;

    /**
     * @param messageID_type $messageID
     * @param message_type $message
     * @access public
     */
    public function __construct($messageID, $message)
    {
      $this->messageID = $messageID;
      $this->message = $message;
    }

}
