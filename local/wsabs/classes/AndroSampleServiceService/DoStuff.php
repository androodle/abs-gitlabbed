<?php

class DoStuff
{

    /**
     * @var string $arg0
     * @access public
     */
    public $arg0 = null;

    /**
     * @param string $arg0
     * @access public
     */
    public function __construct($arg0)
    {
      $this->arg0 = $arg0;
    }

}
