<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * LMS User Profile Data class for AndroSync user staging table.
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package 	wsabs
 *-----------------------------------------------------------------------------
 **/

class UserStagingData {

	// Message meta-data
	private $_messageID;
	private $_action;
	private $_username;
	private $_staging_data;
	private $_staging_data_custom;
	private $_existing_lms_user = NULL;

	private $_feedback;

	private $_errors;
	private $_warnings;

	/**
	 *-----------------------------------------------------------------------------
	 * Constructor
	 * @param  string - message ID
	 * @param  object - user data including properties
	 * @param  string - action
	 * @return bool - TRUE if data added to staging records FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	public function __construct($messageID, $user_staging_data, $action) {

		if (!$this->_validate_message_id($messageID) ||
			!$this->_validate_message_action($action) ||
			!$this->_validate_user_id($user_staging_data->userID)) {
			return FALSE;
		}

		$this->_staging_data = new stdClass;
		$this->_staging_data_custom = new stdClass;
		$this->_feedback = new stdClass;
		$this->_feedback->messageID = $messageID;

		// If delete action, find and delete an existing user and return result
		if ($action == ACTION_DELETE) {
			return $this->_delete_existing_user($user_staging_data->userID);
		}
		elseif ($action == ACTION_UPDATE) {
			return $this->_update_existing_user($user_staging_data);
		}
		else {
			return FALSE; // Unkown action
		}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Get feedback data for handling via callback client
	 * @param  none - (accessor to $this->_feedback)
	 * @return object
	 *-----------------------------------------------------------------------------
	 **/

	public function get_feedback() {
		if (!empty($this->_errors)) {
			$this->_feedback->status = ABS_RC_GENERAL_FAILURE;
			$this->_feedback->description = current($this->_errors);
		}

		return $this->_feedback;
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Log error message to error log
	 * @param  string title
	 * @param  string exception (optional)
	 * @return none, writes message to PHP error log
	 *-----------------------------------------------------------------------------
	 **/

	private function _log_error($title, $data = NULL) {
		error_log('-----------------------------------------------------------------------------');
		error_log("local/wsabs/classes/userstagingdata.class.php");
		error_log("FAULT: $title");
		if ($data) {
			error_log('-----------------------------------------------------------------------------');
			error_log("\n".print_r($data,1)); // String format
		}
		error_log('-----------------------------------------------------------------------------');
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate the supplied message ID
	 * @param  string messageID from ABS
	 * @return TRUE if valid, false otherwise.
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_message_id($messageID = null) {

		// Validate message ID has been provided
	    if (!empty($messageID)) {
        	$this->_messageID =$messageID; // TODO: where to store message ID? URL? Or use custom field?
        	return TRUE;
        }
        else {
    	    $this->_errors['messageID'] = "FAULT: message ID not supplied with request";
    	    return FALSE;
    	}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate user ID in staging data.
	 * @param  userID
	 * @return TRUE if valid, FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_user_id($userID = null) {
		if (!empty($userID)) {
    	    $this->_username = $userID;
    	    return TRUE;
    	}
    	else {
    		$this->_errors['userID'] = "FAULT: user ID not supplied with request";
    		return FALSE;
    	}
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate the supplied message action
	 * @param  string messageID from ABS
	 * @return TRUE if valid, FALSE otherwise.
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_message_action($action = null) {

		if (!empty($action) && ($action == ACTION_UPDATE || $action == ACTION_DELETE)) {
        	$this->_action = $action;
        	return TRUE;
        }
        else {
    	    $this->_errors['action'] = "FAULT: action not supplied or not equal to update or delete";
    	    return FALSE;
    	}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Delete existing user based on provided username
	 * @param  string - username
	 * @return none - populate object if required
	 *		   bool - false if user not found
	 *-----------------------------------------------------------------------------
	 **/

	private function _delete_existing_user($username) {
		global $DB;

		if ($this->_existing_lms_user = $DB->get_record('user', array('username' => $username, 'deleted' => 0))) {
			// Set appropriate staging data required for a delete.
			// All of these are required for the delete to work correctly.
			$this->_staging_data->username = $this->_existing_lms_user->username;
			$this->_staging_data->firstname = $this->_existing_lms_user->firstname;
			$this->_staging_data->lastname = $this->_existing_lms_user->lastname;
			$this->_staging_data->email = $this->_existing_lms_user->email;
			// TODO: check about idnumber as that might be blank so what happens in that case?
			$this->_staging_data->idnumber = $this->_existing_lms_user->idnumber;

			// Suspend the user rather than deleting them in the LMS
			// $this->_staging_data->deleted = 1;
			$this->_staging_data->suspended = 1;

			return $this->_insert();
		}
		else {
			$this->_errors['user'] = "FAULT: user does not exist in the LMS with username: $username";
			return FALSE;
		}
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Update existing user with user properties provided
	 * @param  object - user properties
	 * @return bool - result of updating user
	 *-----------------------------------------------------------------------------
	 **/

	private function _update_existing_user($user_staging_data) {

		// Combine user properties from staging data
		$user_properties = $user_staging_data->properties;
		$user_properties = array_merge($user_properties->property, $user_properties->dateproperty);

		// Process properties into staging tables object
		foreach($user_properties as $property) {
			$this->_process($property);
		}

    	// Validate required data
    	if (!$this->_validate_required_data()) {
    		$this->_log_error('Validation Failed', $this->_errors);
    		return FALSE;
    	}
    	else {
	    	// Insert data into staging table
	    	return $this->_insert();
    	}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Process user message data for inserting into AndroSync staging table.
	 * @param  object - User message data property (name-value pairs)
	 * @return bool - TRUE if processed successfully, FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	private function _process($property) {

		// Username
		$this->_staging_data->username = $this->_username;

		// First name used as alternate name (preferred name is used as firstname)
  		if (($property->name == 'firstname' || $property->name == 'FIRST_NAME')) {
            $this->_staging_data->alternatename = $property->value;
        }

		// Last name
		if (($property->name == 'lastname' || $property->name == 'LAST_NAME')) {
            $this->_staging_data->lastname = $property->value;
        }

        // Preferred (alternate) name, used as firstname
        if (($property->name == 'alternatename' || $property->name == 'PREFERRED_NAME')) {
			$this->_staging_data->firstname = $property->value;
        }

		// ABS Employment type
		if (($property->name == 'absemploymenttype' || $property->name == 'TYPE_OF_EMPLOYMENT')) {
			$this->_staging_data_custom->absemploymenttype = $property->value;
		}

		// ABS Employment status
		if (($property->name == 'absemploymentstatus' || $property->name == 'EMPLOYMENT_STATUS')) {
			$this->_staging_data_custom->absemploymentstatus = $property->value;
		}

		// Email
		if (($property->name == 'email' || $property->name == 'EMAIL' || $property->name == 'EMAIL_ADDRESS')) {
			$this->_staging_data->email = $property->value;
		}

		// Country (code)
		if (($property->name == 'country' || $property->name == 'COUNTRY_CODE')) {
			$this->_staging_data->country = $property->value;
		}

		// ABS Employee Group Description
		if (($property->name == 'absemployeegroupdescription' || $property->name == 'EMPLOYEE_GROUP_DESCRIPTION')) {
			$this->_staging_data_custom->absemployeegroupdescription = $property->value;
		}

		// ABS Classification - Actual
		if (($property->name == 'absclassificationactual' || $property->name == 'ABS_CLASSIFICATION_ACTUAL' || $property->name == 'ACTUAL_POSITION')) {
			$this->_staging_data_custom->absclassificationactual = $property->value;
			$this->_staging_data->posidnumber = $property->value;
		}

		// ABS Classification - Actual
		if (($property->name == 'absclassificationnominal' || $property->name == 'ABS_CLASSIFICATION_NOMINAL' || $property->name == 'NOMINAL_POSITION')) {
			$this->_staging_data_custom->absclassificationnominal = $property->value;
		}

		// ABS Organisation Start Date
		if (($property->name == 'absorganisationstartdate' || $property->name == 'START_DATE_ABS' || $property->name == 'START_DATE_AT_ABS')) {
			$this->_staging_data_custom->absorganisationstartdate = $property->value;
		}

		// Position Start Date
		if (($property->name == 'posstartdate' || $property->name == 'START_DATE_ACTUAL_POSITION' || $property->name == 'START_DATE_AT_CLASSIFICATION')) {
			$this->_staging_data->posstartdate = $property->value;
		}

		// Organisation Hierarchy ID Number
		if (($property->name == 'orgidnumber' || $property->name == 'ORGANISATION_ID' || $property->name == 'ORGANIZATION_ID')) {
			$this->_staging_data->orgidnumber = $property->value;
		}

		// ABS position ID -> position full name
		if (($property->name == 'posidnumber' || $property->name == 'POSITION_ID')) {
			// Position ID is populated as the ABS classification actual
			//$this->_staging_data->posidnumber = $property->value;
			$this->_staging_data->posfullname = $property->value;
		}

		// ABS Position Title
		if (($property->name == 'abspositiontitle' || $property->name == 'POSITION_TITLE')) {
			$this->_staging_data_custom->abspositiontitle  = $property->value;
		}

		// ID Number (ABS UID)
		if (($property->name == 'idnumber' || $property->name == 'ABS_UID')) {
			$this->_staging_data->idnumber  = $property->value;
		}

		// ABS Organisation End Date
		if (($property->name == 'absorganisationenddate' || $property->name == 'END_DATE_ABS' || $property->name == 'END_DATE')) {
			$this->_staging_data_custom->absorganisationenddate = $property->value;
			$this->_staging_data->posenddate = $property->value;
		}

		// ABS Office Location
		if (($property->name == 'absofficelocation ' || $property->name == 'OFFICE')) {
			$this->_staging_data_custom->absofficelocation = $property->value;
		}

		// ABS Supervisor ID
		if (($property->name == 'manageridnumber ' || $property->name == 'SUPERVISOR_ABS_UID')) {
			$this->_staging_data->manageridnumber = $property->value;
		}

		// Timezone
		$this->_staging_data->timezone = $this->_timezone_lookup(
			isset($this->_staging_data_custom->absofficelocation) ? $this->_staging_data_custom->absofficelocation : ''
		);

		// Census Enum Type
		if (($property->name == 'abscensusenumtype ' || $property->name == 'CENSUS_ENUM_TYPE')) {
			switch ($property->value) {
				case "MAIL_OUT":
					$this->_staging_data_custom->abscensusenumtype = "Mail out";
					break;
				case "DROP_OFF":
					$this->_staging_data_custom->abscensusenumtype = "Drop off";
					break;
				case "NOT_APPLICABLE":
					$this->_staging_data_custom->abscensusenumtype = "Not applicable";
					break;
				default:
					$this->_staging_data_custom->abscensusenumtype = "Not applicable";
			}
		}

	}

	/**
	 *-----------------------------------------------------------------------------
	 * Validate data before inserting into staging table. Record errors in
	 * errors array for display.
	 * @param  none
	 * @return TRUE if valid, FALSE if invalid capture error
	 *-----------------------------------------------------------------------------
	 **/

	private function _validate_required_data() {

		$validation_result = TRUE;

		if (empty($this->_staging_data->firstname)) {
			$this->_errors['firstname'] = "FAULT: First name not provided.";
			$validation_result = FALSE;
		}

		if (empty($this->_staging_data->lastname)) {
			$this->_errors['lastname'] = "FAULT: Last name not provided.";
			$validation_result = FALSE;
		}

		if (empty($this->_staging_data->email)) {
			$this->_errors['email'] = "FAULT: Email address not provided.";
			$validation_result = FALSE;
		}

		if (!empty($this->_staging_data->idnumber)) {
			if (strlen($this->_staging_data->idnumber) != 8) {
				$this->_errors['idnumber'] = "FAULT: ABS UID idnumber must be 8 characters in length or empty";
				$validation_result = FALSE;
			}
		}

		return $validation_result;
	}

	/**
	 *-----------------------------------------------------------------------------
	 * Insert data into staging table
	 * @param  none
	 * @return TRUE if successful, FALSE otherwise
	 *-----------------------------------------------------------------------------
	 **/

	private function _insert() {

		global $DB;

		// Staging data source information
    	$this->_staging_data->timecreated = date('U');
    	$this->_staging_data->sourceid = get_androgogic_sync_source_id('ABS Users Web Service');
    	$this->_staging_data->runid = 0; // Run ID should be 0 for web Service source
    	$this->_staging_data->processed = 0; // Processed be 0 for web service source

    	// Staging data defaults
    	$this->_staging_data->auth = 'saml';
		$this->_staging_data->deleted = 0;

    	// BEGIN: USEFUL-DEBUGGING
    	// Kills processing and shows the staging data just before insert into DB.
    	/*
    	print_r("--------- Staging data ---------\n");
    	print_r($this->_staging_data);
    	print_r("\n--------- Custom data ---------\n");
    	print_r($this->_staging_data_custom);
    	die();
    	*/
		// END: USEFUL-DEBUGGING

    	// Insert staging data
    	try {
    		$staging_data_id = $DB->insert_record('androgogic_sync_user', $this->_staging_data, true);
    	}
    	catch (Exception $e) {
    		// TODO: callback required here?
    		$this->_log_error("exception inserting staging data into database", $this->_staging_data);
    		return FALSE;
    	}

    	// Custom staging data can now be added row by row
    	if (!empty($this->_staging_data_custom)) {
    		foreach ($this->_staging_data_custom as $custom_data_key => $custom_data_value) {
    			$custom_data = new stdClass();
    			$custom_data->timecreated = $this->_staging_data->timecreated;
    			$custom_data->sourceid = $this->_staging_data->sourceid;
    			$custom_data->stagingid = $staging_data_id;
    			$custom_data->shortname = $custom_data_key;
    			$custom_data->data = $custom_data_value;

	    		try {
	    			$staging_data_custom_id = $DB->insert_record('androgogic_sync_user_custom',  $custom_data, true);
	    		}
	    		catch (Exception $e) {
	    			$this->_log_error("exception inserting custom staging data into database", $custom_data);
	    			return FALSE;
	    		}
    		}
    	}

    	$this->_feedback->status = ABS_RC_SUCCESS;
    	$this->_feedback->description = "Successfully staged user data for processing";

    	return TRUE;

	}

	/**
	 *-----------------------------------------------------------------------------
	 * ABS Office Location Timezone Lookup
	 * @param  string - ABS office location
	 * @return string - timezone value if mapping found
     * 		   bool - FALSE if timezone mapping not found
	 *-----------------------------------------------------------------------------
	 **/

	private function _timezone_lookup($absofficelocation) {

		switch ($absofficelocation) {
			case 'ABS House':
				return 'Australia/Sydney';
				break;
			case 'Australian Capital Territory':
				return 'Australia/Sydney';
				break;
			case 'Melbourne, Victoria':
				return 'Australia/Melbourne';
				break;
			case 'New South Wales':
				return 'Australia/Sydney';
				break;
			case 'Northern Territory':
				return 'Australia/Darwin';
				break;
			case 'Queensland':
				return 'Australia/Brisbane';
				break;
			case 'South Australia':
				return 'Australia/Adelaide';
				break;
			case 'Tasmania':
				return 'Australia/Hobart';
				break;
			case 'Victoria':
				return 'Australia/Melbourne';
				break;
			case 'Western Australia':
				return 'Australia/Perth';
				break;
			default:
				return 'Australia/Sydney'; // Default timezone if one can't be determined
		}

	}

}