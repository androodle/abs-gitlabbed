<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Function library
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package     wsabs
 *-----------------------------------------------------------------------------
 **/

define('RUNID', 0);
define('PROCESSED', 0);

/**
 *-----------------------------------------------------------------------------
 * Trigger plugin cron
 * @return none
 *-----------------------------------------------------------------------------
 */

function local_wsabs_cron() {
	global $CFG;
    require_once($CFG->dirroot . '/local/wsabs/cron.php');
    local_wsabs_respond_to_abs();
}