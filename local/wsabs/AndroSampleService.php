<?php
include_once('locallib.php');

include_once('classes/AndroSampleServiceService/DoStuff.php');
include_once('classes/AndroSampleServiceService/DoStuffResponse.php');

/*----------------------------------------------------------------------------*
 * AndroSampleService class
 *----------------------------------------------------------------------------*/

class AndroSampleServiceService {
	public function DoStuff($params) {

		$result = "Hello, $params->arg0!";
		$response = new DoStuffResponse($result);

		return $response;
	}
}

/*----------------------------------------------------------------------------*
 * Provide the service
 *----------------------------------------------------------------------------*/

provide_service('sample_service', 'AndroSampleServiceService');