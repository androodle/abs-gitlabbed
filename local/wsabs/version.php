<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * Version information
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package 	wsabs
 *-----------------------------------------------------------------------------
 **/

$plugin->version  = 2015120900;
$plugin->requires = 2010112400; // Requires this Moodle version - at least 2.0
$plugin->cron     = 0;
$plugin->release = '1.0';
$plugin->maturity = MATURITY_STABLE;
