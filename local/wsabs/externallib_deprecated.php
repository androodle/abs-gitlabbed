<?php

/**
 *-----------------------------------------------------------------------------
 * ABS Web Services
 *-----------------------------------------------------------------------------
 * External library
 *
 * @author      Androgogic
 * @copyright   Androgogic Pty Ltd <http://www.androgogic.com>
 * @package     wsabs
 *-----------------------------------------------------------------------------
 **/

require_once($CFG->libdir . "/externallib.php");

class local_wsabs_external extends external_api {

    const ACTION_DELETE = 'D';
    const ACTION_UPDATE = 'U';
    const RUNID = 0; // Not sure if this is ok?
    const PROCESSED = 0;

    static $staging_data;
    static $staging_data_custom;

    /**
     *-----------------------------------------------------------------------------
     * Create organisation structure
     * @return external_function_parameters
     *-----------------------------------------------------------------------------
     */

    public static function create_organisation_structure_parameters() {
        return new external_function_parameters(
            array(
                'OrganisationStructureList' => new external_multiple_structure (
                        new external_single_structure(
                        array(
                            'Organisation' => new external_single_structure(
                                array(
                                    'organisationID' => new external_value(PARAM_USERNAME, 'Organisation ID'),
                                    'name' => new external_value(PARAM_RAW, 'Organisation Name'),
                                    'parentID' => new external_value(PARAM_RAW, 'Organisation Parent ID')
                                )
                            )
                        )
                    ),
                    'Organisation structure data'
                )
            )
        );
    }

    /**
     *-----------------------------------------------------------------------------
     * Create organisation structure service core
     * @return string result of insert into the staging table
     *-----------------------------------------------------------------------------
     */

    public static function create_organisation_structure($organisation_data) {

        if ($result = self::_validate_organisation_data($organisation_data)) {
            return $result;
        }

        return self::_insert_into_organisation_staging_record();
    }

    /**
     *-----------------------------------------------------------------------------
     * Description of create organisation structure service
     * @return external_value
     *-----------------------------------------------------------------------------
     */

    public static function create_organisation_structure_returns() {
        return new external_value(PARAM_TEXT, 'Web service call result');
    }

    /**
     *-----------------------------------------------------------------------------
     * Delete user account parameters
     * @return external_function_parameters
     *-----------------------------------------------------------------------------
     */

    public static function delete_user_account_parameters() {
        return new external_function_parameters(
            array(
                'UserAccount' => new external_single_structure (
                    array(
                        'UserID' => new external_value(
                            PARAM_USERNAME,
                            'LMS username confirming to LMS username standards to be deleted'
                        ),
                        'Properties' => new external_multiple_structure (
                             new external_single_structure (
                                array(
                                    'Property' => new external_single_structure (
                                        array(
                                            'Name' => new external_value(PARAM_RAW, 'User account property name'),
                                            'Value' => new external_value(PARAM_RAW, 'User account property value')
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    'User account data including user Id and properties as name-value pairs'
                )
            )
        );
    }

    /**
     *-----------------------------------------------------------------------------
     * Delete user account service core
     * @return string result of insert into the staging table
     *-----------------------------------------------------------------------------
     */

    public static function delete_user_account($user_account_data) {

        if ($result = self::_validate_user_account_data($user_account_data, self::ACTION_DELETE)) {
            return $result;
        }

        return self::_insert_into_staging_record($user_account_data, self::ACTION_DELETE);
    }

    /**
     *-----------------------------------------------------------------------------
     * Description of delete user account service
     * @return external_value
     *-----------------------------------------------------------------------------
     */

    public static function delete_user_account_returns() {
        return new external_value(PARAM_TEXT, 'Web service call result');
    }

    /**
     *-----------------------------------------------------------------------------
     * Hello world service parameters
     * @return external_function_parameters
     *-----------------------------------------------------------------------------
     */

    public static function hello_world_parameters() {
        return new external_function_parameters(
                array('welcomemessage' => new external_value(PARAM_TEXT, 'The welcome message. By default it is "Hello world,"', VALUE_DEFAULT, 'Hello world, '))
        );
    }

    /**
     *-----------------------------------------------------------------------------
     * Hello world service core
     * @return string welcome message
     *-----------------------------------------------------------------------------
     */

    public static function hello_world($welcomemessage = 'Hello world, ') {
        global $USER;

        //Parameter validation
        //REQUIRED
        $params = self::validate_parameters(self::hello_world_parameters(),
                array('welcomemessage' => $welcomemessage));

        //Context validation
        //OPTIONAL but in most web service it should present
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        //OPTIONAL but in most web service it should present
        if (!has_capability('moodle/user:viewdetails', $context)) {
            throw new moodle_exception('cannotviewprofile');
        }

        return $params['welcomemessage'] . $USER->firstname ;;
    }

    /**
     *-----------------------------------------------------------------------------
     * Description of hello world service
     * @return external_description
     *-----------------------------------------------------------------------------
     */

    public static function hello_world_returns() {
        return new external_value(PARAM_TEXT, 'The welcome message + user first name');
    }

    /**
     *-----------------------------------------------------------------------------
     * Update user account parameters
     * @return external_function_parameters
     *-----------------------------------------------------------------------------
     */
    public static function update_user_account_parameters() {
        return new external_function_parameters(
            array(
                'UserAccount' => new external_single_structure (
                    array(
                        'UserID' => new external_value(
                            PARAM_USERNAME,
                            'LMS username confirming to LMS username standards to be deleted'
                        ),
                        'Properties' => new external_multiple_structure (
                             new external_single_structure (
                                array(
                                    'Property' => new external_single_structure (
                                        array(
                                            'Name' => new external_value(PARAM_RAW, 'User account property name'),
                                            'Value' => new external_value(PARAM_RAW, 'User account property value')
                                        )
                                    )
                                )
                            )
                        )
                    ),
                    'User account data including user Id and properties as name-value pairs'
                )
            )
        );
    }

    /**
     *-----------------------------------------------------------------------------
     * Update user account core
     * @param  object user account data
     * @return string result of inserting into the staging table
     *-----------------------------------------------------------------------------
     */

    public static function update_user_account($user_account_data) {

        if ($result = self::_validate_user_account_data($user_account_data, self::ACTION_UPDATE)) {
            return $result;
        }

        return self::_insert_into_staging_record($user_account_data, self::ACTION_UPDATE);
    }

    /**
     *-----------------------------------------------------------------------------
     * Description of update user account service
     * @return external_value
     *-----------------------------------------------------------------------------
     */

    public static function update_user_account_returns() {
        return new external_value(PARAM_TEXT, 'Web service call result');
    }

    /**
     *-----------------------------------------------------------------------------
     * Validate user account data and parse into a single object
     * @param object $user_account_data user account data
     * @param const $action web service action type
     * @return bool validation failure (false if validation passes)
     *-----------------------------------------------------------------------------
     */

     private static function _validate_user_account_data($user_account_data, $action) {

        if (empty($user_account_data['UserID'])) {
            return "ERROR: User account user id (username) not specified";
        }

        // Create and update require a parameter
        if (!isset($user_account_data['Properties']) ||
            empty($user_account_data['Properties']) ||
            !is_array($user_account_data['Properties'])
           ) {
           return "ERROR: User account properties not provided.";
        }


        // Parse and validate data
        $invalid_idnumber = TRUE;
        $invalid_firstname = TRUE;
        $invalid_lastname = TRUE;
        $invalid_email = TRUE;

        self::$staging_data = new stdClass;
        self::$staging_data->username = $user_account_data['UserID'];

        foreach ($user_account_data['Properties'] as $property) {

            // Required fields, validate
            if (($property['name'] == 'idnumber' || $property['name'] == 'ABS_UID')
                && !empty($property['value'])) {
                self::$staging_data->idnumber = $property['value'];
                $invalid_idnumber = FALSE;
            }
            if (($property['name'] == 'firstname' || $property['name'] == 'FIRST_NAME')
                && !empty($property['value'])) {
                self::$staging_data->firstname = $property['value'];
                $invalid_firstname = FALSE;
            }
            if (($property['name'] == 'lastname' || $property['name'] == 'LAST_NAME')
                && !empty($property['value'])) {
                self::$staging_data->lastname = $property['value'];
                $invalid_lastname = FALSE;
            }
            if (($property['name'] == 'email' || $property['name'] == 'EMAIL')
                && !empty($property['value'])) {
                self::$staging_data->email = $property['value'];
                $invalid_email = FALSE;
            }

            // Optional fields, populate if provided
            if (($property['name'] == 'alternatename' || $property['name'] == 'PREFERRED_NAME')
                && !empty($property['value'])) {
                self::$staging_data->alternatename = $property['value'];
            }
            if ($property['name'] == 'middlename' && !empty($property['value'])) {
                self::$staging_data->middlename = $property['value'];
            }
            if ($property['name'] == 'phone1' && !empty($property['value'])) {
                self::$staging_data->phone1 = $property['value'];
            }
            if ($property['name'] == 'phone2' && !empty($property['value'])) {
                self::$staging_data->phone2 = $property['value'];
            }
            if ($property['name'] == 'city' && !empty($property['value'])) {
                self::$staging_data->city = $property['value'];
            }
            if (($property['name'] == 'country' || $property['name'] == 'COUNTRY_CODE')
                && !empty($property['value'])) {
                self::$staging_data->country = $property['value'];
            }
            if (($property['name'] == 'timezone' || $property['name'] == 'TIMEZONE')
                && !empty($property['value'])) {
                self::$staging_data->timezone = $property['value'];
            }
            if (($property['name'] == 'manageridnumber' || $property['name'] == 'SUPERVISORS_ID')
                && !empty($property['value'])) {
                self::$staging_data->manageridnumber = $property['value'];
            }
            if ($property['name'] == 'appraiseridnumber' && !empty($property['value'])) {
                self::$staging_data->appraiseridnumber = $property['value'];
            }
            if (($property['name'] == 'orgidnumber' || $property['name'] == 'ORGANISATION_ID')
                && !empty($property['value'])) {
                self::$staging_data->orgidnumber = $property['value'];
            }
            if ($property['name'] == 'orgfullname' && !empty($property['value'])) {
                self::$staging_data->orgfullname = $property['value'];
            }
            if (($property['name'] == 'posidnumber' || $property['name'] == 'POSITION_ID')
                && !empty($property['value'])) {
                self::$staging_data->posidnumber = $property['value'];
            }
            if ($property['name'] == 'posfullname' && !empty($property['value'])) {
                self::$staging_data->posfullname = $property['value'];
            }
            if (($property['name'] == 'posstartdate' || $property['name'] == 'START_DATE_ACTUAL_POSITION')
                && !empty($property['value'])) {
                self::$staging_data->posstartdate = $property['value'];
            }
            if (($property['name'] == 'posenddate' || $property['name'] == 'END_DATE_ABS')
                && !empty($property['value'])) {
                self::$staging_data->posenddate = $property['value'];
            }

            // Optional: Custom data fields
            if (($property['name'] == 'absemploymenttype' || $property['name'] == 'TYPE_OF_EMPLOYMENT')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absemploymenttype', 'data' => $property['value']);
            }

            if (($property['name'] == 'absemploymentstatus' || $property['name'] == 'EMPLOYMENT_STATUS')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absemploymentstatus', 'data' => $property['value']);
            }

            if (($property['name'] == 'absofficelocation' || $property['name'] == 'OFFICE')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absofficelocation', 'data' => $property['value']);
            }

            if (($property['name'] == 'absemployeegroupdescription' || $property['name'] == 'EMPLOYEE_GROUP_DESCRIPTION')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absemployeegroupdescription', 'data' => $property['value']);
            }

            if (($property['name'] == 'absclassificationnominal' || $property['name'] == 'ABS_CLASSIFICATION_NOMINAL')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absclassificationnominal', 'data' => $property['value']);
            }

            if (($property['name'] == 'absorganisationstartdate' || $property['name'] == 'START_DATE_ABS')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absorganisationstartdate', 'data' => $property['value']);
            }

            if (($property['name'] == 'absorganisationenddate' || $property['name'] == 'END_DATE_ABS')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'absorganisationenddate', 'data' => $property['value']);
            }

            if (($property['name'] == 'abspositiontitle' || $property['name'] == 'ABS_POSITION_TITLE')
                && !empty($property['value'])) {
                self::$staging_data_custom[] = (object) array('shortname' => 'abspositiontitle', 'data' => $property['value']);
            }

            // USEFUL-DEBUGGING
            // echo($property['name'].'='.$property['value']."\n");

        }

        // Error messages
        if ($invalid_idnumber) {
            return "ERROR: User idnumber not specified or blank";
        }

        if ($invalid_firstname) {
            return "ERROR: User firstname not specified or blank";
        }

        if ($invalid_lastname) {
            return "ERROR: User lastname not specified or blank";
        }

        if ($invalid_email) {
            return "ERROR: User email not specified or blank";
        }

        // USEFUL-DEBUGGING
        // Show contents of staging data in web service response and die
        // print_r(self::$staging_data);
        // print_r(self::$staging_data_custom);
        // die();

        // Validation passes
        return FALSE;
    }

    /**
     *-----------------------------------------------------------------------------
     * Common database function to insert web service data into staging record.
     * All actions are added even deletes, but delete action has not user account
     * properties.
     * @param object $user_account_data user account data
     * @param const $action web service action type
     * @return string
     *-----------------------------------------------------------------------------
     */

    private static function _insert_into_staging_record($user_account_data, $action) {

        global $DB;

        // Header fields
        self::$staging_data->timecreated = date('U');
        self::$staging_data->sourceid = 1; // TODO: Programatically determine this
        self::$staging_data->runid = self::RUNID;
        self::$staging_data->processed = self::PROCESSED;

        // Default fields
        self::$staging_data->auth = 'saml'; // Determine based on source
        self::$staging_data->deleted = 0; // Never delete a user
        if ($action == self::ACTION_DELETE) {
            //self::$staging_data->suspended = 1; // suspend the user (don't delete)
            self::$staging_data->deleted = 1; // delete the user
        }
        else {
            self::$staging_data->deleted = 0;
        }

        // Data fields: these are populated during validation

        $insertid = $DB->insert_record('androgogic_sync_user', self::$staging_data, true);

        // Custom data fields: these are populated during validation
        if (!empty(self::$staging_data_custom)) {
            foreach (self::$staging_data_custom as $custom) {
                $custom->timecreated = self::$staging_data->timecreated;
                $custom->sourceid = self::$staging_data->sourceid;
                $custom->stagingid = $insertid; // ID of the inserted staging data

                // Data fields: these are populated during validation

                $custominsertid = $DB->insert_record('androgogic_sync_user_custom',  $custom, true);
            }
        }

        return "SUCCESS:\n".print_r(self::$staging_data, 1)."\nfor Action: $action with Database ID: $insertid";
    }

    /**
     *-----------------------------------------------------------------------------
     * Validate organisation data
     * @param object $organisation_data user account data
     * @param const $action web service action type
     * @return bool validation failure (false if validation passes)
     *-----------------------------------------------------------------------------
     */

     private static function _validate_organisation_data($organisation_data) {

        $i = 0;
        foreach ($organisation_data as $organisation) {

            // Header fields
            self::$staging_data[$i]->created = date('U');
            self::$staging_data[$i]->sourceid = 2; // TODO: Programatically determine this!
            self::$staging_data[$i]->runid = self::RUNID;
            self::$staging_data[$i]->processed = self::PROCESSED;

            if (isset($organisation['organisationID']) && !empty($organisation['organisationID'])) {
                self::$staging_data[$i]->idnumber = $organisation['organisationID'];
            }
            else {
                return 'ERROR: Organisation idnumber not specified or blank';
            }

            if (isset($organisation['name']) && !empty($organisation['name'])) {
                self::$staging_data[$i]->fullname = $organisation['name'];
            }
            else {
                return 'ERROR: Organisation name not specified or blank';
            }

            if (isset($organisation['parentID']) && !empty($organisation['parentID'])) {
                self::$staging_data[$i]->parentidnumber = $organisation['parentID'];
            }
            else {
                return 'ERROR: Organisation parent idnumber not specified or blank';
            }

            $i++;
        }

        // False if validation passes
        return FALSE;
    }

    /**
     *-----------------------------------------------------------------------------
     * Insert organisation data into Androsync staging record
     * @param object $organisation_data
     * @return string
     *-----------------------------------------------------------------------------
     */

    private static function _insert_into_organisation_staging_record() {

        global $DB;
        for ($i = 0; $i < sizeof(self::$staging_data); $i++) {
            $insertid = $DB->insert_record('androgogic_sync_org', self::$staging_data[$i], true);
        }
        return 'SUCCESS: Organisation(s) added to staging records';
    }
}
