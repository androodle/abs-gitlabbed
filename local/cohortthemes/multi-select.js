
/**
 * See Doco at http://loudev.com/
 */

$('.multiselect').multiSelect({
    selectableHeader: "<div class='ms-ag-selectable'>Available:</div>",
    selectionHeader: "<div class='ms-ag-selected'>Selected:</div>"
});

function multireset() {
    $('.multiselect').multiSelect('deselect_all');
    return true;
}