<?php
/**
 * Cohort (audience) based theme assignment
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 * 
 **/

$string['pluginname'] = 'Audience based user themes';
$string['settings'] = 'Audience theme settings';
$string['changessaved'] = 'Audience theme settings have been saved.<br />
                           Changes, if any, will be applied to enrolled users by a scheduled cron task soon.';
$string['assignthemes'] = 'Assign themes to audiences';
$string['formnotice'] = 'Here you can link audiences to themes.<br />
    If an audience is not linked to any theme, the standard site theme will be used for users in this audience (any existing user theme will be cleared).<br />
    <strong>NOTE:</strong> Please assign only one theme per audience.<br />
    <br />
    Changes made on this page will be applied to users, who are already enrolled in audiences, in the background during the next cron run.<br />
    Users, newly enrolled in audiences, will be assigned a user theme immediately.<br />
    Users, unenrolled from audiences, will be unassigned a user theme immediately.<br />
    ';
$string['cohortassignedto2themes'] = 'Audience "{$a}" linked to more than one theme';
$string['updateexistingusers'] = 'Update existing users';




