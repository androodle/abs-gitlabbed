<?php
/**
 * Cohort (audience) based theme assignment event handler definition.
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @category    event
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 */

$observers = array(
    array(
        'eventname' => '\core\event\user_loggedin',
        'callback' => 'local_cohortthemes_observer::user_loggedin',
    ),
);
