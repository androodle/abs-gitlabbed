<?php
/**
 * Cohort (audience) based theme assignment
 *
 * Local plugin to automatically assign theme to users based on cohort (audience) they are enrolled in.
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

$plugin->version = 2015062202;
$plugin->requires = 2013111800; // Indicate we need at least Moodle 2.6
