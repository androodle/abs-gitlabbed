<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

$plugin->version = 2015102300;		// The current plugin version (Date: YYYYMMDDXX).
$plugin->requires = 2013111800; 	// Requires this Moodle version - requires Totara 2.6 or above
$plugin->component = 'local_androgogic_sync';	// Full name of the plugin (used for diagnostics)
