<?php
/**
 * Generic User matching/deduplication class 
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

class UserMatch {

	const RESULT_MATCHED = 1;
	const RESULT_NOMATCH = 0;
	const RESULT_REVIEW = -1;

	protected $_idnumber;
	protected $_firstname;
	protected $_lastname;
	protected $_middlename;
	protected $_alternatename;
	protected $_email;
	protected $_dob;
	protected $_username;
	protected $_matchcount;
	protected $_matcheduserid;
	protected $_matchedusername;
	protected $_matchedfields;

	public function findMatch($user) {
		global $DB;
		
		assert(is_object($user), '$user must be an object');
/*
        Fields that can be passed for matching are:
        idnumber (must contain UID)
        firstname
        lastname
        middlename (optional, requires Totara v2.6)
        alternatename (optional preferred first name, requires Totara v2.6)
        email
        profile_field_dateofbirth (requires user customfield in the format YYYY-MM-DD)
        username (optional)
        
        NOTE: Postgres is case sensitive
 */
        $this->_idnumber = isset($user->idnumber) ? strtolower(trim($user->idnumber)) : '';
        $this->_firstname = isset($user->firstname) ? strtolower(trim($user->firstname)) : '';
        $this->_lastname = isset($user->lastname) ? strtolower(trim($user->lastname)) : '';
        $this->_middlename = isset($user->middlename) ? strtolower(trim($user->middlename)) : '';
        $this->_alternatename = isset($user->alternatename) ? strtolower(trim($user->alternatename)) : '';
        $this->_username = isset($user->username) ? strtolower(trim($user->username)) : '';
        $this->_email = isset($user->email) ? strtolower(trim($user->email)) : '';
        $this->_dob = isset($user->profile_field_dateofbirth) ? trim($user->profile_field_dateofbirth) : '';
        $this->_firstandmiddle = trim($this->_firstname.' '.$this->_middlename);

		$this->_matchcount = 0;
		$this->_matcheduserid = array();
		$this->_matchedusername = array();
		$this->_matchedfields = array();
		
		$select = 'SELECT u.id, u.username, u.idnumber, u.firstname, u.lastname, u.alternatename, u.email FROM {user} u ';
		$joincustomfields = ' JOIN {user_info_data} d ON d.userid = u.id
				 JOIN {user_info_field} f ON d.fieldid = f.id ';		
		$namematch = " AND (LOWER(TRIM(u.firstname)) = :firstname OR LOWER(TRIM(u.firstname)) = :firstandmiddle)
				AND LOWER(TRIM(u.lastname)) = :lastname";
			
	    //		
		// look for UID matches (when idnumber is present)
		//
		if (!empty($this->_idnumber)) {  
			$sql = $select."WHERE LOWER(TRIM(u.idnumber)) = :idnumber AND TRIM(u.idnumber)<>''";
			if ($this->_findPartialMatch($sql, array('idnumber'=>$this->_idnumber)) > 0) {
				return $this->_matchcount;
			}
		}
			
		//
		// look for exact matches
		//
		if (!empty($this->_dob)) {
		    $date = explode('-', $this->_dob);
		    $data = make_timestamp($date[0], $date[1], $date[2]);
			$sql = $select.$joincustomfields."WHERE f.shortname = 'dateofbirth' 
				AND LEFT(d.data,10) = :data".$namematch; 
			if ($this->_findExactMatch($sql, array('data'=>$data,'firstname'=>$this->_firstname,'firstandmiddle'=>$this->_firstandmiddle,'lastname'=>$this->_lastname), array('firstname', 'lastname', 'DOB')) > 0) {
				return $this->_matchcount;
			}
		}
		if (!empty($user->email)) {
			$sql = $select."WHERE LOWER(TRIM(u.email)) = :email".$namematch;
			if ($this->_findExactMatch($sql, array('email'=>$this->_email,'firstname'=>$this->_firstname,'firstandmiddle'=>$this->_firstandmiddle,'lastname'=>$this->_lastname), array('firstname', 'lastname', 'email')) > 0) {
				return $this->_matchcount;
			}
		}
		if (!empty($user->username)) {
			$sql = $select."WHERE LOWER(TRIM(u.username)) = :username".$namematch;
			if ($this->_findExactMatch($sql, array('username'=>$this->_username,'firstname'=>$this->_firstname,'firstandmiddle'=>$this->_firstandmiddle,'lastname'=>$this->_lastname), array('firstname', 'lastname', 'username')) > 0) {
				return $this->_matchcount;
			}
		}
		
		//
		// look for partial matches
		//
		if (!empty($user->email)) {
			$sql = $select."WHERE LOWER(TRIM(u.email)) = :email";
			if ($this->_findPartialMatch($sql, array('email'=>$this->_email)) > 0) {
				return $this->_matchcount;
			}
		}
		if (!empty($user->username)) {
			$sql = $select."WHERE LOWER(TRIM(u.username)) = :username";
			if ($this->_findPartialMatch($sql, array('username'=>$this->_username)) > 0) {
				return $this->_matchcount;
			}
		}
		return $this->_matchcount;
	}
	
	public function getMatchCount() {
		return $this->_matchcount;
	}
	
	public function getMatchResult() {
		if ($this->_matchcount == 0) {
			$retval = self::RESULT_NOMATCH;
			
		} else if ($this->_matchcount == 1 and count($this->_matchedfields[1]) > 1) {
			$retval = self::RESULT_MATCHED;
			
		} else {
			// review is required when matched multiple users OR matched single user and insufficient matched fields
			$retval = self::RESULT_REVIEW;
		}
		return $retval;
	}		
	
	public function getMatchedUserid($index) {
		assert(is_numeric($index), '$index must be numeric');
		assert($index>0 && $index<=$this->_matchcount, "index $index is out of range");
		return $this->_matcheduserid[$index];
	}	
	
	public function getMatchedUsername($index) {
		assert(is_numeric($index), '$index must be numeric');
		assert($index>0 && $index<=$this->_matchcount, "index $index is out of range");
		return $this->_matchedusername[$index];
	}		
	
	public function getMatchedFields($index) {
		// returns comma-delimitted list of matched fields
		assert(is_numeric($index), '$index must be numeric');
		assert($index>0 && $index<=$this->_matchcount, "index $index is out of range");
		return implode(', ', $this->_matchedfields[$index]);
	}
    
	private function _findPartialMatch($sql, $params) {
		global $DB;
		
		assert(is_string($sql), '$sql must be a string');
		assert(is_array($params), 'params must be an array');

 		$rows = $DB->get_records_sql($sql, $params); 
		foreach ($rows as $row) {
			$this->_matchcount++;
			$this->_matcheduserid[$this->_matchcount] = $row->id;
			$this->_matchedusername[$this->_matchcount] = $row->username;
	
			if (!empty($this->_idnumber)) {
				if (!empty($row->idnumber) and $this->_stringCompare($row->idnumber, $this->_idnumber)) {
					$this->_matchedfields[$this->_matchcount][] = 'idnumber';
				}		
			}
			
			if (!empty($row->firstname) and $this->_stringCompare($row->firstname, $this->_firstname)) {
				$this->_matchedfields[$this->_matchcount][] = 'firstname';
			}
			
			if (!empty($row->lastname) and $this->_stringCompare($row->lastname, $this->_lastname)) {
				$this->_matchedfields[$this->_matchcount][] = 'lastname';
			}
			
			if (!empty($this->_alternatename)) {
				if (!empty($row->alternatename) and $this->_stringCompare($row->alternatename, $this->_alternatename)) {
					$this->_matchedfields[$this->_matchcount][] = 'alternatename';
				}
			}
			
			if (!empty($this->_middlename)) {
				if (!empty($row->middlename) and $this->_stringCompare($row->middlename, $this->_middlename)) {
					$this->_matchedfields[$this->_matchcount][] = 'middlename';
				}
			}
			
			if (!empty($this->_email)) {
				if (!empty($row->email) and $this->_stringCompare($row->email, $this->_email)) {
					$this->_matchedfields[$this->_matchcount][] = 'email';
				}	
			}
					
			if (!empty($this->_username)) {
				if (!empty($row->username) and $this->_stringCompare($row->username, $this->_username)) {
					$this->_matchedfields[$this->_matchcount][] = 'username';
				}	
			}
						
			if (!empty($this->_dob)) {
				$dateofbirth = androgogic_sync_get_user_custom_field($row->id, 'dateofbirth');
				if ($dateofbirth == $this->_dob) {
					$this->_matchedfields[$this->_matchcount][] = 'DOB';
				}
			}
		}	
		return $this->_matchcount;
	} 
	  
	private function _stringCompare($string1, $string2) {
		return strtolower(trim($string1)) == strtolower(trim($string2));
	}
	
	private function _findExactMatch($sql, $params, $matchfields) {
		global $DB;
		
		assert(is_string($sql), '$sql must be a string');
		assert(is_array($params), 'params must be an array');
		assert(is_array($matchfields), 'matchfields must be an array');
		
 		$rows = $DB->get_records_sql($sql, $params); 
		foreach ($rows as $row) {
			$this->_matchcount++;
			$this->_matchedfields[$this->_matchcount] = $matchfields;
			$this->_matcheduserid[$this->_matchcount] = $row->id;
			$this->_matchedusername[$this->_matchcount] = $row->username;
		}
		return $this->_matchcount;
	}
}
