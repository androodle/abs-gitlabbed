<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot.'/local/androgogic_sync/classes/synclog.class.php');

class editsourceform extends moodleform {

    // Define the form
    function definition() {
        global $DB;

        $mform =& $this->_form;
        $source = $this->_customdata['source'];
        $element = $this->_customdata['element'];
        
        $mform->addElement('text', 'shortname', get_string('shortname', 'local_androgogic_sync'), array("size"=>60));
        $mform->setType('shortname', PARAM_TEXT);
        
        if ($source == SyncLog::SOURCE_CSV) {
			$mform->addElement('header', 'filesettings', get_string('filesettings', 'local_androgogic_sync'));
       		$mform->setExpanded('filesettings');

        	$mform->addElement('text', 'csvfileprefix', get_string('csvfileprefix', 'local_androgogic_sync'));
        	$mform->setType('csvfileprefix', PARAM_TEXT);
        	$mform->addRule('csvfileprefix', null, 'required', null, 'client');
        	
			$options = array(
				','=>get_string('comma', 'local_androgogic_sync'),
				';'=>get_string('semicolon', 'local_androgogic_sync'),
				':'=>get_string('colon', 'local_androgogic_sync'),
				'\t'=>get_string('tab', 'local_androgogic_sync'),
				'|'=>get_string('pipe', 'local_androgogic_sync')
			);
			$mform->addElement('select', 'csvdelimiter', get_string('csvdelimiter', 'local_androgogic_sync'), $options);
			$mform->setType('csvdelimiter', PARAM_TEXT);

			$mform->addElement('selectyesno', 'csvheader', get_string('csvheader', 'local_androgogic_sync'));
			$mform->addHelpButton('csvheader', 'csvheader', 'local_androgogic_sync');
			$mform->setType('csvheader', PARAM_INT);
			$mform->setDefault('csvheader', 1);

			$mform->addElement('selectyesno', 'allrecords', get_string('allrecords', 'local_androgogic_sync'));
			$mform->addHelpButton('allrecords', 'allrecords', 'local_androgogic_sync');
			$mform->setType('allrecords', PARAM_INT);
			$mform->disabledIf('allrecords', 'element', 'eq', SyncLog::ELEMENT_COMP);
       
			if ($element == SyncLog::ELEMENT_USER) {			
				$options = array(
					'1'=>get_string('deleteuser', 'local_androgogic_sync'),
					'0'=>get_string('suspenduser', 'local_androgogic_sync')
				);
				$mform->addElement('select', 'userdeletion', get_string('obsoleteusers', 'local_androgogic_sync'), $options);
				$mform->setType('userdeletion', PARAM_INT);
				$mform->disabledIf('userdeletion', 'allrecords', 'eq', 0);
			}
		}
        if ($element == SyncLog::ELEMENT_USER or $element == SyncLog::ELEMENT_COMP) {			
			$dateformats = array(
				'Y-m-d',
				'Y/m/d',
				'Y M d',
				'Y-M-d',
				'd-m-Y',
				'd/m/Y',
				'd.m.Y',
				'd/m/y',
				'd M Y',
				'd-M-Y',
				'm/d/Y'
			);				
			
			$date = new DateTime('now');
			$options = array();
			foreach ($dateformats as $dateformat) {
				$dateformatoptions[$dateformat] = $dateformat.'   ('.$date->format($dateformat).')';
			}
			$mform->addElement('select', 'dateformat', get_string('dateformat', 'local_androgogic_sync'), $dateformatoptions);
			$mform->setType('dateformat', PARAM_TEXT);
        }
        
        if ($element == SyncLog::ELEMENT_USER or $element == SyncLog::ELEMENT_COMP ) {			

			$mform->addElement('header', 'fielddefaults', get_string('fielddefaults', 'local_androgogic_sync'));
       		$mform->setExpanded('fielddefaults');

			$auths = get_plugin_list('auth');
			$enabled = get_string('pluginenabled', 'core_plugin');
			$disabled = get_string('plugindisabled', 'core_plugin');
			$options = array($enabled=>array(), $disabled=>array());
			foreach ($auths as $auth=>$unused) {
				if (is_enabled_auth($auth)) {
					$options[$enabled][$auth] = get_string('pluginname', "auth_{$auth}");
				} else {
					$options[$disabled][$auth] = get_string('pluginname', "auth_{$auth}");
				}
			}
			$mform->addElement('selectgroups', 'auth', get_string('auth', 'local_androgogic_sync'), $options);
			$mform->addHelpButton('auth', 'auth', 'local_androgogic_sync');
			$mform->setDefault('auth', 'manual');
        	$mform->setType('auth', PARAM_TEXT);
        	        				
			$mform->addElement('selectyesno', 'forcepasswordchange', get_string('forcepasswordchange', 'local_androgogic_sync'));
			$mform->addHelpButton('forcepasswordchange', 'forcepasswordchange', 'local_androgogic_sync');
			$mform->setType('forcepasswordchange', PARAM_INT);
			
			$mform->addElement('selectyesno', 'policyagreed', get_string('policyagreed', 'local_androgogic_sync'));
			$mform->setType('policyagreed', PARAM_INT);
			
			$mform->addElement('selectyesno', 'senduseremail', get_string('senduseremail', 'local_androgogic_sync'));
			$mform->addHelpButton('senduseremail', 'senduseremail', 'local_androgogic_sync');
			$mform->setType('senduseremail', PARAM_INT);	
					
			//$mform->addElement('selectyesno', 'allowduplicatedemails', get_string('allowduplicatedemails', 'local_androgogic_sync'));
			//$mform->setType('allowduplicatedemails', PARAM_INT);
        }
        
        if ($element == SyncLog::ELEMENT_USER or $element == SyncLog::ELEMENT_ORG) {
            //
            // organisation framework
            //
   			$items = $DB->get_records_menu('org_framework',array(),'fullname','id,fullname');
        	if ($element == SyncLog::ELEMENT_USER) {
        		// insert empty first option for optional field
   				$options = array();
   				$options[0] = '';
   				foreach ($items as $k => $v) {
   					$options[$k] = $v;
   				}
   			} else {
   				$options = $items;
   			}	
			$mform->addElement('select', 'orgframeworkid', get_string('orgframework', 'local_androgogic_sync'), $options);
        	$mform->setType('orgframeworkid', PARAM_INT); 
        }
        
        if ($element == SyncLog::ELEMENT_USER or $element == SyncLog::ELEMENT_POS) {
        	//
            // position framework
            //
   			$items = $DB->get_records_menu('pos_framework',array(),'fullname','id,fullname');
        	if ($element == SyncLog::ELEMENT_USER) {
        		// insert empty first option for optional field
   				$options = array();
   				$options[0] = '';
   				foreach ($items as $k => $v) {
   					$options[$k] = $v;
   				}
   			} else {
   				$options = $items;
   			}
			$mform->addElement('select', 'posframeworkid', get_string('posframework', 'local_androgogic_sync'), $options);
        	$mform->setType('posframeworkid', PARAM_INT);
        }
        
        if ($element == SyncLog::ELEMENT_COMP) {
        	$options = array(
				'1'=>get_string('createcourses', 'local_androgogic_sync'),
				'0'=>get_string('createlearningplan', 'local_androgogic_sync')
			);
			$mform->addElement('select', 'createcourses', get_string('coursenotfound', 'local_androgogic_sync'), $options);
			$mform->setType('createcourses', PARAM_INT);

            // course category to create courses
   			$options = $DB->get_records_menu('course_categories',array(),'name','id,name');
			$mform->addElement('select', 'coursecategory', get_string('coursecategory', 'local_androgogic_sync'), $options);
        	$mform->setType('coursecategory', PARAM_INT);
			$mform->disabledIf('coursecategory', 'createcourses', 'eq', 0);
    
            // evidencetype to create courses
   			$options = $DB->get_records_menu('dp_evidence_type',array(),'name','id,name');
			$mform->addElement('select', 'evidencetypeid', get_string('evidencetype', 'local_androgogic_sync'), $options);
        	$mform->setType('evidencetypeid', PARAM_INT);
			$mform->disabledIf('evidencetypeid', 'createcourses', 'eq', 1);
        }       
        
        //
		// field mapping
		//
		if ($element == SyncLog::ELEMENT_USER) {
		
			$fieldnames = array(
				'idnumber',
				'firstname',
				'lastname',
				'alternatename',
				'middlename',
				'username',
				'deleted',
				'suspended',
				'auth',
				'email',
				'address',
				'city',
				'country',
				'phone1',
				'phone2',
				'timezone',
				'orgname',
				'orgidnumber',
				'managername',
				'manageridnumber',
				'appraiseridnumber',
				'posidnumber',
				'posfullname',
				'posstartdate',
				'posenddate');

		} else if ($element == SyncLog::ELEMENT_ORG or $element == SyncLog::ELEMENT_POS) {

			$fieldnames = array(
				'fullname',
				'idnumber',
				'parentidnumber',
				'typeidnumber',
				'deleted');
		
		} else if ($element == SyncLog::ELEMENT_COMP) {

			$fieldnames = array(
				'useridnumber',
				'username',
				'firstname',
				'lastname',
				'email',
				'courseidnumber',
				'courseshortname',
				'enroldate',
				'startdate',
				'completedate',
				'grade');
		}
	
		$mform->addElement('header', 'fieldmapping', get_string('fieldmapping', 'local_androgogic_sync'));
		$mform->setExpanded('fieldmapping');
		
		$mform->addElement('static', 'fieldmapping_help', '', html_writer::tag('p', get_string('fieldmapping_help', 'local_androgogic_sync')));
		foreach ($fieldnames as $fieldname) {
			$elementname = 'field_'.$fieldname;
			if ($source == SyncLog::SOURCE_CSV) {
				$options = array();
				$options[0] = '';
				for ($i = 1; $i < 50; $i++) {
					$options[$i] = "$i.   ".$this->num2alpha($i-1);
				} 
				
				$mform->addElement('select', $elementname, $fieldname, $options);
				$mform->setType($elementname, PARAM_INT);
			} else {
				$mform->addElement('checkbox', $elementname, $fieldname);
			}
		}
		$mform->addRule('shortname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
		$mform->addRule('field_idnumber', get_string('idnumberuid', 'local_androgogic_sync'), 'required', null, 'client');
		$mform->addRule('field_idnumber', get_string('idnumberuid', 'local_androgogic_sync'), 'nonzero', null, 'client'); 
	
		if ($element == SyncLog::ELEMENT_USER) {
			$mform->addRule('field_firstname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_firstname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client'); 
			$mform->addRule('field_lastname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_lastname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client'); 
		} else if ($element == SyncLog::ELEMENT_ORG or $element == SyncLog::ELEMENT_POS) {
			$mform->addRule('field_fullname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_fullname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
		} else if ($element == SyncLog::ELEMENT_COMP) {
			$mform->addRule('field_username', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_username', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			$mform->addRule('field_firstname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_firstname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			$mform->addRule('field_lastname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_lastname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			$mform->addRule('field_email', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_email', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			$mform->addRule('field_useridnumber', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_useridnumber', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			//$mform->addRule('field_courseidnumber', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			//$mform->addRule('field_courseidnumber', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
			$mform->addRule('field_courseshortname', get_string('requiredfield', 'local_androgogic_sync'), 'required', null, 'client');
			$mform->addRule('field_courseshortname', get_string('requiredfield', 'local_androgogic_sync'), 'nonzero', null, 'client');  
		}

		//
		// user custom field mapping
		//
		if ($element == SyncLog::ELEMENT_USER or $element == SyncLog::ELEMENT_COMP) {
			$rows = $DB->get_records('user_info_field');
			if (count($rows) > 0) {
				$mform->addElement('header', 'customfields', get_string('customfields', 'local_androgogic_sync'));
				$mform->setExpanded('customfields');
				
				$options = array();
				$options[0] = '';
				for ($i = 1; $i < 50; $i++) {
					$options[$i] = "$i.   ".$this->num2alpha($i-1);
				}
				foreach ($rows as $row) {
					$fieldname = 'profile_field_'.$row->shortname;
					if ($source == SyncLog::SOURCE_CSV) {
						$mform->addElement('select', $fieldname, "$row->name<br>($fieldname)", $options);
						$mform->setType($fieldname, PARAM_INT);
					} else {
						$mform->addElement('checkbox', $fieldname, "$row->name<br>($fieldname)");
					}
				}
			} 
		}
	
        /// hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'visible');
        $mform->setType('visible', PARAM_INT);
		$mform->setDefault('visible', 1);
		
       	$mform->addElement('hidden', 'source');
       	$mform->setType('source', PARAM_ALPHA);
		$mform->setDefault('source', $source);

       	$mform->addElement('hidden', 'element');
       	$mform->setType('element', PARAM_ALPHA);
		$mform->setDefault('element', $element);
        
        $this->add_action_buttons();
    }

    function validation($data, $files) {
        $errors = array();
        $data = (object)$data;

        if ($data->element == SyncLog::ELEMENT_COMP) {
    		if ($data->createcourses == 1 and empty($data->coursecategory)) {
                $errors['coursecategory'] = 'Course category is required to create courses';
    		} else if ($data->createcourses == 0 and empty($data->evidencetypeid)) {
                $errors['evidencetypeid'] = 'Evidence type is required to create evidence items';
            }
    	}
        return $errors;
    }
    
	/**
	* Converts an integer into the alphabet base (A-Z).
	* (e.g. the column names in a Microsoft Windows Excel sheet..A-Z, AA-ZZ, AAA-ZZZ, ...)
	*
	* @param int $n This is the number to convert.
	* @return string The converted number.
	* @author Theriault
	* 
	*/
	function num2alpha($n) {
		$r = '';
		for ($i = 1; $n >= 0 && $i < 10; $i++) {
			$r = chr(0x41 + ($n % pow(26, $i) / pow(26, $i - 1))) . $r;
			$n -= pow(26, $i);
		}
		return $r;
	}
}
