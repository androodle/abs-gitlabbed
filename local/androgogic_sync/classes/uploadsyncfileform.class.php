<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     August 2015
 *
 **/
 
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class uploadsyncfileform extends moodleform {
    function definition() {
        global $CFG, $FILEPICKER_OPTIONS;

        $mform =& $this->_form;
        $source = $this->_customdata['source'];
        $element = $this->_customdata['element'];
        $id = $this->_customdata['id'];

        $options = $FILEPICKER_OPTIONS;
		$options['maxbytes'] = get_max_upload_file_size($CFG->maxbytes);
		$options['subdirs'] = 0;
		$options['maxfiles'] = 1;
		$options['forcehttps'] = true;
		$options['accepted_types'] = array('.csv');

        $mform->addElement('filepicker', 'syncfile', get_string('syncfile', 'local_androgogic_sync'), null, $options);
        $mform->setType('syncfile', PARAM_FILE);
		$mform->addRule('syncfile', null, 'required', null, 'client');
		
        ///       
        /// hidden fields
        ///
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
		$mform->setDefault('id', $id);
		
       	$mform->addElement('hidden', 'source');
       	$mform->setType('source', PARAM_ALPHA);
		$mform->setDefault('source', $source);

       	$mform->addElement('hidden', 'element');
       	$mform->setType('element', PARAM_ALPHA);
		$mform->setDefault('element', $element);
        
        $this->add_action_buttons();
    }

	function validation($data, $files){
		$errors = parent::validation($data, $files);

		return $errors;
	}
}
