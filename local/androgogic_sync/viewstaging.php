<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     October 2015
 *
 **/

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once("{$CFG->libdir}/adminlib.php");
require_once('classes/synclog.class.php');

global $DB;

require_login();

// Get params.
$logid    = required_param('logid', PARAM_INT);

$context = context_system::instance();
require_capability('local/androgogic_sync:viewlog', $context);
$PAGE->set_context($context);

$urlparams = array('logid' => $logid);
$PAGE->set_url('/local/androgogic_sync/viewstaging.php', $urlparams);
//$PAGE->set_pagelayout('popup');
$PAGE->set_pagelayout('admin');

$synclog = $DB->get_record('androgogic_sync_log', array('id' => $logid), '*', MUST_EXIST);
$source = $DB->get_record('androgogic_sync_source', array('id' => $synclog->sourceid), '*', MUST_EXIST);

if ($source->element == SyncLog::ELEMENT_USER) {
    $tablename = 'androgogic_sync_user'; 
} else if ($source->element == SyncLog::ELEMENT_ORG) {
    $tablename = 'androgogic_sync_org';
} else if ($source->element == SyncLog::ELEMENT_POS) {
    $tablename = 'androgogic_sync_pos';
} else if ($source->element == SyncLog::ELEMENT_COMP) {
    $tablename = 'androgogic_sync_comp';  
}
$staging = $DB->get_record($tablename, array('id' => $synclog->stagingid), '*', MUST_EXIST);
$staging->timecreated = date('Y-m-d H:i:s', $staging->timecreated);

if ($source->element == SyncLog::ELEMENT_USER or $source->element == SyncLog::ELEMENT_COMP) {
    $sql = "SELECT s.*, i.datatype, i.name FROM {androgogic_sync_user_custom} s
              JOIN {user_info_field} i ON i.shortname = s.shortname
             WHERE s.stagingid = $staging->id
               AND s.sourceid = $source->id";
    if ($rows = $DB->get_records_sql($sql)) {
        foreach ($rows as $row) {
            //$fieldname = 'profile_field_'.$row->shortname.' ('.$row->name.')';
            $fieldname = 'profile_field_'.$row->shortname;
            if (!empty($row->data) and $row->datatype == 'datetime' and $source->dateformat != 'Y-m-d') {
                //
                // convert custom field dates when CSV date format is not Y-m-d
                //
                $time = totara_date_parse_from_format($source->dateformat, $row->data);
                $staging->$fieldname = date('Y-m-d', $time);
            } else {
                $staging->$fieldname = $row->data;
            }
        }
    }
}
unset($staging->runid);
unset($staging->sourceid);
unset($staging->processed);

///
/// Generate page
///

// Create display table.
$table = new html_table();

// Setup column headers.
$table->head = array('Field', 'Value');

// Add rows to table.
foreach ($staging as $key=>$value) {
    $row = array();
    
    $mappedfield = $DB->record_exists('androgogic_sync_field', array('sourceid'=>$source->id, 'dbfieldname'=>$key));
    
    if (!$mappedfield and ($key == 'deleted' or $key == 'suspended')) {
        continue;
    }
    
    if ($mappedfield or !empty($value)) {
    
        $cssclass = $mappedfield ? '' : 'dimmed';
    
        $row[] = html_writer::tag('span', format_string($key), array('class'=>$cssclass));

        $row[] = html_writer::tag('span', format_string($value), array('class'=>$cssclass));

        $table->data[] = $row;
    }
}

///
/// Display page
///
//$elementname = get_string($source->element, 'local_androgogic_sync');
//$sourcename = get_string($source->source, 'local_androgogic_sync');		
//$heading = "$elementname $sourcename";
$pagetitle = get_string('viewstaging', 'local_androgogic_sync');

$PAGE->set_title($pagetitle);

$PAGE->navbar->add(get_string('pluginname', 'local_androgogic_sync'));
$PAGE->navbar->add(get_string('viewlog', 'local_androgogic_sync'), new moodle_url('/local/androgogic_sync/synclog.php'));
$PAGE->navbar->add($pagetitle);

echo $OUTPUT->header();

echo $OUTPUT->heading($pagetitle);
echo html_writer::table($table);

echo $OUTPUT->footer();
