<?php

/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     June 2015
 *
 **/
 
require_once($CFG->dirroot . '/local/androgogic_sync/userlib.php');

defined('MOODLE_INTERNAL') || die();

function androgogic_sync_process_comp_source($log, $source, $progressbar=null) {
    global $DB, $USER;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    
	if ($progressbar<>null) {
		$message = 'updating course completions';
		$count = 0;
		$total = $DB->count_records('androgogic_sync_comp', array('runid'=>$log->runid, 'sourceid'=>$source->id));
		$progressbar->update_full(0, $message);
	}
	
    $log->count_comp_created = 0;
    $log->count_comp_skipped = 0;

    $log->count_course_created = 0;
    $log->count_evidence_created = 0;

	$log->count_user_created = 0;
    $log->count_user_updated = 0;
    $log->count_user_skipped = 0;
    
    // skip records that are missing required fields
 	androgogic_sync_comp_required_field($log, $source, 'useridnumber');
 	androgogic_sync_comp_required_field($log, $source, 'username');
 	androgogic_sync_comp_required_field($log, $source, 'firstname');
 	androgogic_sync_comp_required_field($log, $source, 'lastname');
 	androgogic_sync_comp_required_field($log, $source, 'email');
 	//androgogic_sync_comp_required_field($log, $source, 'courseidnumber');
 	androgogic_sync_comp_required_field($log, $source, 'courseshortname');

	$pluginname = get_string('pluginname', 'local_androgogic_sync');
	
    $rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_comp} 
    	WHERE runid=$log->runid AND sourceid=$source->id AND processed=0");
    if ($rs->valid()) {
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;
            $sucess = false;
            
			if ($progressbar<>null) {
				$count++;
				$progressbar->update($count, $total, $message);
			}
			
        	$mappedfields = androgogic_sync_load_mapped_fields($source, $staging);
        	
			$mappedfields->idnumber = $mappedfields->useridnumber;
			$success = androgogic_sync_process_user($log, $source, $mappedfields);
			unset($mappedfields->idnumber);

			if ($success) { 
				$timestart = 0;
				if (!empty($mappedfields->startdate)) {
					$timestart = totara_date_parse_from_format($source->dateformat, $mappedfields->startdate);
					if ($timestart == -1) {
						$log->add_to_log(SyncLog::TYPE_WARNING, "unable to set course start date, invalid startdate: $mappedfields->startdate", "$mappedfields->courseshortname, courseidnumber=$mappedfields->courseidnumber, userid=$mappedfields->userid");
						$timestart = 0;
					}
				}
				if ($timestart == 0 and !empty($mappedfields->enroldate)) {
					$timestart = totara_date_parse_from_format($source->dateformat, $mappedfields->enroldate);
					if ($timestart == -1) {
						$log->add_to_log(SyncLog::TYPE_WARNING, "unable to set course enrol date, invalid enroldate: $mappedfields->enroldate", "$mappedfields->courseshortname, courseidnumber=$mappedfields->courseidnumber, userid=$mappedfields->userid");
						$timestart = 0;
					}
				}
		
				$timecompleted = 0;
				if (!empty($mappedfields->completedate)) {
					$timecompleted = totara_date_parse_from_format($source->dateformat, $mappedfields->completedate);
					if ($timecompleted == -1) {
						$log->add_to_log(SyncLog::TYPE_WARNING, "unable to set course completion date, invalid completedate: $mappedfields->completedate", "$mappedfields->courseshortname, courseidnumber=$mappedfields->courseidnumber, userid=$mappedfields->userid");
						$timecompleted = 0;
					}
				}
			
				$courseid = $DB->get_field_sql('SELECT id FROM {course} WHERE LOWER(shortname) = LOWER(:shortname)', array('shortname'=>$mappedfields->courseshortname));
				if (empty($courseid) and $source->createcourses == 1) {
					//
					// create missing course
					//
					$course = new stdClass();
					$course->idnumber = $mappedfields->courseidnumber;
					$course->shortname = $mappedfields->courseshortname;
					$course->category = $source->coursecategory;
					$course = create_course($course);	
					$courseid = $course->id;
					$log->count_course_created++;
					$log->add_to_log(SyncLog::TYPE_TRACE, 'course created', "$mappedfields->courseshortname, courseidnumber=$mappedfields->courseidnumber");
				}
				if (empty($courseid)) {
					//
					// add missing courses to other training (evidence)					
					//
					$evidenceid = $DB->get_field('dp_plan_evidence', 'id', array('userid'=>$mappedfields->userid, 'name'=>$mappedfields->courseidnumber, 'datecompleted'=>$timecompleted));
					if (empty($evidenceid)) {
						$item = new stdClass();
						$item->name = $mappedfields->courseidnumber;
						$item->description = $mappedfields->courseshortname;
						$item->datecompleted = $timecompleted;
						$item->evidencetypeid = $source->evidencetypeid;
						$item->timecreated = time();
						$item->userid = $mappedfields->userid;
						$item->timemodified = $item->timecreated;
						$item->usermodified = $USER->id;
						$item->readonly = 1;
						if (!$DB->insert_record('dp_plan_evidence', $item)) {
							throw new Exception($DB->get_last_error());	
						}
						$log->count_evidence_created++;
						$log->add_to_log(SyncLog::TYPE_TRACE, 'evidence created', "$mappedfields->courseshortname, courseidnumber=$mappedfields->courseidnumber, userid=$mappedfields->userid");
					}
				} else {
					//
					// create course completion
					//
					if (androgogic_sync_enrol_user($log, $mappedfields->userid, $courseid, $timestart, $timecompleted)) {
					    androgogic_sync_create_course_completion($log, $mappedfields->userid, $courseid, $timecompleted, $pluginname, (isset($mappedfields->grade) ? $mappedfields->grade : NULL));
					}
				} 
			}
			
			if ($success) {
				$processed = 1;
			} else {
				$processed = -1;
			}
			$DB->execute("UPDATE {androgogic_sync_comp} SET processed=$processed WHERE id=$staging->id");
		}
    }
    $rs->close();
    
	if ($progressbar<>null) {
		$progressbar->update_full(100, $message);
	}

    $log->stagingid = NULL;
    $log->add_to_log(SyncLog::TYPE_INFO, "course completion totals: $log->count_comp_created created, $log->count_comp_skipped skipped");
	if ($source->createcourses == 1) {
    	$log->add_to_log(SyncLog::TYPE_INFO, "course totals: $log->count_course_created created");
    } else {
    	$log->add_to_log(SyncLog::TYPE_INFO, "evidence totals: $log->count_evidence_created created");
	}
    $log->add_to_log(SyncLog::TYPE_INFO, "user totals: $log->count_user_created created, $log->count_user_updated updated, $log->count_user_skipped skipped");
}
    
function androgogic_sync_enrol_user($log, $userid, $courseid, $timestart=0, $timeend=0) {
	global $DB;
		
	if (empty($timestart)) {
		$timestart=$timeend;
	}

	if (!$instance = $DB->get_record('enrol', array('enrol'=>'manual', 'courseid'=>$courseid, 'status'=>ENROL_INSTANCE_ENABLED))) {
		$coursename = $DB->get_field('course', 'shortname', array('id'=>$courseid));

    	$log->add_to_log(SyncLog::TYPE_ERROR, "enrolment failed, manual enrolment is not enabled for this course", "$coursename, courseid=$courseid, userid=$userid");
        return false;
	}

	// check if already enrolled
	$enrolment = $DB->get_record('user_enrolments', array('enrolid'=>$instance->id, 'userid'=>$userid));	
	if (!$enrolment) {
		$coursename = $DB->get_field('course', 'shortname', array('id'=>$courseid));

		// we use only manual enrol plugin here
		if (!$plugin = enrol_get_plugin('manual')) {
    	    $log->add_to_log(SyncLog::TYPE_ERROR, "enrolment failed, cannot get manual enrolment plugin", "$coursename, courseid=$courseid, userid=$userid");
            return false;
		}
                
		// enrol user in course
		$roleid = $DB->get_field('role', 'id', array('name'=>'Learner'), MUST_EXIST);
		$plugin->enrol_user($instance, $userid, $roleid, $timestart, $timeend);
		
		$log->add_to_log(SyncLog::TYPE_TRACE, 'user enrolled', "$coursename, courseid=$courseid, userid=$userid");

    } else if ($enrolment->status == ENROL_USER_SUSPENDED) {
    	$coursename = $DB->get_field('course', 'shortname', array('id'=>$courseid));

		// we use only manual enrol plugin here
		if (!$plugin = enrol_get_plugin('manual')) {
    	    $log->add_to_log(SyncLog::TYPE_ERROR, "enrolment failed, cannot get manual enrolment plugin", "$coursename, courseid=$courseid, userid=$userid");
            return false;
		}
		
        // unsuspend enrolment
        $plugin->update_user_enrol($instance, $userid, ENROL_USER_ACTIVE);

		$log->add_to_log(SyncLog::TYPE_TRACE, 'user enrolment unsuspended', "$coursename, courseid=$courseid, userid=$userid");
	}
	return true;
}

function androgogic_sync_create_course_completion($log, $userid, $courseid, $timecompleted=NULL, $rpl=NULL, $rplgrade=NULL) {
	global $DB;
		
    // Load course completion
	$completion = new completion_completion(array('userid'=>$userid, 'course'=>$courseid));

	if (!$completion) {
		$coursename = $DB->get_field('course', 'shortname', array('id'=>$courseid));
		$log->add_to_log(SyncLog::TYPE_ERROR, 'cannot mark course complete', "$coursename, courseid=$courseid, userid=$userid");
	} else {	
		// Check if already marked complete
		if (!$completion->is_complete()) {		
			if (!empty($rpl)) {
				$completion->rpl = $rpl;
			}			
			if (!empty($rplgrade)) {
				$completion->rplgrade = $rplgrade;
			}
			$completion->status = COMPLETION_STATUS_COMPLETEVIARPL;
			$completion->mark_complete($timecompleted);
			
			$log->count_comp_created++;
			$coursename = $DB->get_field('course', 'shortname', array('id'=>$courseid));
			$log->add_to_log(SyncLog::TYPE_TRACE, 'course completion created', "$coursename, courseid=$courseid, userid=$userid");
		}
	}
}

function androgogic_sync_comp_required_field($log, $source, $fieldname) {
	global $DB;
	
	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$source must be an object');
	assert(is_string($fieldname), '$fieldname must be a string');
	//
	// validate required fields
	//
	$rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_comp} 
		WHERE runid=$log->runid AND sourceid=$source->id AND processed=0 AND TRIM($fieldname) = ''");
	if ($rs->valid()) {
        foreach ($rs as $staging) {
        	$log->stagingid = $staging->id;
			$log->count_comp_skipped++;
			$log->add_to_log(SyncLog::TYPE_WARNING, "completion skipped: $fieldname is required", "useridnumber=$staging->useridnumber, courseidnumber=$staging->courseidnumber, courseshortname=$staging->courseshortname");
    		$DB->execute("UPDATE {androgogic_sync_comp} SET processed=-1 WHERE id=$staging->id");
        }
    }
    $rs->close();
}
