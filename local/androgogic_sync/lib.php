<?php

/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 * */
defined('MOODLE_INTERNAL') || die();

require_once('classes/synclog.class.php');
require_once('csvlib.php');
require_once('userlib.php');
require_once('hierarchylib.php');
require_once('completionlib.php');

function local_androgogic_sync_cron() {

    $starthour = date('G');
    $cronhours = get_config('local_androgogic_sync', 'cron_hours');
    if (!empty($cronhours) && in_array($starthour, explode(',', $cronhours))) {
        if (androgogic_sync_get_cronlock()) {
            mtrace("AndroSync cron started.");
            androgogic_sync_run();
            mtrace("AndroSync cron ended.");
        } else {
            mtrace("AndroSync cron is already running.");
        }
    }
}

function androgogic_sync_get_cronlock() {
    global $CFG;

    $lockfile = $CFG->dataroot . '/' . $CFG->dbname . '_androgogic_sync.lock';
    if (file_exists($lockfile)) {
        // Check if file modification time is more than a day old. If so, something 
        // has probably gone wrong, and the lock file is not valid anymore.
        if (filemtime($lockfile) < time() - 86400) {
            unlink($lockfile);
        } else {
            return false;
        }
    }
    if (!touch($lockfile)) {
        return false;
    }
    register_shutdown_function('androgogic_sync_release_cronlock');
    return true;
}

function androgogic_sync_release_cronlock() {
    global $CFG;

    // make sure the lockfile is deleted
    // before the script is complete.

    $lockfile = $CFG->dataroot . '/' . $CFG->dbname . '_androgogic_sync.lock';
    unlink($lockfile);
}

function androgogic_sync_run($runfrombrowser = false) {
    global $DB, $USER;

    $config = get_config('local_androgogic_sync');
    if (empty($config->sync_dir)) {
        throw new Exception('Sync directory setting is required');
    }
    if (!is_dir($config->sync_dir)) {
        throw new Exception("Sync directory '$config->sync_dir' is not found");
    }
    if (empty($config->archive_dir)) {
        throw new Exception('Archive directory setting is required');
    }
    if (!is_dir($config->sync_dir)) {
        throw new Exception("Archive directory '$config->archive_dir' is not found");
    }

    if ($runfrombrowser) {
        $progressbar = new progress_bar('syncprogress', 500, true);
    } else {
        $progressbar = null;
    }

    try {
        raise_memory_limit(MEMORY_EXTRA);
        // Stop time outs, this might take a while
        set_time_limit(0);
        $timestart = microtime(true);

        $log = new SyncLog();
        $message = get_string('syncstarted', 'local_androgogic_sync');
        $log->add_to_log(SyncLog::TYPE_INFO, $message, 'run by ' . ($runfrombrowser ? "$USER->firstname $USER->lastname, username=$USER->username" : 'CRON'));
        if ($runfrombrowser) {
            $progressbar->update_full(0, $message);
        }

        //
        // remove expired staging data
        //
		$retention = get_config('local_androgogic_sync', 'staging_retention');
        if (!empty($retention) and is_numeric($retention)) {
        	$unixtime = time() - ($retention * 24 * 60 * 60);
            if ($DB->count_records_sql("SELECT COUNT(*) FROM {androgogic_sync_user} WHERE timecreated < $unixtime") > 0
            or $DB->count_records_sql("SELECT COUNT(*) FROM {androgogic_sync_org} WHERE timecreated < $unixtime") > 0
            or $DB->count_records_sql("SELECT COUNT(*) FROM {androgogic_sync_pos} WHERE timecreated < $unixtime") > 0
            or $DB->count_records_sql("SELECT COUNT(*) FROM {androgogic_sync_comp} WHERE timecreated < $unixtime") > 0) {
                $DB->execute("DELETE c FROM {androgogic_sync_user_custom} c
					INNER JOIN {androgogic_sync_user} u ON u.id = c.stagingid and u.sourceid = c.sourceid
					WHERE u.timecreated < $unixtime");
                $DB->execute("DELETE FROM {androgogic_sync_user} WHERE timecreated < $unixtime");
                $DB->execute("DELETE FROM {androgogic_sync_org} WHERE timecreated < $unixtime");
                $DB->execute("DELETE FROM {androgogic_sync_pos} WHERE timecreated < $unixtime");
                $DB->execute("DELETE c FROM {androgogic_sync_user_custom} c
					INNER JOIN {androgogic_sync_comp} comp ON comp.id = c.stagingid and comp.sourceid = c.sourceid
					WHERE comp.timecreated < $unixtime");
                $DB->execute("DELETE FROM {androgogic_sync_comp} WHERE timecreated < $unixtime");
                $log->add_to_log(SyncLog::TYPE_TRACE, "deleted staging data older than $retention days");
            }
        }

        if ($sources = $DB->get_records_sql("SELECT * FROM {androgogic_sync_source} WHERE deleted=0 AND visible=1 ORDER BY sortorder ASC")) {
            foreach ($sources as $source) {
                $log->sourceid = $source->id;

                if ($source->source == SyncLog::SOURCE_CSV) {
                    $csvfiles = androgogic_sync_get_matching_files($config->sync_dir, $source->csvfileprefix);
                    if (count($csvfiles) == 0) {
                        $elementname = get_string($source->element, 'local_androgogic_sync');
                        $log->add_to_log(SyncLog::TYPE_INFO, "no $elementname CSV files to sync with prefix $source->csvfileprefix");
                    } else {
                        foreach ($csvfiles as $filename) {
                            $archivefilename = androgogic_sync_archive_csvfile($filename);
                            if (androgogic_sync_validate_csvfile($log, $source, $archivefilename) === true) {
                                androgogic_sync_load_csvfile($log, $source, $archivefilename, $progressbar);
                                androgogic_sync_process_source($log, $source, $progressbar);
                            } 
                        }
                    }
                    
                } else if ($source->source == SyncLog::SOURCE_WEBSERVICE) {
                    // set runid of the web service records to be processed 
                    $tablename = 'androgogic_sync_'.strtolower($source->element);
                    $DB->execute("UPDATE {{$tablename}} SET runid=$log->runid WHERE runid=0 AND sourceid=$source->id AND processed=0");
                    androgogic_sync_process_source($log, $source, $progressbar);
                }
            }
        }    
        $message = get_string('syncsuccess', 'local_androgogic_sync');
        
    } catch (Exception $e) {
        $log->error_to_log($e);
        $message = get_string('syncproblem', 'local_androgogic_sync');
    }

	$log->stagingid = NULL;
	$log->sourceid = NULL;
	$warnings = $DB->count_records('androgogic_sync_log', array('runid' => $log->runid, 'logtype' => SyncLog::TYPE_WARNING));
	$errors = $DB->count_records('androgogic_sync_log', array('runid' => $log->runid, 'logtype' => SyncLog::TYPE_ERROR));
	$timeend = microtime(true);
	$log->add_to_log(SyncLog::TYPE_INFO, "$message, $warnings warnings, $errors errors", 'execution time ' . number_format($timeend - $timestart, 2) . 's');

    if ($runfrombrowser) {
        $progressbar->update_full(100, $message);
    }

    $notifyemailto = get_config('local_androgogic_sync', 'notify_emailto');
    if (!empty($notifyemailto)) {
        $notifyinfo = get_config('local_androgogic_sync', 'notify_info');
        $notifywarning = get_config('local_androgogic_sync', 'notify_warning');
        $notifyreview = get_config('local_androgogic_sync', 'notify_review');
        $notifyerror = get_config('local_androgogic_sync', 'notify_error');
        $logtypes = array();
        if (!empty($notifyinfo)) {
            $logtypes[] = SyncLog::TYPE_INFO;
        }
        if (!empty($notifywarning)) {
            $logtypes[] = SyncLog::TYPE_WARNING;
        }
        if (!empty($notifyreview)) {
            $logtypes[] = SyncLog::TYPE_REVIEW;
        }
        if (!empty($notifyerror)) {
            $logtypes[] = SyncLog::TYPE_ERROR;
        }
        if (count($logtypes) > 0) {
            androgogic_sync_notify($log->runid, $notifyemailto, $logtypes);
        }
    }
    return;
}

function androgogic_sync_process_source($log, $source, $progressbar) {
    global $DB;

	$reccount = $DB->count_records('androgogic_sync_'.strtolower($source->element), array('runid' => $log->runid, 'sourceid' => $source->id, 'processed' => '0'));
	if ($reccount > 0) {
		if ($source->element == SyncLog::ELEMENT_USER) {
		
			androgogic_sync_process_user_source($log, $source, $progressbar);
			
		} else if ($source->element == SyncLog::ELEMENT_ORG) {

			$hierarchy = new organisation();
			androgogic_sync_process_hierarchy_source($log, $source, $hierarchy, $progressbar);
			
		} else if ($source->element == SyncLog::ELEMENT_POS) {

			$hierarchy = new position();
			androgogic_sync_process_hierarchy_source($log, $source, $hierarchy, $progressbar);
			
		} else if ($source->element == SyncLog::ELEMENT_COMP) {

			androgogic_sync_process_comp_source($log, $source, $progressbar);
		}
	}
}

function androgogic_sync_send_email($userid, $messagestringname) {
    global $DB, $CFG;

    assert(is_numeric($userid), '$userid must be numeric');
    assert(is_string($messagestringname), '$messagestringname must be a string');
	
	$user = $DB->get_record('user', array('id' => $userid));

    $site = get_site();
    $supportuser = core_user::get_support_user();

    $username = urlencode($user->username);
    $username = str_replace('.', '%2E', $username); // prevent problems with trailing dots

    $userauth = $DB->get_field('user', 'auth', array('username' => $user->username));
    if (!$userauth === false) {
    	$auth = get_auth_plugin($userauth);
    	if ($auth->can_change_password() and $auth->change_password_url()) {
        	$data->changepasswordurl = $auth->change_password_url();
        }
    }
    $data = new stdClass();
    $data->firstname = $user->firstname;
    $data->sitename = format_string($site->fullname);
    $data->siteurl = $CFG->wwwroot;
    $data->username = $username;

    $message = get_string($messagestringname, 'local_androgogic_sync', $data);
    $messagehtml = text_to_html(get_string($messagestringname, 'local_androgogic_sync', $data), false, false, true);
    $subject = $site->fullname;

    $user->mailformat = 1;  // Always send HTML version as well
    return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
}

function androgogic_sync_fields($dbfields, $stagingfields) {
    $modified = false;
    foreach ($dbfields as $key => $value) {
        if (isset($stagingfields->$key)) {
            if ($value != $stagingfields->$key) {
                $dbfields->$key = $stagingfields->$key;
                $modified = true;
            }
        }
    }
    return $modified;
}

function androgogic_sync_get_matching_files($filedir, $fileprefix) {

    assert(is_string($filedir), '$filedir must be a string');
    assert(is_string($fileprefix), '$fileprefix must be a string');
    assert(!empty($filedir), '$filedir is required');
    assert(!empty($fileprefix), '$fileprefix is required');

    if (!is_dir($filedir)) {
        throw new Exception("$filedir is not a valid directory");
    }

    $matchingfiles = array();

    $dir = scandir($filedir);
    if (!$dir === false) {
        foreach ($dir as $filename) {
            if (strtolower(substr($filename, 0, strlen($fileprefix))) == strtolower($fileprefix)) {
                $filepath = $filedir . '/' . $filename;
                $filemd5 = md5_file($filepath);
                while (true) {
                    // Ensure file is not currently being written to
                    sleep(2);
                    $newmd5 = md5_file($filepath);
                    if ($filemd5 != $newmd5) {
                        $filemd5 = $newmd5;
                    } else {
                        break;
                    }
                }
                $matchingfiles[] = $filepath;
            }
        }
    }
    return $matchingfiles;
}

function androgogic_sync_load_mapped_fields($source, $staging) {
    global $DB;

    $mappedfields = new stdClass();
    $mappedfields->stagingid = $staging->id;
    //
    // only return staging data for fields that have been mapped in source settings
    //
    if ($rows = $DB->get_records_sql("SELECT * FROM {androgogic_sync_field} WHERE sourceid = $source->id AND LEFT(dbfieldname,12) <> 'profilefield'")) {
        foreach ($rows as $row) {
            if (isset($staging->{$row->dbfieldname})) {
                $mappedfields->{$row->dbfieldname} = $staging->{$row->dbfieldname};
            }
        }

        //
        // load mapped user custom fields from staging table
        //
	if ($source->element == SyncLog::ELEMENT_USER or $source->element == SyncLog::ELEMENT_COMP) {
            $sql = "SELECT s.*, i.datatype FROM {androgogic_sync_user_custom} s
					  JOIN {user_info_field} i ON i.shortname = s.shortname
					  JOIN {androgogic_sync_field} f ON f.sourceid = $source->id AND f.dbfieldname = CONCAT('profile_field_', s.shortname)
					 WHERE s.stagingid = $staging->id
					   AND s.sourceid = $source->id";
            if ($rows = $DB->get_records_sql($sql)) {
                foreach ($rows as $row) {
                    $fieldname = 'profile_field_' . $row->shortname;
                    if (!empty($row->data) and $row->datatype == 'datetime' and $source->dateformat != 'Y-m-d') {
                        //
                        // convert custom field dates when CSV date format is not Y-m-d
                        //
						$time = totara_date_parse_from_format($source->dateformat, $row->data);
                        $mappedfields->$fieldname = date('Y-m-d', $time);
                    } else {
                        $mappedfields->$fieldname = $row->data;
                    }
                }
            }
        }
    }
    return $mappedfields;
}

function androgogic_sync_notify($runid, $notifyemailto, $notifytypes) {
    global $CFG, $DB;

    assert(is_numeric($runid), '$runid must be numeric');
    assert(is_string($notifyemailto), '$notifyemailto must be a string');
    assert(is_array($notifytypes), '$notifytypes must be an array');

    // Get log messages for runid
    list($sqlin, $params) = $DB->get_in_or_equal($notifytypes);
    $sql = "SELECT l.*, s.shortname, s.element, s.source FROM {androgogic_sync_log} l
   LEFT OUTER JOIN {androgogic_sync_source} s ON s.id = l.sourceid
    		 WHERE logtype {$sqlin} AND l.runid = $runid
             ORDER BY id DESC LIMIT 200";
    $rows = $DB->get_records_sql($sql, $params);
    if (!$rows) {
        // Nothing to report.
        return true;
    }

    // Build email message
    $dateformat = get_string('strftimedateseconds', 'langconfig');
    $logcount = count($rows);
    $sitename = get_site();
    $sitename = format_string($sitename->fullname);
    $subject = get_string('notifysubject', 'local_androgogic_sync', $sitename);

    $a = new stdClass();
    $a->logtypes = implode(', ', $notifytypes);
    $a->count = $logcount;
    $message = get_string('notifymessagestart', 'local_androgogic_sync', $a);
    $message .= "\n\n";
    foreach ($rows as $row) {
        $a = new stdClass();
        $a->time = date_format_string($row->time, $dateformat);
        $a->message = $row->action;
        $a->logtype = $row->logtype;
        if (!empty($row->info)) {
            $a->message .= ", $row->info";
        }
        if (!empty($row->source)) {
            $elementname = get_string($row->element, 'local_androgogic_sync');
            $sourcename = get_string($row->source, 'local_androgogic_sync');
            $a->message .= ", Source: $row->shortname $elementname $sourcename";
        }
        $message .= get_string('notifymessage', 'local_androgogic_sync', $a) . "\n\n";
    }
    $message .= "\n" . get_string('viewloghere', 'local_androgogic_sync', $CFG->wwwroot . '/local/androgogic_sync/synclog.php');

    // Send email
    if (strpos($notifyemailto, ',') === false) {
        $notifyemailtos = array($notifyemailto);
    } else {
        $notifyemailtos = explode(',', $notifyemailto);
    }
    $supportuser = core_user::get_support_user();
    foreach ($notifyemailtos as $emailaddress) {
        $userto = \totara_core\totara_user::get_external_user(trim($emailaddress));
        email_to_user($userto, $supportuser, $subject, $message);
    }
    return true;
}

function androgogic_sync_create_log_report($reportname) {
    global $DB;

	// create Sync Log in Report Builder
	// copied from /totara/reportbuilder/index.php
	
	$sourcename = 'androgogic_sync_log';
	
	$todb = new stdClass();
	$todb->fullname = $reportname;
	$todb->shortname = reportbuilder::create_shortname($reportname);
	$todb->source = $sourcename;
	$todb->hidden = 0;
	$todb->recordsperpage = 40;
	$todb->contentmode = REPORT_BUILDER_CONTENT_MODE_NONE;
	$todb->accessmode = REPORT_BUILDER_ACCESS_MODE_ANY; // default to limited access
	$todb->embedded = 0;
	$todb->defaultsortcolumn = 'androgogic_sync_log_id';
	$todb->defaultsortorder = 3;

	try {
		$transaction = $DB->start_delegated_transaction();

		$newid = $DB->insert_record('report_builder', $todb);
		//add_to_log(SITEID, 'reportbuilder', 'new report', 'report.php?id=' . $newid, $reportname . ' (ID=' . $newid . ')');

		// by default we'll require a role but not set any, which will restrict report access to
		// the site administrators only
		$todb = new stdClass();
		$todb->reportid = $newid;
		$todb->type = 'role_access';
		$todb->name = 'enable';
		$todb->value = 1;
		$DB->insert_record('report_builder_settings', $todb);

		// restrict access to new report to site managers (and implicitly admins)
		// (if role doesn't exist report will not be visible to anyone)
		if ($managerroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'))) {
			$todb = new stdClass();
			$todb->reportid = $newid;
			$todb->type = 'role_access';
			$todb->name = 'activeroles';
			$todb->value = $managerroleid;
			$DB->insert_record('report_builder_settings', $todb);
		}

		// create columns for new report based on default columns
		$src = reportbuilder::get_source_object($sourcename);
		if (isset($src->defaultcolumns) && is_array($src->defaultcolumns)) {
			$defaultcolumns = $src->defaultcolumns;
			$so = 1;
			foreach ($defaultcolumns as $option) {
				$heading = isset($option['heading']) ? $option['heading'] :
					null;
				$hidden = isset($option['hidden']) ? $option['hidden'] : 0;
				$column = $src->new_column_from_option($option['type'],
					$option['value'], $heading, $hidden);
				$todb = new stdClass();
				$todb->reportid = $newid;
				$todb->type = $column->type;
				$todb->value = $column->value;
				$todb->heading = $column->heading;
				$todb->hidden = $column->hidden;
				$todb->sortorder = $so;
				$todb->customheading = 0; // initially no columns are customised
				$DB->insert_record('report_builder_columns', $todb);
				$so++;
			}
		}
		// create filters for new report based on default filters
		if (isset($src->defaultfilters) && is_array($src->defaultfilters)) {
			$defaultfilters = $src->defaultfilters;
			$so = 1;
			foreach ($defaultfilters as $option) {
				$todb = new stdClass();
				$todb->reportid = $newid;
				$todb->type = $option['type'];
				$todb->value = $option['value'];
				$todb->advanced = isset($option['advanced']) ? $option['advanced'] : 0;
				$todb->sortorder = $so;
				$todb->region = isset($option['region']) ? $option['region'] : rb_filter_type::RB_FILTER_REGION_STANDARD;
				$DB->insert_record('report_builder_filters', $todb);
				$so++;
			}
		}
		// Create toolbar search columns for new report based on default toolbar search columns.
		if (isset($src->defaulttoolbarsearchcolumns) && is_array($src->defaulttoolbarsearchcolumns)) {
			foreach ($src->defaulttoolbarsearchcolumns as $option) {
				$todb = new stdClass();
				$todb->reportid = $newid;
				$todb->type = $option['type'];
				$todb->value = $option['value'];
				$DB->insert_record('report_builder_search_cols', $todb);
			}
		}
		$transaction->allow_commit();
		$result = true;

	} catch (Exception $e) {
		$transaction->rollback($e);
		$result = false;
	}
	return $result;
}