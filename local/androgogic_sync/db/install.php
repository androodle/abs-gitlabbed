<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
require_once($CFG->dirroot . '/totara/reportbuilder/lib.php');
require_once($CFG->dirroot.'/local/androgogic_sync/lib.php');

function xmldb_local_androgogic_sync_install() {
    global $DB;
	
	$reportname = get_string('logreport', 'local_androgogic_sync');
	if ($DB->get_record('report_builder', array('fullname'=>$reportname))) {
		return true;
	}
	
    $result = androgogic_sync_create_log_report($reportname);

    return $result;
}
