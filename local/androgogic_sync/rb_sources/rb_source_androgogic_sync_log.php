<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot.'/local/androgogic_sync/classes/synclog.class.php');

class rb_source_androgogic_sync_log extends rb_base_source {
    public $base, $joinlist, $columnoptions, $filteroptions;
    public $contentoptions, $paramoptions, $defaultcolumns;
    public $defaultfilters, $requiredcolumns, $sourcetitle;
   
    function __construct() {
        global $CFG;
        $this->base = '{androgogic_sync_log}';
        $this->joinlist = $this->define_joinlist();
        $this->columnoptions = $this->define_columnoptions();
        $this->filteroptions = $this->define_filteroptions();
        $this->contentoptions = $this->define_contentoptions();
        $this->paramoptions = $this->define_paramoptions();
        $this->defaultcolumns = $this->define_defaultcolumns();
        $this->defaultfilters = $this->define_defaultfilters();
        $this->requiredcolumns = $this->define_requiredcolumns();
        $this->sourcetitle = get_string('logreport', 'local_androgogic_sync');
        parent::__construct();
    }

    //
    //
    // Methods for defining contents of source
    //
    //

    protected function define_joinlist() {
        $joinlist = array(
            new rb_join(
                'source',
                'LEFT',
                '{androgogic_sync_source}',
                'source.id = base.sourceid',
                REPORT_BUILDER_RELATION_ONE_TO_ONE
            )
        );

        //$this->add_user_table_to_joinlist($joinlist, 'base', 'userid');
        //$this->add_position_tables_to_joinlist($joinlist, 'base', 'userid');
        return $joinlist;
    }

    protected function define_columnoptions() {
        
        $columnoptions = array(
            new rb_column_option(
                'androgogic_sync_log',
                'id',
                get_string('logid', 'local_androgogic_sync'),
                "base.id"
            ),            
            new rb_column_option(
                'androgogic_sync_log',
                'runid',
                get_string('runid', 'local_androgogic_sync'),
                "base.runid"
            ),
            new rb_column_option(
                'androgogic_sync_log',
                'time',
                get_string('datetime', 'local_androgogic_sync'),
                "base.time",
                array('displayfunc'=>'nice_datetime_seconds')
            ),
            new rb_column_option(
                'androgogic_sync_log',
                'logtype',
                get_string('logtype', 'local_androgogic_sync'),
                "base.logtype",
                array('displayfunc'=>'logtype')
            ),
            new rb_column_option(
                'androgogic_sync_log',
                'action',
                get_string('action', 'local_androgogic_sync'),
                "base.action"
            ),
            new rb_column_option(
                'androgogic_sync_log',
                'info',
                get_string('info', 'local_androgogic_sync'),
                "base.info",
                array('displayfunc'=>'linkinfo')
            ),    
            new rb_column_option(
                'androgogic_sync_log',
                'stagingid',
                get_string('stagingid', 'local_androgogic_sync'),
                "base.stagingid",
                array('displayfunc'=>'linkstagingid',
                    'extrafields' => array(
                    'logid' => 'base.id'
                ))
            ),      
            new rb_column_option(
                'androgogic_sync_log',
                'sourceid',
                get_string('source', 'local_androgogic_sync'),
                "base.sourceid",
                array('joins'=>'source',
                	'displayfunc'=>'linksourceid',
                    'extrafields' => array(
                    'source' => 'source.source',
                    'element' => 'source.element',
                    'shortname' => 'source.shortname'
               ))
            ),                    
        ); 

        return $columnoptions;
    }

    protected function define_filteroptions() {
        $filteroptions = array(
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'runid',           // value
                get_string('runid', 'local_androgogic_sync'), // label
                'number'     // filtertype
            ),
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'stagingid',           // value
                get_string('stagingid', 'local_androgogic_sync'), // label
                'number'     // filtertype
            ),
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'time',           // value
                get_string('datetime', 'local_androgogic_sync'), // label
                'date',     // filtertype
                array(
                    'includetime'=>true,
                )
            ),
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'logtype',           // value
                get_string('logtype', 'local_androgogic_sync'), // label
                'select',     // filtertype
                array(
                    'selectfunc'=>'logtypes',
                    'attributes'=>rb_filter_option::select_width_limiter(),
                )
            ),
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'action',           // value
                get_string('action', 'local_androgogic_sync'), // label
                'text'     // filtertype
            ),
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'info',           // value
                get_string('info', 'local_androgogic_sync'), // label
                'textarea'     // filtertype
            ),            
            new rb_filter_option(
                'androgogic_sync_log',         // type
                'sourceid',           // value
                get_string('source', 'local_androgogic_sync'), // label
                'select',     // filtertype
                array(
                    'selectfunc'=>'sources'
                )
            ),
        );

        return $filteroptions;
    }

    protected function define_contentoptions() {
        $contentoptions = array(

            new rb_content_option(
                'date',
                get_string('datetime', 'local_androgogic_sync'),
                'base.time'
            ),
        );

        return $contentoptions;
    }

    protected function define_paramoptions() {
        return array();
    }

    protected function define_defaultcolumns() {
        $defaultcolumns = array(
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'id',
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'runid',
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'time',
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'logtype',
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'action',
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'info',
            ),                 
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'sourceid',
            ), 
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'stagingid',
            ),       
        );

        return $defaultcolumns;
    }

    protected function define_defaultfilters() {
        $defaultfilters = array(
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'runid',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'stagingid',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'time',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'logtype',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'action',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_sync_log',
                'value'=>'info',
                'advanced'=>0,
            ), 

            array(
                'type'=>'androgogic_sync_log',
                'value'=>'sourceid',
                'advanced'=>0,
            ),   
        );

        return $defaultfilters;
    }

    protected function define_requiredcolumns() {
        $requiredcolumns = array(
            /*
            // array of rb_column objects, e.g:
            new rb_column(
                '',         // type
                '',         // value
                '',         // heading
                '',         // field
                array()     // options
            )
            */
        );
        return $requiredcolumns;
    }


    //
    //
    // Source specific column display methods
    //
    //
    // add methods here with [name] matching column option displayfunc
    /*
    function rb_display_[name]($item, $row) {
        // variable $item refers to the current item
        // $row is an object containing the whole row
        // which will include any extrafields
        //
        // should return a string containing what should be displayed
    }
    */
    
    function rb_display_logtype($type, $row) {
        switch ($type) {
            case SyncLog::TYPE_ERROR:
                $class = 'notifyproblem';
                break;
            case SyncLog::TYPE_WARNING:
                $class = 'notifynotice';
                break;
            case SyncLog::TYPE_REVIEW:
                $class = 'notifynotice';
                break;
            default:
                $class = 'notifysuccess';
                break;
        }
        return html_writer::tag('span', $type, array('class'=>$class, 'title'=>$type));
    }
    
    function rb_display_linksourceid($sourceid, $row) {
		if (empty($sourceid)) {
			return '';
		}
		$elementname = get_string($row->element, 'local_androgogic_sync');
		$sourcename = get_string($row->source, 'local_androgogic_sync');
        $url = new moodle_url('/local/androgogic_sync/editsource.php', array('source'=>$row->source,'element'=>$row->element,'id'=>$sourceid));
        return html_writer::link($url, "$row->shortname, $elementname $sourcename");
    }
    
    function rb_display_linkstagingid($stagingid, $row) {
		if (empty($stagingid)) {
			return '';
		}
        $url = new moodle_url('/local/androgogic_sync/viewstaging.php', array('logid'=>$row->logid));
        return html_writer::link($url, "$stagingid");
    }
    
    function rb_display_linkinfo($info, $row, $isexport = false) {
        if ($isexport) {
            return $info;
        }
        
		if (empty($info)) {
			return '';
		}
		
		$url = '';
		$pieces = explode(",", $info);
		foreach ($pieces as $piece) {
			$arr = explode("=", $piece);
			if (count($arr) == 2 and !empty($arr[1])) {
				$prefix = trim($arr[0]);
				$suffix = trim($arr[1]);
				if ($prefix == 'userid') {
					$url = new moodle_url('/user/view.php', array('id'=>$suffix));
					break;
				} else if ($prefix == 'orgid') {
					$url = new moodle_url('/totara/hierarchy/item/view.php', array('prefix'=>'organisation','id'=>$suffix));
					break;
				} else if ($prefix == 'posid') {
					$url = new moodle_url('/totara/hierarchy/item/view.php', array('prefix'=>'position','id'=>$suffix));
					break;
				} else if ($prefix == 'courseid') {
					$url = new moodle_url('/course/edit.php', array('id'=>$suffix));
					break;
				}
			}
		}
		if (empty($url)) {
			return $info;
		}
        return html_writer::link($url, $info);
    }

    //
    //
    // Source specific filter display methods
    //
    //

    function rb_filter_logtypes() {
        return array(
            SyncLog::TYPE_INFO=>SyncLog::TYPE_INFO,
            SyncLog::TYPE_REVIEW=>SyncLog::TYPE_REVIEW,
            SyncLog::TYPE_WARNING=>SyncLog::TYPE_WARNING,
            SyncLog::TYPE_TRACE=>SyncLog::TYPE_TRACE,
            SyncLog::TYPE_ERROR=>SyncLog::TYPE_ERROR
        );
    }
        
    function rb_filter_sources() {
		global $DB;

		$options = array();
		if ($rows = $DB->get_records('androgogic_sync_source')) {
			foreach ($rows as $row) {
				$elementname = get_string($row->element, 'local_androgogic_sync');
				$sourcename = get_string($row->source, 'local_androgogic_sync');
				$options[$row->id] = "$row->shortname, $elementname $sourcename";
			}
		}
        return $options;
    }
} 

